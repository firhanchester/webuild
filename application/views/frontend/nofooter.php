    <script src="<?= base_url()?>assets/webuild/js/plugins.js"></script>
    <script src="<?= base_url()?>assets/webuild/js/main.js"></script>
    <script type="text/javascript">
        function link(id){
            $.ajax({
                url: "<?php echo site_url('view_notif')?>",
                type: "post",
                data : {id : id},
                success: function(res){
                    // console.log(res);
                    window.location.href = res.link;
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
    </script>
    </body>
</html>