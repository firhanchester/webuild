<div class="container">
    <div class="row" style="margin-top: 2em;">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <h4>Progress</h4>
        <form>
          <div class="form-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" disabled><?= $progress['description']?></textarea>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Progress</label>
                  <input type="text" class="form-control" disabled value="<?= $progress['progress']?> %">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Date</label>
                  <input type="text" class="form-control" disabled value="<?= date('d M Y' ,strtotime($progress['created_date']))?>">
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <?php foreach($image as $i){?>
            <div class="col-md-4" style="padding: 1em;">
              <a target="_blank" href="<?= base_url('uploads/progress/'.$i['attachment'])?>">
                <img style="width: 100%" src="<?= base_url('uploads/progress/'.$i['attachment'])?>">
              </a>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
</div>