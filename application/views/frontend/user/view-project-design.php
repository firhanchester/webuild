<style type="text/css">
    .card {
      border: 0 !important;
    }
    #unpaid {
        background: #ff4c4c;
        padding: .3em .7em;
        color: white;
    }

    #paid {
        background: #ffd54c;
        padding: .3em .7em;
    }

    #paid2 {
        background: white;
        padding: .3em .7em;
    }

    #ttl {
        font-weight: bold;
    }

    #comp {
        background: #28a745;
        padding: .3em .7em;
        color: white;
    }

    nav > .nav.nav-tabs {
        border: none;
        color: black;
        font-size: 15px;
        font-weight: bold;
        /*text-transform: uppercase;*/
        /*background:#ffd54c5e;*/
        border-radius:0;
    }

    nav > div a.nav-item.nav-link
    {
        border: none;
        color: black;
        font-size: 17px;
        font-weight: bold;
        /*text-transform: uppercase;*/
        padding: 15px 20px;
        /*background: #ffd54c;*/
        border-radius:0;
    }


    nav > div a.nav-item.nav-link.active
    {
        border: none;
        color: black;
        font-size: 17px;
        font-weight: bold;
        /*text-transform: uppercase;*/
        padding: 15px 20px;
        background: #ffd54c;
        border-radius:0;
    }
    .tab-content{
        background: #fdfdfd;
        line-height: 25px;
    }

    nav > div a.nav-item.nav-link:hover,
    nav > div a.nav-item.nav-link:focus
    {
        border: none;
        background: #ffd54c;
        /*color:#fff;*/
        border-radius:0;
        transition:background 0.20s linear;
    }

    tr {
        text-align: center;
    }

    th {
        font-size: 14px;
    }

    table {
        margin-top: 2em;
    }

    table thead{
        /*background-color: #ffd54c;*/
        margin-top: 2em;
    }

    #tbl-list td {
      width: 50%;
    }

</style>
<section style="margin: 2em 0em;">
    <div class="container mobile">   
      <div class="row">
        <div class="col-12">
          <div class="breadcrumb_content">
            <ul>
                <li><a id="ttl" href="<?= site_url('my-order')?>">My order</a></li>
                <li><a id="ttl" href="<?= site_url('my-order/design')?>">Design</a></li>
                <li><?= $order['data']['code']?></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="accordion_area">
        <div class="container">
          <div class="row">
            <div class="col-md-12"> 
                <div id="accordion" class="card__accordion">
                    <div class="card  card_dipult">
                      <div class="card-header card_accor" id="headingThree" style="background: #ffd54c !important;">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                           Request Detail
                             <i class="fa fa-plus"></i>
                             <i class="fa fa-minus"></i>
                          </button>
                      </div>

                      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="container" style="padding: 1em;">
                          <?php foreach($order['comp'] as $comp){?>
                            <span style="font-weight: bold;">Lantai <?= $comp['lantai']?></span>
                            <table class="table" id="tbl-list">
                              <tr>
                                <th>Jenis</th>
                                <th>Jumlah</th>
                              </tr>
                              <?php if($comp['main_bedroom'] != 0){?>
                              <tr>
                                <td>Main Bedroom</td>
                                <td id="<?= $comp['main_bedroom']?>">
                                  <?= $comp['main_bedroom']?>
                                  <input class="hasil1" type="hidden" value="<?= $comp['main_bedroom']?>">
                                  <input value="0" name="hasil1" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['bedroom'] != 0){?>
                              <tr>
                                <td>Bedroom</td>
                                <td id="<?= $comp['bedroom']?>">
                                  <?= $comp['bedroom']?>
                                  <input class="hasil2" type="hidden" value="<?= $comp['bedroom']?>">
                                  <input value="0" name="hasil2" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['living_bedroom'] != 0){?>
                              <tr>
                                <td>Living Bedroom</td>
                                <td id="<?= $comp['living_bedroom']?>">
                                  <?= $comp['living_bedroom']?>
                                  <input class="hasil3" type="hidden" value="<?= $comp['living_bedroom']?>">
                                  <input value="0" name="hasil3" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['family_bedroom'] != 0){?>
                              <tr>
                                <td>Family Bedroom</td>
                                <td id="<?= $comp['family_bedroom']?>">
                                  <?= $comp['family_bedroom']?>
                                  <input class="hasil4" type="hidden" value="<?= $comp['family_bedroom']?>">
                                  <input value="0" name="hasil4" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['main_bathroom'] != 0){?>
                              <tr>
                                <td>Main Bathroom</td>
                                <td id="<?= $comp['main_bathroom']?>">
                                  <?= $comp['main_bathroom']?>
                                  <input class="hasil5" type="hidden" value="<?= $comp['main_bathroom']?>">
                                  <input value="0" name="hasil5" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['bathroom'] != 0){?>
                              <tr>
                                <td>Bathroom</td>
                                <td id="<?= $comp['bathroom']?>">
                                  <?= $comp['bathroom']?>
                                  <input class="hasil6" type="hidden" value="<?= $comp['bathroom']?>">
                                  <input value="0" name="hasil6" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['guest_bathroom'] != 0){?>
                              <tr>
                                <td>Guest Bathroom</td>
                                <td id="<?= $comp['guest_bathroom']?>">
                                  <?= $comp['guest_bathroom']?>
                                  <input class="hasil7" type="hidden" value="<?= $comp['guest_bathroom']?>">
                                  <input value="0" name="hasil7" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['laundry_room'] != 0){?>
                              <tr>
                                <td>Laundry Room</td>
                                <td id="<?= $comp['laundry_room']?>">
                                  <?= $comp['laundry_room']?>
                                  <input class="hasil8" type="hidden" value="<?= $comp['laundry_room']?>">
                                  <input value="0" name="hasil8" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['study_room'] != 0){?>
                              <tr>
                                <td>Study Room</td>
                                <td id="<?= $comp['study_room']?>">
                                  <?= $comp['study_room']?>
                                  <input class="hasil9" type="hidden" value="<?= $comp['study_room']?>">
                                  <input value="0" name="hasil9" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['maid_room'] != 0){?>
                              <tr>
                                <td>Maid Room</td>
                                <td id="<?= $comp['maid_room']?>">
                                  <?= $comp['maid_room']?>
                                  <input class="hasil10" type="hidden" value="<?= $comp['maid_room']?>">
                                  <input value="0" name="hasil10" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['kitchen'] != 0){?>
                              <tr>
                                <td>Kitchen</td>
                                <td id="<?= $comp['kitchen']?>">
                                  <?= $comp['kitchen']?>
                                  <input class="hasil11" type="hidden" value="<?= $comp['kitchen']?>">
                                  <input value="0" name="hasil11" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['pantry'] != 0){?>
                              <tr>
                                <td>Pantry</td>
                                <td id="<?= $comp['pantry']?>">
                                  <?= $comp['pantry']?>
                                  <input class="hasil12" type="hidden" value="<?= $comp['pantry']?>">
                                  <input value="0" name="hasil12" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['garage'] != 0){?>
                              <tr>
                                <td>Garage</td>
                                <td id="<?= $comp['garage']?>">
                                  <?= $comp['garage']?>
                                  <input class="hasil13" type="hidden" value="<?= $comp['garage']?>">
                                  <input value="0" name="hasil13" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['carport'] != 0){?>
                              <tr>
                                <td>Carport</td>
                                <td id="<?= $comp['carport']?>">
                                  <?= $comp['carport']?>
                                  <input class="hasil14" type="hidden" value="<?= $comp['carport']?>">
                                  <input value="0" name="hasil14" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                            </table>
                          <?php } ?>
                        </div>
                      </div>

                    </div>

                    <div class="card card_dipult">
                      <div class="card-header card_accor" id="headingThree" style="background: #ffd54c !important;">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Invoice
                             <i class="fa fa-plus"></i>
                             <i class="fa fa-minus"></i>
                          </button>
                      </div>

                      <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="container" style="padding: 1em;">
                            <div class="col-md-12">
                              <table class="table table-stripped">
                                <thead>
                                  <th>Invoice</th>
                                  <th>Status</th>
                                  <th>Action</th>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td><?= $order['invoice']['code']?></td>
                                    <td>
                                      <?php if($order['invoice']['status'] == 0){?>
                                        <span>Unpaid</span>
                                      <?php }else if($order['invoice']['status'] == 1){?>
                                        <span>Waiting</span>
                                      <?php }else{?>
                                        <span>Paid</span>
                                      <?php }?>
                                    </td>
                                    <td>
                                      <a href="<?= site_url('invoice_design/'.$order['invoice']['code'])?>" class="btn btn-sm btn-warning">Show</a>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                      </div>

                    </div>

                    <div class="card card_dipult">
                      <div class="card-header card_accor" id="headingThree" style="background: #ffd54c !important;">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                           Design Result
                             <i class="fa fa-plus"></i>
                             <i class="fa fa-minus"></i>
                          </button>
                      </div>

                      <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="container" style="padding: 1em;">
                            <div class="col-md-12">
                              <table class="table table-stripped">
                                <thead>
                                  <th>Attachment</th>
                                  <th>Actions</th>
                                </thead>
                                <tbody>
                                  <?php foreach($order['file'] as $file){?>
                                  <tr>
                                    <td><?= $file['file']?></td>
                                    <td>
                                      <a href="<?= site_url('download_result/'.$file['id'])?>" class="btn btn-sm btn-warning"><i class="fa fa-download"></i></a>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                            </div>
                        </div>
                      </div>

                    </div>

                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container web">
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Request Detail</a>
                      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Invoice</a>
                      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Design Result</a>
                    </div>
                  </nav>
                  <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <?php foreach($order['comp'] as $comp){?>
                          <span style="font-weight: bold;">Lantai <?= $comp['lantai']?></span>
                          <table class="table" id="tbl-list">
                            <tr>
                              <th>Jenis</th>
                              <th>Jumlah</th>
                            </tr>
                            <?php if($comp['main_bedroom'] != 0){?>
                            <tr>
                              <td>Main Bedroom</td>
                              <td id="<?= $comp['main_bedroom']?>">
                                <?= $comp['main_bedroom']?>
                                <input class="hasil1" type="hidden" value="<?= $comp['main_bedroom']?>">
                                <input value="0" name="hasil1" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['bedroom'] != 0){?>
                            <tr>
                              <td>Bedroom</td>
                              <td id="<?= $comp['bedroom']?>">
                                <?= $comp['bedroom']?>
                                <input class="hasil2" type="hidden" value="<?= $comp['bedroom']?>">
                                <input value="0" name="hasil2" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['living_bedroom'] != 0){?>
                            <tr>
                              <td>Living Bedroom</td>
                              <td id="<?= $comp['living_bedroom']?>">
                                <?= $comp['living_bedroom']?>
                                <input class="hasil3" type="hidden" value="<?= $comp['living_bedroom']?>">
                                <input value="0" name="hasil3" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['family_bedroom'] != 0){?>
                            <tr>
                              <td>Family Bedroom</td>
                              <td id="<?= $comp['family_bedroom']?>">
                                <?= $comp['family_bedroom']?>
                                <input class="hasil4" type="hidden" value="<?= $comp['family_bedroom']?>">
                                <input value="0" name="hasil4" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['main_bathroom'] != 0){?>
                            <tr>
                              <td>Main Bathroom</td>
                              <td id="<?= $comp['main_bathroom']?>">
                                <?= $comp['main_bathroom']?>
                                <input class="hasil5" type="hidden" value="<?= $comp['main_bathroom']?>">
                                <input value="0" name="hasil5" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['bathroom'] != 0){?>
                            <tr>
                              <td>Bathroom</td>
                              <td id="<?= $comp['bathroom']?>">
                                <?= $comp['bathroom']?>
                                <input class="hasil6" type="hidden" value="<?= $comp['bathroom']?>">
                                <input value="0" name="hasil6" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['guest_bathroom'] != 0){?>
                            <tr>
                              <td>Guest Bathroom</td>
                              <td id="<?= $comp['guest_bathroom']?>">
                                <?= $comp['guest_bathroom']?>
                                <input class="hasil7" type="hidden" value="<?= $comp['guest_bathroom']?>">
                                <input value="0" name="hasil7" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['laundry_room'] != 0){?>
                            <tr>
                              <td>Laundry Room</td>
                              <td id="<?= $comp['laundry_room']?>">
                                <?= $comp['laundry_room']?>
                                <input class="hasil8" type="hidden" value="<?= $comp['laundry_room']?>">
                                <input value="0" name="hasil8" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['study_room'] != 0){?>
                            <tr>
                              <td>Study Room</td>
                              <td id="<?= $comp['study_room']?>">
                                <?= $comp['study_room']?>
                                <input class="hasil9" type="hidden" value="<?= $comp['study_room']?>">
                                <input value="0" name="hasil9" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['maid_room'] != 0){?>
                            <tr>
                              <td>Maid Room</td>
                              <td id="<?= $comp['maid_room']?>">
                                <?= $comp['maid_room']?>
                                <input class="hasil10" type="hidden" value="<?= $comp['maid_room']?>">
                                <input value="0" name="hasil10" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['kitchen'] != 0){?>
                            <tr>
                              <td>Kitchen</td>
                              <td id="<?= $comp['kitchen']?>">
                                <?= $comp['kitchen']?>
                                <input class="hasil11" type="hidden" value="<?= $comp['kitchen']?>">
                                <input value="0" name="hasil11" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['pantry'] != 0){?>
                            <tr>
                              <td>Pantry</td>
                              <td id="<?= $comp['pantry']?>">
                                <?= $comp['pantry']?>
                                <input class="hasil12" type="hidden" value="<?= $comp['pantry']?>">
                                <input value="0" name="hasil12" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['garage'] != 0){?>
                            <tr>
                              <td>Garage</td>
                              <td id="<?= $comp['garage']?>">
                                <?= $comp['garage']?>
                                <input class="hasil13" type="hidden" value="<?= $comp['garage']?>">
                                <input value="0" name="hasil13" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['carport'] != 0){?>
                            <tr>
                              <td>Carport</td>
                              <td id="<?= $comp['carport']?>">
                                <?= $comp['carport']?>
                                <input class="hasil14" type="hidden" value="<?= $comp['carport']?>">
                                <input value="0" name="hasil14" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                          </table>
                        <?php } ?>
                    </div>

                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <table class="table table-stripped">
                          <thead>
                            <th>Invoice</th>
                            <th>Status</th>
                            <th>Action</th>
                          </thead>
                          <tbody>
                            <tr>
                              <td><?= $order['invoice']['code']?></td>
                              <td>
                                <?php if($order['invoice']['status'] == 0){?>
                                  <span>Unpaid</span>
                                <?php }else if($order['invoice']['status'] == 1){?>
                                  <span>Waiting</span>
                                <?php }else{?>
                                  <span>Paid</span>
                                <?php }?>
                              </td>
                              <td>
                                <a href="<?= site_url('invoice_design/'.$order['invoice']['code'])?>" class="btn btn-sm btn-warning">Show</a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <table class="table table-stripped">
                          <thead>
                            <th>Attachment</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                            <?php foreach($order['file'] as $file){?>
                            <tr>
                              <td><?= $file['file']?></td>
                              <td>
                                <a href="<?= site_url('download_result/'.$file['id'])?>" class="btn btn-sm btn-warning"><i class="fa fa-download"></i></a>
                              </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                    </div>

                  </div>
            </div>     
        </div>
    </div>
</section>