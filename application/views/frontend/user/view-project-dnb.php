<style type="text/css">
    .card {
      border: 0 !important;
    }
    #unpaid {
        background: #ff4c4c;
        padding: .3em .7em;
        color: white;
    }

    #paid {
        background: #ffd54c;
        padding: .3em .7em;
    }

    #paid2 {
        background: white;
        padding: .3em .7em;
    }

    #ttl {
        font-weight: bold;
    }

    #comp {
        background: #28a745;
        padding: .3em .7em;
        color: white;
    }

    .unpaid {
        background: #ff4c4c;
        padding: .3em .7em;
        color: white;
    }

    .paid {
        background: #5cd23e;
        padding: .3em .7em;
        color: white;
    }

    .waiting {
        background: #ffd54c;
        padding: .3em .7em;
    }

    #ttl {
        font-weight: bold;
    }

    nav > .nav.nav-tabs {
        border: none;
        color: black;
        font-size: 15px;
        font-weight: bold;
        /*text-transform: uppercase;*/
        /*background:#ffd54c5e;*/
        border-radius:0;
    }

    nav > div a.nav-item.nav-link
    {
        border: none;
        color: black;
        font-size: 17px;
        font-weight: bold;
        /*text-transform: uppercase;*/
        padding: 15px 20px;
        /*background: #ffd54c;*/
        border-radius:0;
    }


    nav > div a.nav-item.nav-link.active
    {
        border: none;
        color: black;
        font-size: 17px;
        font-weight: bold;
        /*text-transform: uppercase;*/
        padding: 15px 20px;
        background: #ffd54c;
        border-radius:0;
    }
    .tab-content{
        background: #fdfdfd;
        line-height: 25px;
    }

    nav > div a.nav-item.nav-link:hover,
    nav > div a.nav-item.nav-link:focus
    {
        border: none;
        background: #ffd54c;
        /*color:#fff;*/
        border-radius:0;
        transition:background 0.20s linear;
    }

    tr {
        text-align: center;
    }

    th {
        font-size: 14px;
    }

    table {
        margin-top: 2em;
    }

    table thead{
        /*background-color: #ffd54c;*/
        margin-top: 2em;
    }

    #tbl-list td {
      width: 50%;
    }

    .right-number {
      font-weight: bold;
      background: white;
      padding: 5px 10px;
      border-radius: 50%;
    }

</style>
<section style="margin: 2em 0em;">
    <div class="container mobile">   
      <div class="row">
        <div class="col-12">
          <div class="breadcrumb_content">
            <ul>
                <li><a id="ttl" href="<?= site_url('my-order')?>">My order</a></li>
                <li><a id="ttl" href="<?= site_url('my-order/dnb')?>">Dnb</a></li>
                <li><?= $order['data']['code']?></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="accordion_area">
        <div class="container">
          <div class="row">
            <div class="col-md-12"> 
                <div id="accordion" class="card__accordion">
                    <div class="card  card_dipult">
                      <div class="card-header card_accor" id="headingThree" style="background: #ffd54c !important;">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                           <b>Project Detail</b>
                             <i class="fa fa-plus"></i>
                             <i class="fa fa-minus"></i>
                          </button>
                      </div>

                      <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="container" style="padding: 1em;">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Provinsi</label>
                                <input type="text" name="provinsi" class="form-control" readonly value="<?= $order['data']['province']?>">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>City</label>
                                <input type="text" name="city" class="form-control" readonly value="<?= $order['data']['city']?>">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Type</label>
                                <input type="text" name="type" class="form-control" readonly value="<?= $order['data']['type']?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="card card_dipult">
                      <div class="card-header card_accor" id="headingFour" style="background: #ffd54c !important;">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                           Request Detail
                             <i class="fa fa-plus"></i>
                             <i class="fa fa-minus"></i>
                          </button>
                      </div>

                      <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="container" style="padding: 1em;">
                          <div class="row">
                            <div class="col-md-12">
                              <?php foreach($order['comp'] as $comp){?>
                            <span style="font-weight: bold;">Lantai <?= $comp['lantai']?></span>
                            <table class="table">
                              <tr>
                                <th>Jenis</th>
                                <th>Jumlah</th>
                              </tr>
                              <?php if($comp['main_bedroom'] != 0){?>
                              <tr>
                                <td>Main Bedroom</td>
                                <td id="<?= $comp['main_bedroom']?>">
                                  <?= $comp['main_bedroom']?>
                                  <input class="hasil1" type="hidden" value="<?= $comp['main_bedroom']?>">
                                  <input value="0" name="hasil1" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['bedroom'] != 0){?>
                              <tr>
                                <td>Bedroom</td>
                                <td id="<?= $comp['bedroom']?>">
                                  <?= $comp['bedroom']?>
                                  <input class="hasil2" type="hidden" value="<?= $comp['bedroom']?>">
                                  <input value="0" name="hasil2" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['living_bedroom'] != 0){?>
                              <tr>
                                <td>Living Bedroom</td>
                                <td id="<?= $comp['living_bedroom']?>">
                                  <?= $comp['living_bedroom']?>
                                  <input class="hasil3" type="hidden" value="<?= $comp['living_bedroom']?>">
                                  <input value="0" name="hasil3" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['family_bedroom'] != 0){?>
                              <tr>
                                <td>Family Bedroom</td>
                                <td id="<?= $comp['family_bedroom']?>">
                                  <?= $comp['family_bedroom']?>
                                  <input class="hasil4" type="hidden" value="<?= $comp['family_bedroom']?>">
                                  <input value="0" name="hasil4" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['main_bathroom'] != 0){?>
                              <tr>
                                <td>Main Bathroom</td>
                                <td id="<?= $comp['main_bathroom']?>">
                                  <?= $comp['main_bathroom']?>
                                  <input class="hasil5" type="hidden" value="<?= $comp['main_bathroom']?>">
                                  <input value="0" name="hasil5" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['bathroom'] != 0){?>
                              <tr>
                                <td>Bathroom</td>
                                <td id="<?= $comp['bathroom']?>">
                                  <?= $comp['bathroom']?>
                                  <input class="hasil6" type="hidden" value="<?= $comp['bathroom']?>">
                                  <input value="0" name="hasil6" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['guest_bathroom'] != 0){?>
                              <tr>
                                <td>Guest Bathroom</td>
                                <td id="<?= $comp['guest_bathroom']?>">
                                  <?= $comp['guest_bathroom']?>
                                  <input class="hasil7" type="hidden" value="<?= $comp['guest_bathroom']?>">
                                  <input value="0" name="hasil7" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['laundry_room'] != 0){?>
                              <tr>
                                <td>Laundry Room</td>
                                <td id="<?= $comp['laundry_room']?>">
                                  <?= $comp['laundry_room']?>
                                  <input class="hasil8" type="hidden" value="<?= $comp['laundry_room']?>">
                                  <input value="0" name="hasil8" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['study_room'] != 0){?>
                              <tr>
                                <td>Study Room</td>
                                <td id="<?= $comp['study_room']?>">
                                  <?= $comp['study_room']?>
                                  <input class="hasil9" type="hidden" value="<?= $comp['study_room']?>">
                                  <input value="0" name="hasil9" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['maid_room'] != 0){?>
                              <tr>
                                <td>Maid Room</td>
                                <td id="<?= $comp['maid_room']?>">
                                  <?= $comp['maid_room']?>
                                  <input class="hasil10" type="hidden" value="<?= $comp['maid_room']?>">
                                  <input value="0" name="hasil10" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['kitchen'] != 0){?>
                              <tr>
                                <td>Kitchen</td>
                                <td id="<?= $comp['kitchen']?>">
                                  <?= $comp['kitchen']?>
                                  <input class="hasil11" type="hidden" value="<?= $comp['kitchen']?>">
                                  <input value="0" name="hasil11" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['pantry'] != 0){?>
                              <tr>
                                <td>Pantry</td>
                                <td id="<?= $comp['pantry']?>">
                                  <?= $comp['pantry']?>
                                  <input class="hasil12" type="hidden" value="<?= $comp['pantry']?>">
                                  <input value="0" name="hasil12" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['garage'] != 0){?>
                              <tr>
                                <td>Garage</td>
                                <td id="<?= $comp['garage']?>">
                                  <?= $comp['garage']?>
                                  <input class="hasil13" type="hidden" value="<?= $comp['garage']?>">
                                  <input value="0" name="hasil13" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                              <?php if($comp['carport'] != 0){?>
                              <tr>
                                <td>Carport</td>
                                <td id="<?= $comp['carport']?>">
                                  <?= $comp['carport']?>
                                  <input class="hasil14" type="hidden" value="<?= $comp['carport']?>">
                                  <input value="0" name="hasil14" type="hidden">
                                </td>
                                <td>
                                </td>
                              </tr>
                              <?php } ?>
                            </table>
                          <?php } ?>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                    <a href="<?= site_url('list_invoice/'.$order['data']['code'])?>">
                      <div class="card card_dipult">
                        <div class="card-header card_accor" id="headingSix" style="background: #ffd54c !important;">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                             Invoice
                             <i class="right-number"><?= $total_invoice;?></i>
                            </button>
                        </div>

                      </div>
                    </a>

                    <div class="card card_dipult">
                      <div class="card-header card_accor" id="headingFive" style="background: #ffd54c !important;">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                           RAB
                             <i class="fa fa-plus"></i>
                             <i class="fa fa-minus"></i>
                          </button>
                      </div>

                      <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="container" style="padding: 1em;">
                          <div class="row">
                            <div class="col-md-12">
                              <table class="table table-stripped">
                                <thead>
                                  <th>File RAB</th>
                                  <th></th>
                                </thead>
                                <tbody>
                                  <tr>
                                    <?php if($rab != NULL) {?>
                                    <td><?= strip_tags(substr($rab['file'],0,15).'...', '<p><a>') ?></td>
                                    <td>
                                      <a href="<?= site_url('download_rab/'.$order['data']['code'])?>" class="btn btn-sm btn-warning"><i class="fa fa-download"></i></a>
                                    </td>
                                    <?php }else{?>
                                    <td colspan="2">Empty</td>
                                    <?php }?>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="card card_dipult">
                      <div class="card-header card_accor" id="headingThree" style="background: #ffd54c !important;">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsex" aria-expanded="false" aria-controls="collapsex">
                           Design Result
                             <i class="fa fa-plus"></i>
                             <i class="fa fa-minus"></i>
                          </button>
                      </div>

                      <div id="collapsex" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="container" style="padding: 1em;">
                            <div class="col-md-12">
                              <table class="table table-stripped">
                                <thead>
                                  <th>Attachment</th>
                                  <th>Actions</th>
                                </thead>
                                <tbody>
                                  <?php foreach($order['file'] as $file){?>
                                  <tr>
                                    <td>
                                      <?= strip_tags(substr($file['file'],0,15).'...', '<p><a>') ?>
                                    </td>
                                    <td>
                                      <a href="<?= site_url('download_result/'.$file['id'])?>" class="btn btn-sm btn-warning"><i class="fa fa-download"></i></a>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                            </div>
                        </div>
                      </div>
                    </div>

                    <div class="card card_dipult">
                      <div class="card-header card_accor" id="headingSix" style="background: #ffd54c !important;">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                           Progress
                             <i class="fa fa-plus"></i>
                             <i class="fa fa-minus"></i>
                          </button>
                      </div>

                      <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="container" style="padding: 1em;">
                          <div class="row">
                            <div class="col-md-12">
                              <table class="table table-stripped">
                                <thead>
                                  <th>Description</th>
                                  <th>Progress</th>
                                  <th>Actions</th>
                                </thead>
                                <tbody>
                                  <?php foreach($progress as $p){?>
                                  <tr>
                                    <td>
                                      <?= strip_tags(substr($p['description'],0,35).'...', '<p><a>') ?>
                                    </td>
                                    <td>
                                      <?= $p['progress']?> %
                                    </td>
                                    <td>
                                      <a href="<?= site_url('progress/'.$p['id'])?>" class="btn btn-sm btn-warning">View</a>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>

                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container web">
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link active" id="nav-project-detail-tab" data-toggle="tab" href="#nav-project-detail" role="tab" aria-controls="nav-project-detail" aria-selected="true">Project Detail</a>
                      <a class="nav-item nav-link" id="nav-req-detail-tab" data-toggle="tab" href="#nav-req-detail" role="tab" aria-controls="nav-req-detail" aria-selected="false">Request Detail</a>
                      <a class="nav-item nav-link" id="nav-invoice-tab" data-toggle="tab" href="#nav-invoice" role="tab" aria-controls="nav-invoice" aria-selected="false">Invoice</a>
                      <a class="nav-item nav-link" id="nav-rab-tab" data-toggle="tab" href="#nav-rab" role="tab" aria-controls="nav-rab" aria-selected="false">RAB</a>
                      <a class="nav-item nav-link" id="nav-design-tab" data-toggle="tab" href="#nav-design" role="tab" aria-controls="nav-design" aria-selected="false">Design Result</a>
                      <a class="nav-item nav-link" id="nav-progress-tab" data-toggle="tab" href="#nav-progress" role="tab" aria-controls="nav-progress" aria-selected="false">Progress</a>
                    </div>
                  </nav>
                  <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-project-detail" role="tabpanel" aria-labelledby="nav-project-detail-tab">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Provinsi</label>
                            <input type="text" name="provinsi" class="form-control" readonly value="<?= $order['data']['province']?>">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>City</label>
                            <input type="text" name="city" class="form-control" readonly value="<?= $order['data']['city']?>">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Type</label>
                            <input type="text" name="type" class="form-control" readonly value="<?= $order['data']['type']?>">
                          </div>
                        </div>
                      </div>
                        
                    </div>

                    <div class="tab-pane fade" id="nav-req-detail" role="tabpanel" aria-labelledby="nav-req-detail-tab">
                        <?php foreach($order['comp'] as $comp){?>
                          <span style="font-weight: bold;">Lantai <?= $comp['lantai']?></span>
                          <table class="table" id="tbl-list">
                            <tr>
                              <th>Jenis</th>
                              <th>Jumlah</th>
                            </tr>
                            <?php if($comp['main_bedroom'] != 0){?>
                            <tr>
                              <td>Main Bedroom</td>
                              <td id="<?= $comp['main_bedroom']?>">
                                <?= $comp['main_bedroom']?>
                                <input class="hasil1" type="hidden" value="<?= $comp['main_bedroom']?>">
                                <input value="0" name="hasil1" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['bedroom'] != 0){?>
                            <tr>
                              <td>Bedroom</td>
                              <td id="<?= $comp['bedroom']?>">
                                <?= $comp['bedroom']?>
                                <input class="hasil2" type="hidden" value="<?= $comp['bedroom']?>">
                                <input value="0" name="hasil2" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['living_bedroom'] != 0){?>
                            <tr>
                              <td>Living Bedroom</td>
                              <td id="<?= $comp['living_bedroom']?>">
                                <?= $comp['living_bedroom']?>
                                <input class="hasil3" type="hidden" value="<?= $comp['living_bedroom']?>">
                                <input value="0" name="hasil3" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['family_bedroom'] != 0){?>
                            <tr>
                              <td>Family Bedroom</td>
                              <td id="<?= $comp['family_bedroom']?>">
                                <?= $comp['family_bedroom']?>
                                <input class="hasil4" type="hidden" value="<?= $comp['family_bedroom']?>">
                                <input value="0" name="hasil4" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['main_bathroom'] != 0){?>
                            <tr>
                              <td>Main Bathroom</td>
                              <td id="<?= $comp['main_bathroom']?>">
                                <?= $comp['main_bathroom']?>
                                <input class="hasil5" type="hidden" value="<?= $comp['main_bathroom']?>">
                                <input value="0" name="hasil5" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['bathroom'] != 0){?>
                            <tr>
                              <td>Bathroom</td>
                              <td id="<?= $comp['bathroom']?>">
                                <?= $comp['bathroom']?>
                                <input class="hasil6" type="hidden" value="<?= $comp['bathroom']?>">
                                <input value="0" name="hasil6" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['guest_bathroom'] != 0){?>
                            <tr>
                              <td>Guest Bathroom</td>
                              <td id="<?= $comp['guest_bathroom']?>">
                                <?= $comp['guest_bathroom']?>
                                <input class="hasil7" type="hidden" value="<?= $comp['guest_bathroom']?>">
                                <input value="0" name="hasil7" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['laundry_room'] != 0){?>
                            <tr>
                              <td>Laundry Room</td>
                              <td id="<?= $comp['laundry_room']?>">
                                <?= $comp['laundry_room']?>
                                <input class="hasil8" type="hidden" value="<?= $comp['laundry_room']?>">
                                <input value="0" name="hasil8" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['study_room'] != 0){?>
                            <tr>
                              <td>Study Room</td>
                              <td id="<?= $comp['study_room']?>">
                                <?= $comp['study_room']?>
                                <input class="hasil9" type="hidden" value="<?= $comp['study_room']?>">
                                <input value="0" name="hasil9" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['maid_room'] != 0){?>
                            <tr>
                              <td>Maid Room</td>
                              <td id="<?= $comp['maid_room']?>">
                                <?= $comp['maid_room']?>
                                <input class="hasil10" type="hidden" value="<?= $comp['maid_room']?>">
                                <input value="0" name="hasil10" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['kitchen'] != 0){?>
                            <tr>
                              <td>Kitchen</td>
                              <td id="<?= $comp['kitchen']?>">
                                <?= $comp['kitchen']?>
                                <input class="hasil11" type="hidden" value="<?= $comp['kitchen']?>">
                                <input value="0" name="hasil11" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['pantry'] != 0){?>
                            <tr>
                              <td>Pantry</td>
                              <td id="<?= $comp['pantry']?>">
                                <?= $comp['pantry']?>
                                <input class="hasil12" type="hidden" value="<?= $comp['pantry']?>">
                                <input value="0" name="hasil12" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['garage'] != 0){?>
                            <tr>
                              <td>Garage</td>
                              <td id="<?= $comp['garage']?>">
                                <?= $comp['garage']?>
                                <input class="hasil13" type="hidden" value="<?= $comp['garage']?>">
                                <input value="0" name="hasil13" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                            <?php if($comp['carport'] != 0){?>
                            <tr>
                              <td>Carport</td>
                              <td id="<?= $comp['carport']?>">
                                <?= $comp['carport']?>
                                <input class="hasil14" type="hidden" value="<?= $comp['carport']?>">
                                <input value="0" name="hasil14" type="hidden">
                              </td>
                              <td>
                              </td>
                            </tr>
                            <?php } ?>
                          </table>
                        <?php } ?>
                    </div>

                    <div class="tab-pane fade" id="nav-invoice" role="tabpanel" aria-labelledby="nav-invoice-tab">
                      <table class="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Actions</th>                
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($invoice as $ls){?>
                            <tr>
                                <td><?= $ls['title']?></td>
                                <td>
                                    <?php 
                                        if($ls['status'] == 0){
                                            echo "<span class='unpaid'>Unpaid</span>";
                                        }else if($ls['status'] == 1){
                                            echo "<span class='waiting'>Waiting</span>";
                                        }else{
                                            echo "<span class='paid'>Paid</span>";
                                        }

                                    ?>
                                        
                                    </td>
                                <td>
                                    <?= date('d M Y - H:i' ,strtotime($ls['created_date']))?>
                                </td>
                                <td>
                                    <?php if($ls['tbl'] != 'progress'){?>
                                        <a href="<?= site_url('invoice_'.$ls['tbl'].'/'.$ls['code'])?>" class="btn btn-sm btn-warning">View Detail</a>
                                    <?php }else{?>
                                        <a href="<?= site_url('invoice_progress/'.$ls['id'])?>" class="btn btn-sm btn-warning">View Detail</a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                      </table>
                    </div>

                    <div class="tab-pane fade" id="nav-rab" role="tabpanel" aria-labelledby="nav-rab-tab">
                      <table class="table table-stripped">
                        <thead>
                          <th>File RAB</th>
                          <th></th>
                        </thead>
                        <tbody>
                          <tr>
                            <?php if($rab != NULL) {?>
                            <td><?= $rab['file']?></td>
                            <td>
                              <a href="<?= site_url('download_rab/'.$order['data']['code'])?>" class="btn btn-sm btn-warning"><i class="fa fa-download"></i></a>
                            </td>
                            <?php }else{?>
                            <td colspan="2">Empty</td>
                            <?php }?>
                          </tr>
                        </tbody>
                      </table>
                    </div>

                    <div class="tab-pane fade" id="nav-design" role="tabpanel" aria-labelledby="nav-design-tab">
                      <table class="table table-stripped">
                        <thead>
                          <th>Attachment</th>
                          <th>Actions</th>
                        </thead>
                        <tbody>
                          <?php foreach($order['file'] as $file){?>
                          <tr>
                            <td>
                              <?= $file['file'] ?>
                            </td>
                            <td>
                              <a href="<?= site_url('download_result/'.$file['id'])?>" class="btn btn-sm btn-warning"><i class="fa fa-download"></i></a>
                            </td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>

                    <div class="tab-pane fade" id="nav-progress" role="tabpanel" aria-labelledby="nav-progress-tab">
                      <table class="table table-stripped">
                        <thead>
                          <th>Description</th>
                          <th>Progress</th>
                          <th>Actions</th>
                        </thead>
                        <tbody>
                          <?php foreach($progress as $p){?>
                          <tr>
                            <td>
                              <?= strip_tags(substr($p['description'],0,35), '<p><a>') ?>
                            </td>
                            <td>
                              <?= $p['progress']?> %
                            </td>
                            <td>
                              <a href="<?= site_url('progress/'.$p['id'])?>" class="btn btn-sm btn-warning">View</a>
                            </td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>

                  </div>
            </div>     
        </div>
    </div>
</section>