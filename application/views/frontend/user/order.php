<style type="text/css">
    #unpaid {
        background: #ff4c4c;
        padding: .3em .7em;
        color: white;
    }

    .card {
        border: 0;
    }

    .card-body {
        background: #ffd54c;
        border : 1px solid #ffd54c;
        border-radius: 7px;
    }

    #paid {
        background: #ffd54c;
        padding: .3em .7em;
    }

    #paid2 {
        background: white;
        padding: .3em .7em;
    }

    #ttl {
        font-weight: bold;
    }

    #comp {
        background: #28a745;
        padding: .3em .7em;
        color: white;
    }
</style>
<section class="main_content_area">
    <div class="container">  
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a id="ttl" href="<?= site_url('my-order')?>">My order</a></li>
                        <li><?= ucfirst($var);?></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="web">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Actions</th>                
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($list as $ls){?>
                            <tr>
                                <td><?= $ls['code']?></td>
                                <td>
                                    <?php if($ls['status'] == 0){?>
                                      <span>Unpaid</span>
                                    <?php }else if($ls['status'] == 1){?>
                                      <span>Waiting</span>
                                    <?php }else if($ls['status'] == 2){?>
                                      <span id="paid">On Going</span>
                                    <?php }else{?>
                                      <span id="comp">Completed</span>
                                    <?php }?>
                                </td>
                                <td>
                                    <?= date('d M Y - H:i' ,strtotime($ls['created_date']))?>
                                </td>
                                <td>
                                    <a href="<?= site_url('my-order/show_'.$var.'/'.$ls['code'])?>" class="btn btn-sm btn-warning">View Detail</a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <div class="mobile">
                    <?php foreach($list as $ls){?>
                    <a href="<?= site_url('my-order/show_'.$var.'/'.$ls['code'])?>">
                        <div class="card">
                            <div class="card-body" id="stat<?= $ls['status']?>">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                      <h4 class="card-title" style="text-transform: uppercase;">
                                        <?= $ls['code']?>
                                      </h4>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6" style="text-align: right">
                                        <?php if($ls['status'] == 0){?>
                                          <span>Unpaid</span>
                                        <?php }else if($ls['status'] == 1){?>
                                          <span>Waiting</span>
                                        <?php }else if($ls['status'] == 2){?>
                                          <span id="paid2">On Going</span>
                                        <?php }else{?>
                                          <span id="comp">Completed</span>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>        	
</section>