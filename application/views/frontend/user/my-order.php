<style type="text/css">
    .card {
      border: 0 !important;
    }
    #unpaid {
        background: #ff4c4c;
        padding: .3em .7em;
        color: white;
    }

    #paid {
        background: #ffd54c;
        padding: .3em .7em;
    }

    #paid2 {
        background: white;
        padding: .3em .7em;
    }

    #ttl {
        font-weight: bold;
    }

    #comp {
        background: #28a745;
        padding: .3em .7em;
        color: white;
    }

    nav > .nav.nav-tabs {
        border: none;
        color: black;
        font-size: 15px;
        font-weight: bold;
        /*text-transform: uppercase;*/
        /*background:#ffd54c5e;*/
        border-radius:0;
    }

    nav > div a.nav-item.nav-link
    {
        border: none;
        color: black;
        font-size: 17px;
        font-weight: bold;
        /*text-transform: uppercase;*/
        padding: 15px 20px;
        /*background: #ffd54c;*/
        border-radius:0;
    }


    nav > div a.nav-item.nav-link.active
    {
        border: none;
        color: black;
        font-size: 17px;
        font-weight: bold;
        /*text-transform: uppercase;*/
        padding: 15px 20px;
        background: #ffd54c;
        border-radius:0;
    }
    .tab-content{
        background: #fdfdfd;
        line-height: 25px;
    }

    nav > div a.nav-item.nav-link:hover,
    nav > div a.nav-item.nav-link:focus
    {
        border: none;
        background: #ffd54c;
        /*color:#fff;*/
        border-radius:0;
        transition:background 0.20s linear;
    }

    tr {
        text-align: center;
    }

    th {
        font-size: 14px;
    }

    table {
        margin-top: 2em;
    }

    table thead{
        /*background-color: #ffd54c;*/
        margin-top: 2em;
    }

</style>
<section style="margin: 2em 0em;">
    <div class="container mobile">   
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <a href="<?= site_url('my-order/design')?>">
                    <div class="card">
                        <div class="card-content-home">
                          <h4 class="margin-0">Design</h4>
                        </div>
                    </div>
                  </a>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <a href="<?= site_url('my-order/build')?>">
                    <div class="card">
                        <div class="card-content-home">
                          <h4 class="margin-0">Build</h4>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <a href="<?= site_url('my-order/dnb')?>">
                    <div class="card">
                        <div class="card-content-home">
                          <h4 class="margin-0">Design & Build</h4>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <a href="<?= site_url('my-order/improve')?>">
                    <div class="card">
                        <div class="card-content-home">
                          <h4 class="margin-0">Improve</h4>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="container web">
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Design</a>
                      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Build</a>
                      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Design & Build</a>
                      <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">Improve</a>
                    </div>
                  </nav>
                  <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Actions</th>                
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($design as $ls){?>
                                <tr>
                                    <td><?= $ls['code']?></td>
                                    <td>
                                        <?php if($ls['status'] == 0){?>
                                          <span>Unpaid</span>
                                        <?php }else if($ls['status'] == 1){?>
                                          <span>Waiting</span>
                                        <?php }else if($ls['status'] == 2){?>
                                          <span id="paid">On Going</span>
                                        <?php }else{?>
                                          <span id="comp">Completed</span>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <?= date('d M Y - H:i' ,strtotime($ls['created_date']))?>
                                    </td>
                                    <td>
                                        <a href="<?= site_url('my-order/show_design/'.$ls['code'])?>" class="btn btn-sm btn-warning">View Detail</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Actions</th>                
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($build as $ls){?>
                                <tr>
                                    <td><?= $ls['code']?></td>
                                    <td>
                                        <?php if($ls['status'] == 0){?>
                                          <span>Unpaid</span>
                                        <?php }else if($ls['status'] == 1){?>
                                          <span>Waiting</span>
                                        <?php }else if($ls['status'] == 2){?>
                                          <span id="paid">On Going</span>
                                        <?php }else{?>
                                          <span id="comp">Completed</span>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <?= date('d M Y - H:i' ,strtotime($ls['created_date']))?>
                                    </td>
                                    <td>
                                        <a href="<?= site_url('my-order/show_build/'.$ls['code'])?>" class="btn btn-sm btn-warning">View Detail</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Actions</th>                
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($dnb as $ls){?>
                                <tr>
                                    <td><?= $ls['code']?></td>
                                    <td>
                                        <?php if($ls['status'] == 0){?>
                                          <span>Unpaid</span>
                                        <?php }else if($ls['status'] == 1){?>
                                          <span>Waiting</span>
                                        <?php }else if($ls['status'] == 2){?>
                                          <span id="paid">On Going</span>
                                        <?php }else{?>
                                          <span id="comp">Completed</span>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <?= date('d M Y - H:i' ,strtotime($ls['created_date']))?>
                                    </td>
                                    <td>
                                        <a href="<?= site_url('my-order/show_dnb/'.$ls['code'])?>" class="btn btn-sm btn-warning">View Detail</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Actions</th>                
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($improve as $ls){?>
                                <tr>
                                    <td><?= $ls['code']?></td>
                                    <td>
                                        <?php if($ls['status'] == 0){?>
                                          <span>Unpaid</span>
                                        <?php }else if($ls['status'] == 1){?>
                                          <span>Waiting</span>
                                        <?php }else if($ls['status'] == 2){?>
                                          <span id="paid">On Going</span>
                                        <?php }else{?>
                                          <span id="comp">Completed</span>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <?= date('d M Y - H:i' ,strtotime($ls['created_date']))?>
                                    </td>
                                    <td>
                                        <a href="<?= site_url('my-order/show_improve/'.$ls['code'])?>" class="btn btn-sm btn-warning">View Detail</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                  </div>
            </div>     
        </div>
    </div>
</section>