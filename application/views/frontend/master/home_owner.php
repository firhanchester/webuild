<style type="text/css">
	#file {
		border : 0px;
		padding: 0px;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="demo" class="carousel slide" data-ride="carousel" data-interval="0">
			  <ul class="carousel-indicators">
			  	<?php for($i = 0; $i < count($banner); $i++) {?>
				    <li data-target="#demo" data-slide-to="<?= $i?>" class="<?php if($i == 0){echo('active');}?>"></li>
				<?php } ?>
			  </ul>
			  <div class="carousel-inner">
			  	<?php for($i = 0; $i < count($banner); $i++) {?>
			    <div class="carousel-item <?php if($i == 0){echo('active');}?>">
			    	<a onclick="modal(<?= $banner[$i]['portofolio_id']?>)">
						<img style="width: 100%;" src="<?= base_url('uploads/portofolio/'.$banner[$i]['image'])?>">
			    	</a>
			    </div>
				<?php } ?>
			  </div>
			  <a class="carousel-control-prev" href="#demo" data-slide="prev">
			    <span class="carousel-control-prev-icon"></span>
			  </a>
			  <a class="carousel-control-next" href="#demo" data-slide="next">
			    <span class="carousel-control-next-icon"></span>
			  </a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade show" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="accordion_area">
    <div class="container">
        <div class="row">
	        <div class="col-12"> 
	            <div id="accordion" class="card__accordion">
	                <div class="card  card_dipult">
		                <div class="card-header card_accor" id="headingThree" style="background: #ffd54c !important;">
		                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
		                     Have a design?
		                       <i class="fa fa-plus"></i>
		                       <i class="fa fa-minus"></i>
		                    </button>
		                </div>

		                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
		                 	<div class="card-body">
		                 		<div class="col-md-12" style="display: block;text-align: center">
			                    	<form class="web" style="display: inline-block;text-align: left;width: 100%" method="post" action="<?= site_url('project/upload')?>" enctype="multipart/form-data">
			                    		<div class="row">
								            <div class="col-md-6">
								              <div class="form-group">
								                  <label>Province</label>
								                  <select required class="form-control" id="province" name="province" onchange="dataCity(this)">
								                      <option value=""> Province </option>
								                  </select>
								              </div>
								            </div>
								            <div class="col-md-6">
								              <div class="form-group">
								                  <label>City</label>
								                  <select required class="form-control" id="city" name="city">
								                      <option> City </option>
								                  </select>
								              </div>
								            </div>
								        </div>
								        <div class="row" id="typee">
								            <div class="col-md-6">
								              <h4>Choose Type</h4>
								              <ul class="check-card" style="display: flex;">
								                <li class="check-card-item">
								                  <input type="radio" id="cluxury" name="type" value="luxury">
								                  <label for="cluxury" class="radio"></label>
								                  <div class="check-card-bg"></div>
								                  <div class="check-card-body">
								                    <div class="check-card-toggle">
								                      <span></span>
								                      <span></span>
								                    </div>
								                    <div class="check-card-body-in">
								                      <span class="check-card-title">Luxury</span>
								                      <br>
								                      <span class="">Start From 5 jt /M<small>2</small></span>
								                    </div>
								                    <div class="check-card-cancel">
								                      <a class="klik-tengah" id="luxury">Specification</a>
								                    </div>
								                  </div>
								                </li>
								                <li class="check-card-item">
								                  <input type="radio" id="cpremium" name="type" value="premium">
								                  <label for="cpremium" class="radio"></label>
								                  <div class="check-card-bg"></div>
								                  <div class="check-card-body">
								                    <div class="check-card-toggle">
								                      <span></span>
								                      <span></span>
								                    </div>
								                    <div class="check-card-body-in">
								                      <span class="check-card-title">Premium</span>
								                      <br>
								                      <span class="">Start From 4.5 jt /M<small>2</small></span>
								                    </div>
								                    <div class="check-card-cancel">
								                      <a class="klik-tengah" id="premium">Specification</a>
								                    </div>
								                  </div>
								                </li>
								              </ul>
								            </div>
								            <div class="col-md-6">
								              <h4 style="color: transparent;">Choose Type</h4>
								              <ul class="check-card" style="display: flex;">
								                <li class="check-card-item">
								                  <input type="radio" id="cminimalist" name="type" value="minimalist">
								                  <label for="cminimalist" class="radio"></label>
								                  <div class="check-card-bg"></div>
								                  <div class="check-card-body">
								                    <div class="check-card-toggle">
								                      <span></span>
								                      <span></span>
								                    </div>
								                    <div class="check-card-body-in">
								                      <span class="check-card-title">Minimalist</span>
								                      <br>
								                      <span class="">Start From 4 jt /M<small>2</small></span>
								                    </div>
								                    <div class="check-card-cancel">
								                      <a class="klik-tengah" id="minimalist">Specification</a>
								                    </div>
								                  </div>
								                </li>
								                <li class="check-card-item">
								                  <input type="radio" id="ccustom" name="type" value="custom">
								                  <label for="ccustom" class="radio"></label>
								                  <div class="check-card-bg"></div>
								                  <div class="check-card-body">
								                    <div class="check-card-toggle">
								                      <span></span>
								                      <span></span>
								                    </div>
								                    <div class="check-card-body-in">
								                      <span class="check-card-title">Custom</h4>
								                    </div>
								                    <div class="check-card-cancel">
								                      <a class="klik-tengah" id="custom">Specification</a>
								                    </div>
								                  </div>
								                </li>
								              </ul>
								            </div>
								          </div>

								          <div class="row">
								            <div class="col-md-12">
								              <div id="showluxury" style="display: none;">
								                <h3>Specification</h3>
								                <table class="table table-bordered">
								                  <?php foreach($luxury['component'] as $lux){?>
								                    <tr>
								                      <td><?= $lux['component']?></td>
								                      <td><?= $lux['spec']?></td>
								                    </tr>
								                  <?php } ?>
								                </table>
								              </div>
								              <div id="showpremium" style="display: none;">
								                <h3>Specification</h3>
								                <table class="table table-bordered">
								                  <?php foreach($premium['component'] as $lux){?>
								                    <tr>
								                      <td><?= $lux['component']?></td>
								                      <td><?= $lux['spec']?></td>
								                    </tr>
								                  <?php } ?>
								                </table>
								              </div>
								              <div id="showminimalist" style="display: none;">
								                <h3>Specification</h3>
								                <table class="table table-bordered">
								                  <?php foreach($minimalist['component'] as $lux){?>
								                    <tr>
								                      <td><?= $lux['component']?></td>
								                      <td><?= $lux['spec']?></td>
								                    </tr>
								                  <?php } ?>
								                </table>
								              </div>
								            </div>
								          </div>
								        <hr>
			                    		<div class="form-group row">
										    <label for="staticEmail" class="col-sm-6 col-form-label">Design 1</small></label>
										    <div class="col-sm-6">
					                    		<input type="hidden" name="user_id" value="<?= $login['id']?>">
				                    			<input type="file" name="denah1" class="form-control" id="file">
										    </div>
										</div>
										<div class="form-group row">
										    <label for="staticEmail" class="col-sm-6 col-form-label">Design 2</small></label>
										    <div class="col-sm-6">
				                    			<input type="file" name="denah2" class="form-control" id="file">
										    </div>
										</div>
										<div class="form-group row">
										    <label for="staticEmail" class="col-sm-6 col-form-label">Design 3</small></label>
										    <div class="col-sm-6">
				                    			<input type="file" name="denah3" class="form-control" id="file">
										    </div>
										</div>
										<div class="form-group row">
										    <label for="staticEmail" class="col-sm-6 col-form-label">Design 4</small></label>
										    <div class="col-sm-6">
				                    			<input type="file" name="denah4" class="form-control" id="file">
										    </div>
										</div>
										<div class="form-group row">
										    <label for="staticEmail" class="col-sm-6 col-form-label">Design 5</small></label>
										    <div class="col-sm-6">
				                    			<input type="file" name="denah5" class="form-control" id="file">
										    </div>
										</div>
			                    		<div class="form-group">
				                    		<button class="btn btn-lg btn-warning btn-block" type="submit">Submit</button>
			                    		</div>
			                    	</form>
			                    	<form class="mobile" style="display: inline-block;text-align: left;width: 95%" method="post" action="<?= site_url('project/upload')?>" enctype="multipart/form-data">
			                    		<div class="row">
								            <div class="col-md-6">
								              <div class="form-group">
								                  <label>Province</label>
								                  <select required class="form-control" id="province" name="province" onchange="dataCity(this)">
								                      <option value=""> Province </option>
								                  </select>
								              </div>
								            </div>
								            <div class="col-md-6">
								              <div class="form-group">
								                  <label>City</label>
								                  <select required class="form-control" id="city" name="city">
								                      <option> City </option>
								                  </select>
								              </div>
								            </div>
								          </div>
								         <hr>
			                    		<div class="form-group">
										    <label for="staticEmail">Design 1</label>
				                    		<input type="hidden" name="user_id" value="<?= $login['id']?>">
			                    			<input type="file" name="denah1" class="form-control" id="file">
										</div>
										<div class="form-group">
										    <label for="staticEmail">Design 2</label>
			                    			<input type="file" name="denah2" class="form-control" id="file">
										</div>
										<div class="form-group">
										    <label for="staticEmail">Design 3</label>
			                    			<input type="file" name="denah3" class="form-control" id="file">
										</div>
										<div class="form-group">
										    <label for="staticEmail">Design 4</label>
			                    			<input type="file" name="denah4" class="form-control" id="file">
										</div>
										<div class="form-group">
										    <label for="staticEmail">Design 5</label>
			                    			<input type="file" name="denah5" class="form-control" id="file">
										</div>
			                    		<div class="form-group">
			                    			<button class="btn btn-lg btn-warning btn-block" type="submit">Submit</button>
			                    		</div>
			                    	</form>
		                 		</div>
		                 	</div>
		                </div>

	              	</div>

	              	<div class="card card_dipult">
		                <div class="card-header card_accor" id="headingThree" style="background: #ffd54c !important;">
		                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
		                     Don't Have design?
		                       <i class="fa fa-plus"></i>
		                       <i class="fa fa-minus"></i>
		                    </button>
		                </div>

		                <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
		                 	<div class="card-body">
		                    	<center>
		                    		<div style="margin: 1em 0em;">
			                    		<a class="btn btn-block btn-lg btn-warning" href="<?= site_url('design')?>">Design</a>
		                    		</div>
		                    		<div>
			                    		<a class="btn btn-block btn-lg btn-warning" href="<?= site_url('design-and-build')?>">Design &amp; Build</a>
		                    		</div>
		                    	</center>
		                 	</div>
		                </div>

	              	</div>

	            </div>
	        </div>
	    </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
    $.ajax({
        url: "<?php echo site_url('admin/project/province')?>",
        type: "post",
        success: function(data){
            $('#province').html('<option value=""> Province </option>');
            $('#province').append(data)
        },
        error: function(error) {
            console.log(error);
        }
    });
  })

    function dataCity(pro){
      var x = $('#province').children(":selected").attr("id");
      $.ajax({
          url: "<?php echo site_url('admin/project/city')?>",
          type: "post",
          data : {province_id : x},
          success: function(data){
              $('#city').html('<option value=""> City </option>');
              $('#city').append(data);
          },
          error: function(error) {
              console.log(error);
          }
      });
    }

    function modal(n){
    	$.ajax({
	        url: "<?php echo site_url('project/click')?>",
	        type: "post",
	        data : {porto_id : n},
	        success: function(data){
	        	console.log(data);
	        	$('.modal-title').html(data.porto.title);
		    	$('.modal-body').html('<table class="table table-bordered"><tr><td>Harga</td><td>Rp'+data.porto.price+'</td></tr><tr><td>Provinsi</td><td>'+data.porto.province+'</td></tr><tr><td>Kota</td><td>'+data.porto.city+'</td></tr><tr><td>Luas Tanah</td><td>'+data.porto.lt+'</td></tr><tr><td>Luas Bangunan</td><td>'+data.porto.lb+'</td></tr><tr><td>Konsep</td><td>'+data.porto.type+'</td></tr></table>');
		    	$('.modal').modal('show');
	        },
	        error: function(error) {
	            console.log(error);
	        }
	     });
    }
</script>