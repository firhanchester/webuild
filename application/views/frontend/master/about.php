<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="demo" class="carousel slide" data-ride="carousel">
              <ul class="carousel-indicators">
                <?php for($i = 0; $i < count($banner); $i++) {?>
                    <li data-target="#demo" data-slide-to="<?= $i?>" class="<?php if($i == 0){echo('active');}?>"></li>
                <?php } ?>
              </ul>
              <div class="carousel-inner">
                <?php for($i = 0; $i < count($banner); $i++) {?>
                <div class="carousel-item <?php if($i == 0){echo('active');}?>">
                  <img style="width: 100%;" src="<?= base_url('uploads/banners/'.$banner[$i]['banner'])?>">
                </div>
                <?php } ?>
              </div>
              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>
        </div>
    </div>
</div>
 <!--about section area -->
<div class="about_section mt-32">
    <div class="container">   
        <div class="row align-items-center">
            <div class="col-12">
                <div class="about_content">
                    <h1>Welcome to WeBuild!</h1>
                    <p>Webuild is an online contractor with difersified architecture experiences.We bridge our clients to providing the best integrated design solution from the beginning of development based on their budget and schedule objectives. 
                    </p>
                </div>
            </div>    
        </div>
    </div>    
</div>
<!--about section end-->
        
<!--chose us area start-->
<div class="choseus_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="chose_title" style="margin: 2em 0;">
                    <h1>Why choose us?</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose" style="padding: 1em">
                        <img style="width: 120px;" src="<?= base_url()?>/assets/webuild/img/icon/shield.png" alt="">
                        <div class="chose_content" style="margin-top: 1em;">
                            <h2>The Safest</h2>
                            <p>membangun berdasarkan peraturan yang ada (SNI) bukan berdasarkan pengalaman atau saran tukang untuk  memberikan rasa aman dan nyaman.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose" style="padding: 1em">
                        <img style="width: 120px;" src="<?= base_url()?>/assets/webuild/img/icon/work.png" alt="">
                        <div class="chose_content" style="margin-top: 1em;">
                            <h2>The Most Efficient</h2>
                            <p> membangun dengan mempertimbangkan dari berbagai aspek sehingga cost efficient dengan hasil yang memuaskan baik dari segi fungsi maupun estetika.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose" style="padding: 1em">
                        <img style="width: 120px;" src="<?= base_url()?>/assets/webuild/img/icon/ux.png" alt="">
                        <div class="chose_content" style="margin-top: 1em;">
                            <h2>The Most Advance</h2>
                            <p>membangun dengan memberikan masukan teknologi konstruksi terbaru (smart home, green building etc) </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--chose us area end-->

<!--chose us area start-->
<div class="choseus_area">
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose" style="padding: 1em">
                        <img style="width: 120px;" src="<?= base_url()?>/assets/webuild/img/icon/abt1.png" alt="">
                        <div class="chose_content" style="margin-top: 1em;">
                            <h2>Smart</h2>
                            <div>• Integrasi desain dan konstruksi dalam sebuah manajemen proyek</div>
                            <div>• Spesifikasi dari WeBuild atau dari User sendiri</div>
                            <div>• Monitoring progress projek dan request perubahan dapat dilakukan dengan mudah</div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose" style="padding: 1em">
                        <img style="width: 120px;" src="<?= base_url()?>/assets/webuild/img/icon/abt2.png" alt="">
                        <div class="chose_content" style="margin-top: 1em;">
                            <h2>Worry Free</h2>
                            <div>• Garansi 1 tahun setelah pekerjaan*</div>
                            <div>• 100% Online</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose" style="padding: 1em">
                        <img style="width: 120px;" src="<?= base_url()?>/assets/webuild/img/icon/abt3.png" alt="">
                        <div class="chose_content" style="margin-top: 1em;">
                            <h2>Transparant</h2>
                            <div>•  RAB detail akan diberikan kepada konsumen dengan analisa harga yang dapat dipertanggungjawabkan</div>
                            <div>•  Laporan Pekerjaan Kurang/Tambah yang dapat dipertanggung jawabkan</div>
                            <div>•  Laporan Progress Pekerjaan yang real time</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" style="text-align: center;">
    <div class="col-md-12">
        <img style="width: 100%" src="<?= base_url()?>/assets/webuild/img/about/ful.jpg">
    </div>
</div>