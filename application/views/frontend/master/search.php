<style type="text/css">
  .card-body:hover {
      background-color: #ffd54c !important;
    }
</style>

<?php if($item == '') {?>

<div class="error_section">
    <div class="container">
      <div class="row">
          <div class="col-12">
              <div class="error_form">
                  <h1>404</h1>
                  <h2>Opps! PAGE NOT BE FOUND</h2>
                  <p>Sorry but the page you are looking for does not exist, have been<br> removed, name changed or is temporarily unavailable.</p>
                  <a href="<?= site_url('')?>">Back to home page</a>
              </div>
          </div>
      </div>
    </div>    
</div>

<?php }else{?>

<section id="portofolio" style="margin: 2em;">
    <div class="container">
        <div style="margin: 1em 0em;">
            <h4>Showing results for <?= $search ?> </h4>
            <hr>
        </div>
        <div class="row">
          <?php foreach($item as $i) {?>
              <div class="col-md-4" id="portofolio">
                <a href="<?= site_url('portofolio-detail/'.$i['slug'])?>">
                  <div class="card">
                    <img class="card-img-top" src="<?= base_url('uploads/portofolio/'.$i['image'])?>" alt="Card image" style="width:100%">
                    <div class="card-body">
                      <h4 class="card-title"><?= $i['title']?></h4>
                      <p class="card-text"><?= $i['province']?> / <?= $i['city']?></p>
                      <p class="card-text">Rp <?= number_format($i['price'])?></p>
                    </div>
                  </div>
                </a>
              </div>
          <?php } ?>
        </div>
    </div>
</section>

<?php } ?>