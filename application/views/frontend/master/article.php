<div class="container">
    <div class="row">
        <?php foreach($article as $a) { ?>
            <div class="col-md-6" style="margin: 1em 0;">
                <div class="card">
                    <img class="card-img-top" src="<?= base_url('uploads/thumbnails/'.$a['thumbnail'])?>" alt="Card image" style="width:100%">
                    <div class="card-body">
                      <h4 class="card-title" style="text-transform: uppercase;"><?= $a['title']?></h4>
                          <p class="card-text">
                            <?= strip_tags(substr($a['content'],0,50).'...', '<p><a>') ?>
                          </p>
                      <a href="<?= site_url('portofolio')?>" class="btn btn-warning">See More</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="blog_pagination" >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="pagination" style="border: 0px;">
                        <ul>
                            <li class="current">1</li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li class="next"><a href="#">next</a></li>
                            <li><a href="#">>></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
        