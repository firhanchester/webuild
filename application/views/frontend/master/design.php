<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="display: block;text-align: center">
        <form style="display: inline-block;text-align: left;width: 80%">
          <input type="hidden" name="customer_id" value="<?= $login['id']?>">
          <input type="hidden" name="cat" value="design">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Luas Tanah Yang Dimiliki (M<small>2</small>)</label>
                <input id="luas_tanah" value="0" type="text" name="luas_tanah" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Luas Bangunan Yang Diinginkan (M<small>2</small>)</label>
                <input id="luas_bangunan" value="0" type="text" name="luas_bangunan" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Jumlah Lantai</label>
                <input type="text" value="1" name="floor" class="form-control" placeholder="Jumlah Lantai" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
              </div>
            </div>
          </div>
          <div class="row" id="btn1">
            <div class="col-md-4">
              <button class="btn btn-warning" style="color:white;" type="button" onclick="show_lantai(1)">Next</button>
            </div>
          </div>
          <hr>
          <div class="row" id="label_first_floor">
            <div class="col-md-4">
              <div class="form-group">
                <h4>First Floor</h4>
              </div>
            </div>
          </div>
          <div class="row" id="input_first_floor">

            <div class="col-md-4">
              
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check1" type="checkbox"/>
                  <label for="check1">Main Bedroom</label>
                </div>
                <input type="text" value="0" name="main_bedroom" class="form-control check1" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check2" type="checkbox"/>
                  <label for="check2">Bedroom</label>
                </div>
                <input type="text" value="0" name="bedroom" class="form-control check2" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check3" type="checkbox"/>
                  <label for="check3">Living Bedroom</label>
                </div>
                <input type="text" value="0" name="living_bedroom" class="form-control check3" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check4" type="checkbox"/>
                  <label for="check4">Family Bedroom</label>
                </div>
                <input type="text" value="0" name="family_bedroom" class="form-control check4" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check6" type="checkbox"/>
                  <label for="check6">Main Bathroom</label>
                </div>
                <input type="text" value="0" name="main_bathroom" class="form-control check6" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

            <div class="col-md-4">
              
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check7" type="checkbox"/>
                  <label for="check7">Bathroom</label>
                </div>
                <input type="text" value="0" name="bathroom" class="form-control check7" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check8" type="checkbox"/>
                  <label for="check8">Guest Bathroom</label>
                </div>
                <input type="text" value="0" name="guest_bathroom" class="form-control check8" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check9" type="checkbox"/>
                  <label for="check9">Laundry Room</label>
                </div>
                <input type="text" value="0" name="laundry_room" class="form-control check9" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check10" type="checkbox"/>
                  <label for="check10">Study Room</label>
                </div>
                <input type="text" value="0" name="study_room" class="form-control check10" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check11" type="checkbox"/>
                  <label for="check11">Maid Room</label>
                </div>
                <input type="text" value="0" name="maid_room" class="form-control check11" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

            <div class="col-md-4">

              <div class="form-group">
                <div class="inputGroup">
                  <input id="check12" type="checkbox"/>
                  <label for="check12">Kitchen</label>
                </div>
                <input type="text" value="0" name="kitchen" class="form-control check12" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check13" type="checkbox"/>
                  <label for="check13">Pantry</label>
                </div>
                <input type="text" value="0" name="pantry" class="form-control check13" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check14" type="checkbox"/>
                  <label for="check14">Garage</label>
                </div>
                <input type="text" value="0" name="garage" class="form-control check14" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check15" type="checkbox"/>
                  <label for="check15">Carport</label>
                </div>
                <input type="text" value="0" name="carport" class="form-control check15" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

          </div>

          <div class="row" id="btn2" style="margin: 2em 0;">
            <div class="col-md-4">
              <button class="btn btn-warning" style="color:white;" type="button" onclick="show_lantai(2)">Next</button>
            </div>
          </div>

          <div class="row" id="label_scnd_floor">
            <div class="col-md-4">
              <div class="form-group">
                <h4>Second Floor</h4>
              </div>
            </div>
          </div>

          <div class="row" id="input_scnd_floor">

            <div class="col-md-4">
              
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check16" type="checkbox"/>
                  <label for="check16">Main Bedroom</label>
                </div>
                <input type="text" value="0" name="main_bedroom2" class="form-control check16" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check17" type="checkbox"/>
                  <label for="check17">Bedroom</label>
                </div>
                <input type="text" value="0" name="bedroom2" class="form-control check17" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check18" type="checkbox"/>
                  <label for="check18">Living Bedroom</label>
                </div>
                <input type="text" value="0" name="living_bedroom2" class="form-control check18" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check19" type="checkbox"/>
                  <label for="check19">Family Bedroom</label>
                </div>
                <input type="text" value="0" name="family_bedroom2" class="form-control check19" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

            <div class="col-md-4">
              
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check20" type="checkbox"/>
                  <label for="check20">Main Bathroom</label>
                </div>
                <input type="text" value="0" name="main_bathroom2" class="form-control check20" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check21" type="checkbox"/>
                  <label for="check21">Bathroom</label>
                </div>
                <input type="text" value="0" name="bathroom2" class="form-control check21" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check22" type="checkbox"/>
                  <label for="check22">Guest Bathroom</label>
                </div>
                <input type="text" value="0" name="guest_bathroom2" class="form-control check22" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check24" type="checkbox"/>
                  <label for="check24">Study Room</label>
                </div>
                <input type="text" value="0" name="study_room2" class="form-control check24" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

            <div class="col-md-4">

              <div class="form-group">
                <div class="inputGroup">
                  <input id="check23" type="checkbox"/>
                  <label for="check23">Laundry Room</label>
                </div>
                <input type="text" value="0" name="laundry_room2" class="form-control check23" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check25" type="checkbox"/>
                  <label for="check25">Maid Room</label>
                </div>
                <input type="text" value="0" name="maid_room2" class="form-control check25" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check26" type="checkbox"/>
                  <label for="check26">Kitchen</label>
                </div>
                <input type="text" value="0" name="kitchen2" class="form-control check26" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check27" type="checkbox"/>
                  <label for="check27">Pantry</label>
                </div>
                <input type="text" value="0" name="pantry2" class="form-control check27" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

          </div>

          <div class="row" id="btn3" style="margin: 2em 0;">
            <div class="col-md-4">
              <button class="btn btn-warning" style="color:white;" type="button" onclick="show_lantai(3)">Next</button>
            </div>
          </div>

          <div class="row" id="label_thrd_floor">
            <div class="col-md-4">
              <div class="form-group">
                <h4>Third Floor</h4>
              </div>
            </div>
          </div>

          <div class="row" id="input_thrd_floor">

            <div class="col-md-4">
              
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check30" type="checkbox"/>
                  <label for="check30">Main Bedroom</label>
                </div>
                <input type="text" value="0" name="main_bedroom3" class="form-control check30" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check31" type="checkbox"/>
                  <label for="check31">Bedroom</label>
                </div>
                <input type="text" value="0" name="bedroom3" class="form-control check31" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check32" type="checkbox"/>
                  <label for="check32">Living Bedroom</label>
                </div>
                <input type="text" value="0" name="living_bedroom3" class="form-control check32" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check33" type="checkbox"/>
                  <label for="check33">Family Bedroom</label>
                </div>
                <input type="text" value="0" name="family_bedroom3" class="form-control check33" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

            <div class="col-md-4">
              
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check34" type="checkbox"/>
                  <label for="check34">Main Bathroom</label>
                </div>
                <input type="text" value="0" name="main_bathroom3" class="form-control check34" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check35" type="checkbox"/>
                  <label for="check35">Bathroom</label>
                </div>
                <input type="text" value="0" name="bathroom3" class="form-control check35" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check36" type="checkbox"/>
                  <label for="check36">Guest Bathroom</label>
                </div>
                <input type="text" value="0" name="guest_bathroom3" class="form-control check36" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check38" type="checkbox"/>
                  <label for="check38">Study Room</label>
                </div>
                <input type="text" value="0" name="study_room3" class="form-control check38" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

            <div class="col-md-4">

              <div class="form-group">
                <div class="inputGroup">
                  <input id="check37" type="checkbox"/>
                  <label for="check37">Laundry Room</label>
                </div>
                <input type="text" value="0" name="laundry_room3" class="form-control check37" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check39" type="checkbox"/>
                  <label for="check39">Maid Room</label>
                </div>
                <input type="text" value="0" name="maid_room3" class="form-control check39" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check40" type="checkbox"/>
                  <label for="check40">Kitchen</label>
                </div>
                <input type="text" value="0" name="kitchen3" class="form-control check40" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check41" type="checkbox"/>
                  <label for="check41">Pantry</label>
                </div>
                <input type="text" value="0" name="pantry3" class="form-control check41" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

          </div>

          <div class="row" id="btn4" style="margin: 2em 0;">
            <div class="col-md-4">
              <button class="btn btn-warning" style="color:white;" type="button" onclick="show_lantai(4)">Next</button>
            </div>
          </div>

          <div class="row" id="label_frth_floor">
            <div class="col-md-4">
              <div class="form-group">
                <h4>Fourth Floor</h4>
              </div>
            </div>
          </div>

          <div class="row" id="input_frth_floor">

            <div class="col-md-4">
              
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check44" type="checkbox"/>
                  <label for="check44">Main Bedroom</label>
                </div>
                <input type="text" value="0" name="main_bedroom4" class="form-control check44" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check45" type="checkbox"/>
                  <label for="check45">Bedroom</label>
                </div>
                <input type="text" value="0" name="bedroom4" class="form-control check45" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check46" type="checkbox"/>
                  <label for="check46">Living Bedroom</label>
                </div>
                <input type="text" value="0" name="living_bedroom4" class="form-control check46" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check47" type="checkbox"/>
                  <label for="check47">Family Bedroom</label>
                </div>
                <input type="text" value="0" name="family_bedroom4" class="form-control check47" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

            <div class="col-md-4">
              
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check48" type="checkbox"/>
                  <label for="check48">Main Bathroom</label>
                </div>
                <input type="text" value="0" name="main_bathroom4" class="form-control check48" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check49" type="checkbox"/>
                  <label for="check49">Bathroom</label>
                </div>
                <input type="text" value="0" name="bathroom4" class="form-control check49" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check50" type="checkbox"/>
                  <label for="check50">Guest Bathroom</label>
                </div>
                <input type="text" value="0" name="guest_bathroom4" class="form-control check50" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check52" type="checkbox"/>
                  <label for="check52">Study Room</label>
                </div>
                <input type="text" value="0" name="study_room4" class="form-control check52" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

            <div class="col-md-4">

              <div class="form-group">
                <div class="inputGroup">
                  <input id="check51" type="checkbox"/>
                  <label for="check51">Laundry Room</label>
                </div>
                <input type="text" value="0" name="laundry_room4" class="form-control check51" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check53" type="checkbox"/>
                  <label for="check53">Maid Room</label>
                </div>
                <input type="text" value="0" name="maid_room4" class="form-control check53" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check54" type="checkbox"/>
                  <label for="check54">Kitchen</label>
                </div>
                <input type="text" value="0" name="kitchen4" class="form-control check54" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>
              <div class="form-group">
                <div class="inputGroup">
                  <input id="check55" type="checkbox"/>
                  <label for="check55">Pantry</label>
                </div>
                <input type="text" value="0" name="pantry4" class="form-control check55" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
              </div>

            </div>

          </div>

          <div class="row" id="other">
            <div class="col-md-12">
              <div class="form-group">
                <label>Other Description</label>
                <textarea rows="7" class="form-control" name="other"></textarea>
              </div>
            </div>
          </div>

          <div class="form-group" id="btnsubmit">
            <button type="button" onclick="proceed()" class="btn btn-warning btn-block btn-lg">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#label_first_floor').hide();
  $('#input_first_floor').hide();

  $('#label_scnd_floor').hide();
  $('#input_scnd_floor').hide();

  $('#label_thrd_floor').hide();
  $('#input_thrd_floor').hide();

  $('#label_frth_floor').hide();
  $('#input_frth_floor').hide();

  $('#typee').hide();
  $('#other').hide();
  $('#btnsubmit').hide();
  $('#btn2').hide();
  $('#btn3').hide();
  $('#btn4').hide();

  for (var i = 1; i < 70; i++) {
    $('.check'+i).hide();
    $('#check'+i).change(function() {
      if($(this).is(':checked')) {
          $('.'+this.id).show();
          $('.'+this.id).val('1');
        } else{
          $('.'+this.id).hide();
          $('.'+this.id).val('0');
        }
    });
  }

  function show_lantai(n){
    var floor = $('input[name=floor]').val();
    var province = $('#province').val();
    var city = $('#city').val();
    var lt = $('input[name=luas_tanah]').val();
    var lb = $('input[name=luas_bangunan]').val();

    if(floor > 3) {
      swal("WARNING!", "Maksimal 3 Lantai", "warning");
    }else{
      if(n == 1){
        console.log(province);
        console.log(city);
        console.log(lt);
        console.log(lb);
        if(city != 'City' && province != '' && lt != '0' && lb != '0'){
          $('input[name=floor]').attr('disabled','disabled');
          $('#province').attr('disabled','disabled');
          $('#city').attr('disabled','disabled');
          $('input[name=luas_tanah]').attr('disabled','disabled');
          $('input[name=luas_bangunan]').attr('disabled','disabled');
          $('#label_first_floor').show();
          $('#input_first_floor').show();
          $('#btn1').hide();
          if(floor == '1'){
            $('#typee').show();
            $('#other').show();
            $('#btnsubmit').show();
          }else{
            $('#btn2').show();
          }
        }else{
          swal("WARNING!", "Please complete the form", "warning");
        }
      }else if(n == 2){
        $('#label_scnd_floor').show();
        $('#input_scnd_floor').show();
        $('#btn2').hide();
        if(floor == '2'){
          $('#typee').show();
          $('#other').show();
          $('#btnsubmit').show();
        }else{
          $('#btn3').show();
        }

      }else if(n == 3){
        $('#label_thrd_floor').show();
        $('#input_thrd_floor').show();
        $('#btn3').hide();
        if(floor == '3'){
          $('#typee').show();
          $('#other').show();
          $('#btnsubmit').show();
        }else{
          $('#btn4').show();
        }     
      }else if(n == 4){
        $('#label_frth_floor').show();
        $('#input_frth_floor').show();
        $('#btn4').hide();
        if(floor == '4'){
          $('#typee').show();
          $('#other').show();
          $('#btnsubmit').show();
        }
      }
    }
  }

  $('#luxury').click(function(){
    $('#showluxury').show()
    $('#showminimalist').hide()
    $('#showpremium').hide()
  })

  $('#premium').click(function(){
    $('#showluxury').hide()
    $('#showminimalist').hide()
    $('#showpremium').show()
  })

  $('#minimalist').click(function(){
    $('#showluxury').hide()
    $('#showminimalist').show()
    $('#showpremium').hide()
  })

  $(document).ready(function(){
    $.ajax({
        url: "<?php echo site_url('admin/project/province')?>",
        type: "post",
        success: function(data){
            $('#province').html('<option value=""> Province </option>');
            $('#province').append(data)
        },
        error: function(error) {
            console.log(error);
        }
    });
  })

  function dataCity(pro){
    var x = $('#province').children(":selected").attr("id");
    $.ajax({
        url: "<?php echo site_url('admin/project/city')?>",
        type: "post",
        data : {province_id : x},
        success: function(data){
            $('#city').html('<option value=""> City </option>');
            $('#city').append(data);
        },
        error: function(error) {
            console.log(error);
        }
    });
  }

    function proceed(){
      var floor = $('input[name=floor]').val();
      if ($('#province').val() == "" || $('#city').val() == "" || $('#luas_tanah').val() == "" || $('#luas_bangunan').val() == "" ) {
          swal("WARNING!", "Please complete the form", "warning");
      }
      else
      {
        var detail = {
            'customer_id': $('input[name=customer_id]').val(),
            'luas_tanah': $('input[name=luas_tanah]').val(),
            'luas_bangunan': $('input[name=luas_bangunan]').val(),
            'main_bedroom': $('input[name=main_bedroom]').val(),
            'bedroom': $('input[name=bedroom]').val(),
            'living_bedroom': $('input[name=living_bedroom]').val(),
            'family_bedroom': $('input[name=family_bedroom]').val(),
            'floor': $('input[name=floor]').val(),
            'main_bathroom': $('input[name=main_bathroom]').val(),
            'bathroom': $('input[name=bathroom]').val(),
            'guest_bathroom': $('input[name=guest_bathroom]').val(),
            'laundry_room': $('input[name=laundry_room]').val(),
            'study_room': $('input[name=study_room]').val(),
            'maid_room': $('input[name=maid_room]').val(),
            'kitchen': $('input[name=kitchen]').val(),
            'pantry': $('input[name=pantry]').val(),
            'garage': $('input[name=garage]').val(),
            'carport': $('input[name=carport]').val(),
            'type': '',
            'other': $('textarea[name=other]').val(),
            'cat': $('input[name=cat]').val(),
            'province': '',
            'city':'',
            'lantai': '1',
        }

        swal({
          title: "Loading",
          text: "Please wait a second",
          icon: "warning",
          buttons: false,
          dangerMode: true,
          closeOnClickOutside: false,
        });

        $.ajax({
            url: "<?php echo site_url('project/proceed'); ?>",
            type: "POST",
            data: detail,
            success: function(status){
              if(status.success == true){
                  console.log(status);
                  var x = status.data;
                  if(floor == 1){
                    swal.close();
                    window.location.href='<?= site_url("request_design/"); ?>' + x;
                  }else if(floor == 2){
                    var detail = {
                        'code': x,
                        'main_bedroom': $('input[name=main_bedroom2]').val(),
                        'bedroom': $('input[name=bedroom2]').val(),
                        'living_bedroom': $('input[name=living_bedroom2]').val(),
                        'family_bedroom': $('input[name=family_bedroom2]').val(),
                        'main_bathroom': $('input[name=main_bathroom2]').val(),
                        'bathroom': $('input[name=bathroom2]').val(),
                        'guest_bathroom': $('input[name=guest_bathroom2]').val(),
                        'laundry_room': $('input[name=laundry_room2]').val(),
                        'study_room': $('input[name=study_room2]').val(),
                        'maid_room': $('input[name=maid_room2]').val(),
                        'kitchen': $('input[name=kitchen2]').val(),
                        'pantry': $('input[name=pantry2]').val(),
                        'lantai': '2',
                    }

                    $.ajax({
                        url: "<?php echo site_url('project/proceed_second'); ?>",
                        type: "POST",
                        data: detail,
                        success: function(status){
                          if(status.success == true){
                              console.log(status);
                              var x = status.data;
                              swal.close();
                              window.location.href='<?= site_url("request_design/"); ?>' + x;
                          }else{
                              
                          }
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                  }else if(floor == 3){
                    var detail = {
                        'code': x,
                        'main_bedroom': $('input[name=main_bedroom2]').val(),
                        'bedroom': $('input[name=bedroom2]').val(),
                        'living_bedroom': $('input[name=living_bedroom2]').val(),
                        'family_bedroom': $('input[name=family_bedroom2]').val(),
                        'main_bathroom': $('input[name=main_bathroom2]').val(),
                        'bathroom': $('input[name=bathroom2]').val(),
                        'guest_bathroom': $('input[name=guest_bathroom2]').val(),
                        'laundry_room': $('input[name=laundry_room2]').val(),
                        'study_room': $('input[name=study_room2]').val(),
                        'maid_room': $('input[name=maid_room2]').val(),
                        'kitchen': $('input[name=kitchen2]').val(),
                        'pantry': $('input[name=pantry2]').val(),
                        'lantai': '2',
                    }

                    $.ajax({
                        url: "<?php echo site_url('project/proceed_second'); ?>",
                        type: "POST",
                        data: detail,
                        success: function(status){
                          if(status.success == true){
                              console.log(status);
                              var x = status.data;
                              var detail = {
                                  'code': x,
                                  'main_bedroom': $('input[name=main_bedroom3]').val(),
                                  'bedroom': $('input[name=bedroom3]').val(),
                                  'living_bedroom': $('input[name=living_bedroom3]').val(),
                                  'family_bedroom': $('input[name=family_bedroom3]').val(),
                                  'main_bathroom': $('input[name=main_bathroom3]').val(),
                                  'bathroom': $('input[name=bathroom3]').val(),
                                  'guest_bathroom': $('input[name=guest_bathroom3]').val(),
                                  'laundry_room': $('input[name=laundry_room3]').val(),
                                  'study_room': $('input[name=study_room3]').val(),
                                  'maid_room': $('input[name=maid_room3]').val(),
                                  'kitchen': $('input[name=kitchen3]').val(),
                                  'pantry': $('input[name=pantry3]').val(),
                                  'lantai': '3',
                              }

                              $.ajax({
                                  url: "<?php echo site_url('project/proceed_second'); ?>",
                                  type: "POST",
                                  data: detail,
                                  success: function(status){
                                    if(status.success == true){
                                        console.log(status);
                                        var x = status.data;
                                        swal.close();
                                        window.location.href='<?= site_url("request_design/"); ?>' + x;
                                    }else{
                                        
                                    }
                                  },
                                  error: function(error) {
                                      console.log(error);
                                  }
                              });
                          }else{
                              
                          }
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                  }else if(floor == 4){

                  }
              }else{
                  
              }
            },
            error: function(error) {
                console.log(error);
            }
        });
      }
    }

</script>