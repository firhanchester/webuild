<div class="contact_map mt-30">
       <div class="container">
            <div class="row">
                <div class="col-12">
                   <div class="map-area">
                      <div id="googleMap"></div>
                   </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contact_area">
        <div class="container"> 
            <div class="row">
                <div class="col-lg-6 col-md-12">
                   <div class="contact_message form">
                        <h3>contact us</h3>
                        <ul>
                            <li><i class="fa fa-envelope-o"></i><a href="#">webuild.ptiki@gmail.com</a></li>
                            <li><i class="fa fa-phone"></i> <a href="tel:02187678987">(021)- 87678987</a></li>
                            <li><i class="fa fa-fax"></i>  Address : Jakarta indonesia</li>
                        </ul> 
                    </div> 
                </div>
                <div class="col-lg-6 col-md-12">
                   <div class="contact_message form">
                        <h3>How About WeBuild?</h3>   
                        <form id="contact-form" method="POST"  action="<?= site_url('feedback')?>">
                            <p>  
                               <label> Your Name (<span style="font-style: italic;">required</span>)</label>
                                <input required name="name" placeholder="Name *" type="text"> 
                            </p>
                            <p>       
                               <label>  Your Email (<span style="font-style: italic;">required</span>)</label>
                                <input required name="email" placeholder="Email *" type="email">
                            </p>
                            <p>          
                               <label>  Subject (<span style="font-style: italic;">required</span>)</label>
                                <input required name="subject" placeholder="Subject *" type="text">
                            </p>    
                            <div class="contact_textarea">
                                <label>  Your Message (<span style="font-style: italic;">required</span>)</label>
                                <textarea required placeholder="Message *" name="message"  class="form-control2" ></textarea>     
                            </div>   
                            <button type="submit"> Send</button>  
                            <p class="form-messege"></p>
                        </form> 

                    </div> 
                </div>
            </div>
        </div>    
    </div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsfbj7kLbW7kGcOzf0k22TZN43zA8rLAA"></script>
<script  src="https://www.google.com/jsapi"></script>
<script src="<?= base_url()?>assets/webuild/js/map.js"></script>
