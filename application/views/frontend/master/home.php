<style type="text/css">
    .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      width: 310px;
      padding: 0 5px 0 15px;
    }

    .ui-rounded .card {
        border-radius: 10px;
    }
    .has-background {
        overflow: hidden;
        display: block;
        position: relative;
    }

    .background {
      height: 100%;
      width: 100%;
      position: absolute;
      margin: 0;
      padding: 0;
      top: 0;
      left: 0;
      z-index: 1;
      background-position: center top;
      background-size: cover;
      background-repeat: no-repeat;
      border-radius: 15px;
    }

    .opacity-30 {
        opacity: 0.3;
    }

    /* line 208, C:/wamp64/www/mobileux2.0/assets/scss/_custom.scss */
    .background > img {
      display: none;
    }

    /* line 212, C:/wamp64/www/mobileux2.0/assets/scss/_custom.scss */
    .background + * {
      position: relative;
      z-index: 1;
    }

    .swiper-pagination {
        padding-left: 35px;
        width: auto;
    }

    .swiper-container-horizontal>.swiper-pagination-bullets, .swiper-pagination-custom, .swiper-pagination-fraction {
        bottom: 10px;
        left: 0;
        width: 100%;
    }
    .white-pagination .swiper-pagination-bullet {
        opacity: 0.4;
        background-color: #ffffff;
        margin: 0 2px;
        border-radius: 4px;
        height: 6px;
        width: 6px;
    }

    .white-pagination .swiper-pagination-bullet.swiper-pagination-bullet-active {
        width: 16px;
        opacity: 1;
    }

    .ui-rounded .btn {
        border-radius: 30px;
    }
    .btn-white {
        background-color: #ffffff;
        color: #000000;
    }
    .btn-group-sm > .btn, .btn-sm {
        padding: 0.375rem .8rem;
        font-size: 12px;
        line-height: 20px;
    }

    .card-body:hover {
      background-color: #ffd54c !important;
    }

    #overlay-img {
      position: absolute;
      top: 55%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
      text-align: center;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <ul class="carousel-indicators">
              <?php for($i = 0; $i < count($banner); $i++) {?>
                  <li data-target="#demo" data-slide-to="<?= $i?>" class="<?php if($i == 0){echo('active');}?>"></li>
              <?php } ?>
            </ul>
            <div class="carousel-inner">
              <?php for($i = 0; $i < count($banner); $i++) {?>
              <div class="carousel-item <?php if($i == 0){echo('active');}?>">
                <a target="_blank" href="<?= $banner[$i]['link']?>">
                  <img style="width: 100%;" src="<?= base_url('uploads/banners/'.$banner[$i]['banner'])?>">
                </a>
              </div>
              <?php } ?>
            </div>
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
              <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>
          </div>
        </div>
    </div>
</div>
<!--shipping area start-->
<section class="shipping_area mb-50 web" style="margin: 3em 0em;">
    <div class="container">
      <div style="margin: 1em 0em;text-align : center;">
          <span class="title">Why Choose Us?</span>
          <hr>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="shipping_inner">
            <div class="single_shipping">
                <div class="shipping_icone">
                  <img style="width: 40px;" src="<?= base_url()?>/assets/webuild/img/icon/shield.png" alt="">
                </div>
                <div class="shipping_content">
                  <h4>The Safest</h4>
                </div>
            </div>
            <div class="single_shipping">
                <div class="shipping_icone">
                  <img style="width: 40px;" src="<?= base_url()?>/assets/webuild/img/icon/work.png" alt="">
                </div>
                <div class="shipping_content">
                  <h4>The Most Efficient</h4>
                </div>
            </div>
            <div class="single_shipping">
                <div class="shipping_icone">
                  <img style="width: 40px;" src="<?= base_url()?>/assets/webuild/img/icon/ux.png" alt="">
                </div>
                <div class="shipping_content">
                  <h4>The Most Advance</h4>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>

<section id="portofolio">
  <div class="container">
    
  </div>
</section>
<section id="portofolio" style="margin: 2em 0em;">
    <div class="container">
      <div style="margin: 1em 0em;text-align : center;">
          <span class="title">Are You Home Owner ?</span>
          <hr>
      </div>

      <div class="row mobile">
        <div class="swiper-container offerslidetab1">
          <div class="swiper-wrapper">
            <?php foreach($concept as $conc) { ?>
            <div class="swiper-slide">
              <a href="<?= site_url('home_owner/'.$conc['name'])?>">
                <div style="background-color: none;" class="card has-background border-0 text-white">
                  <div class="background ">
                      <img style="border-radius: 15px" src="<?= base_url('assets/webuild/img/product/'.$conc['name'].'.jpg')?>" alt="">
                  </div>
                  <div class="card-body">
                      <p class="text-mute" style="color: transparent;">Best product and collections</p>
                      <h3 class="font-weight-normal" style="color: white;text-align: center;"><?= strtoupper($conc['name']) ?></h3>
                      <p class="text-mute" style="color: white;text-align: center;">Start From <?= $conc['price']?> jt/M<small>2</small></p>
                      <p class="text-mute" style="color: transparent;">HELLO</p>
                  </div>
                </div>
              </a>
            </div>
              <?php } ?>
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination white-pagination text-left mb-3"></div>
        </div>
      </div>

      <div id="row1" class="web">
        <?php foreach($concept as $conc) { ?>
          <div class="col" id="portofolio">
            <a href="<?= site_url('home_owner/'.$conc['name'])?>">
              <div class="card">
                <img class="card-img-top" src="<?= base_url('assets/webuild/img/product/'.$conc['name'].'.jpg')?>" alt="Card image" style="width:100%;border-radius: 15px;">
                <div class="card-body" id="overlay-img">
                  <h3 style="color: white"><?= strtoupper($conc['name'])?></h3>
                  <span style="color: white;">Start From <?= $conc['price']?>jt /M<small>2</small></span>
                </div>
              </div>
            </a>
          </div>
        <?php } ?>
      </div>

    </div>
</section>

<section id="portofolio" style="margin: 2em 0em;">
    <div class="container">
      <div style="margin: 1em 0em;text-align : center;">
          <span class="title">Looking For Home Improvement / Project? ?</span>
          <hr>
      </div>

      <div class="row">
        <!--  -->
        <?php foreach($improve as $imp) {?>
        <div class="col-md-3 col-sm-6 col-xs-6">
          <a href="<?= site_url('project/improve/'.$imp['name'])?>">
            <div class="card">
                <div class="card-content-home">
                  <h4 class="margin-0"><?= ucfirst($imp['name']) ?></h4>
                </div>
            </div>
          </a>
        </div>
        <?php } ?>
        <!--  -->
      </div>

    </div>
</section>
<section id="portofolio" style="margin: 2em 0em">
    <div class="container">
      <div style="margin: 1em 0em;text-align : center;">
          <span class="title">Portofolio</span>
          <hr>
      </div>

      <div id="row3" class="web">
        <?php foreach($porto as $i) { ?>
          <div class="col" id="portofolio">
            <a href="<?= site_url('portofolio-detail/'.$i['slug'])?>">
              <div class="card">
                <img class="card-img-top" src="<?= base_url('uploads/portofolio/'.$i['image'][0]['image'])?>" alt="Card image" style="width:100%">
                <div class="card-body">
                  <h4 class="card-title"><?= $i['title']?></h4>
                  <p class="card-text"><?= $i['province']?> / <?= $i['city']?></p>
                  <p class="card-text">Rp <?= number_format($i['price'])?></p>
                </div>
              </div>
            </a>
          </div>
        <?php } ?>
      </div>

      <div class="row mobile">
        <div class="swiper-container offerslidetab1">
          <div class="swiper-wrapper">
            <?php for($i = 1;$i < 4;$i++) { ?>
            <div class="swiper-slide">
              <a href="http://google.com">
                <div style="background-color: none;" class="card has-background border-0 text-white">
                  <div class="background ">
                      <img src="<?= base_url()?>assets/webuild/img/product/house.jpg" alt="">
                  </div>
                  <div class="card-body">
                      <h4 class="font-weight-normal">Portofolio</h4>
                      <p class="text-mute" >Best product and collections</p>
                      <p class="text-mute" style="color: transparent;">Best product and collections</p>
                  </div>
                </div>
              </a>
            </div>
              <?php } ?>
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination white-pagination text-left mb-3"></div>
        </div>
      </div>
    </div>
</section>
  <script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 'auto',
      spaceBetween: 0,
      pagination: {
          el: '.swiper-pagination',
          clickable: true,
      },
    });

    $('.background').each(function () {
        var imgpath = $(this).find('img');
        $(this).css('background-image', 'url(' + imgpath.attr('src') + ')');
        imgpath.hide();
    })

  </script>
  <script type="text/javascript">
  $(document).ready(function(){

      $('#row1').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
      });

      $('#row3').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        autoplay: true,
        autoplaySpeed: 3000,
      });

  });
</script>