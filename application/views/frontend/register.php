<style type="text/css">
    a:hover {
        color: black !important;
    }
</style>

<div class="customer_login mt-32">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="display: block;text-align: center">
                <?php 
                    if($this->session->flashdata('warning')) {
                      echo '<div class="alert alert-warning">';
                      echo $this->session->flashdata('warning');
                      echo '</div>';
                    }
                ?>
                <form method="post" action="<?= site_url('actregis')?>" id="formlogin">
                    <div class="row" style="padding: 1em;">
                        <h3>Register</h3>
                    </div>
                        <hr>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control input-mobile" required>
                    </div>
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="first_name" class="form-control input-mobile" required>
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="first_name" class="form-control input-mobile">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control input-mobile" required>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" name="phone" class="form-control input-mobile" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control input-mobile" required>
                    </div>
                    <div class="form-group">
                        <button style="color: white !important;" type="submit" class="btn btn-warning btn-block btn-lg">Submit</button>
                        <div class="row" style="margin: 1em 0em">
                            <div class="col-md-5 col-sm-5 col-xs-5"><hr></div>
                            <div class="col-md-2 col-sm-2 col-xs-2" style="text-align: center;">OR</div>
                            <div class="col-md-5 col-sm-5 col-xs-5"><hr></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="text-align: center;margin-bottom: .5em;">
                                <a href="<?= site_url('fblogin')?>">
                                    <img style="border-radius: 5px;border: 1px solid grey;" src="<?= base_url('assets/webuild/img/rgs-fb.jpg')?>">
                                </a>
                            </div>
                            <div class="col-md-6" style="text-align: center;margin-bottom: .5em;">
                                <a href="<?= site_url('glogin')?>" >
                                    <img style="border-radius: 5px;border: 1px solid grey;" src="<?= base_url('assets/webuild/img/rgs-google.jpg')?>">
                                </a>
                            </div>
                         </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>