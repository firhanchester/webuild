<header class="header_area">
    <!--header top start-->
    <div class="header_top">
        <div class="container">  
            <div class="top_inner">
                <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <div class="follow_us">
                        <label>Follow Us:</label>
                        <ul class="follow_link">
                            <li><a target="_blank" href="https://facebook.com"><i class="ion-social-facebook"></i></a></li>
                            <li><a target="_blank" href="https://twitter.com"><i class="ion-social-twitter"></i></a></li>
                            <li><a target="_blank" href="https://instagram.com"><i class="ion-social-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="top_right text-right">
                        <ul>
                            <?php if($login == ''){?>
                            <li class="top_links"><a href="<?= site_url('login')?>">Login</a></li> 
                            <?php } else { ?>
                                <li class="top_links"><a href="#"><i class="ion-android-person"></i> Hi , <?= $login['username']?><i class="ion-ios-arrow-down"></i></a>
                                    <ul class="dropdown_links">
                                        <li><a href="<?= site_url('my-order')?>">My Order</a></li>
                                        <li><a href="<?= site_url('my-account')?>">My Account</a></li>
                                        <li><a href="<?= site_url('logout')?>">Logout</a></li>
                                    </ul>
                                </li> 
                            <?php } ?>
                        </ul>
                    </div>   
                </div>
            </div>
            </div>
        </div>
    </div>
    <!--header top start-->
    <!--header middel start-->
    <div class="header_middle">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-6">
                    <div class="logo">
                        <a href="<?= site_url()?>"><img src="<?= base_url()?>assets/webuild/img/logo/webuild-1.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-6">
                    <div class="middel_right">
                        <div class="search-container">
                           <form action="<?= site_url('search')?>" method="get">
                                <div class="search_box">
                                    <input placeholder="Search" type="text" name="search">
                                    <button type="submit"><i class="ion-ios-search-strong"></i></button> 
                                </div>
                            </form>
                        </div>
                        <div class="middel_right_info">

                            <div class="mini_cart_wrapper">
                                <a href="javascript:void(0)"><span class="fa fa-bell-o"></span></a>
                                <?php if($login != ''){?>
                                    <span class="cart_quantity"><?= $jml_notif?></span>
                                    <!--mini cart-->
                                     <div class="mini_cart">
                                        <?php 
                                        if(empty($notif)){
                                            echo("<center><span>You don't have any notifications </span></center>");
                                        }else{

                                        foreach($notif as $ntf){ ?>
                                            
                                            <div class="cart_item <?= ($ntf['status'] == 0) ? 'opened' : '' ;?>">
                                                <div class="cart_info">
                                                    <a onclick="link(<?= $ntf['id'] ?>)">
                                                        <?= $ntf['title']?>
                                                    </a>
                                                    <span><?= date('d M Y - H:i' ,strtotime($ntf['created_date']))?></span>
                                                </div>
                                            </div>
                                        <?php }} ?>
                                    </div>
                                    <!--mini cart end-->
                                <?php } ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--header middel end-->
    <!--header bottom satrt-->
    <div class="header_bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="main_menu header_position"> 
                        <nav>  
                            <ul>
                                <li><a href="<?= site_url()?>">home</a></li>
                                <li><a href="<?= site_url('about')?>">about Us</a></li>
                                <li><a href="<?= site_url('home_owner')?>">home owner</a></li>
                                <li><a href="<?= site_url('project')?>">Project</a></li>
                                <li><a href="<?= site_url('article')?>">news</a></li>
                                <li><a href="<?= site_url('contact')?>">contact</a></li>
                            </ul>  
                        </nav> 
                    </div>
                </div>
               
            </div>
        </div>
    </div>
    <!--header bottom end-->
 
</header>
    <!--header area end-->
     <!--Offcanvas menu area start-->
    <div class="off_canvars_overlay"></div>
    <div class="Offcanvas_menu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="canvas_open">
                        <a href="javascript:void(0)"><i class="ion-navicon"></i></a>
                    </div>
                    <div class="Offcanvas_menu_wrapper">

                        <div class="canvas_close">
                              <a href="#"><i class="ion-android-close"></i></a>  
                        </div>
                        
                        <div class="top_right text-right">
                            <ul>
                                <?php if($login == ''){?>
                                <li class="top_links">
                                    <a href="<?= site_Url('login')?>"><i class="ion-android-person"></i> Login</a>
                                </li>
                                <?php }else{ ?> 
                                <li class="top_links">
                                <a href="#">
                                    <i class="ion-android-person"></i> Hi, <?= $login['username']?><i class="ion-ios-arrow-down"></i></a>
                                    <ul class="dropdown_links">
                                        <li><a href="<?= site_url('my-account')?>">My Account</a></li>
                                        <li><a href="<?= site_url('logout')?>">Logout</a></li>
                                    </ul>
                                </li>
                                <?php } ?> 
                            </ul>
                        </div>
                        <div class="Offcanvas_follow">
                            <label>Follow Us:</label>
                            <ul class="follow_link">
                                <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="#"><i class="ion-social-instagram"></i></a></li>
                            </ul>
                        </div>
                        <div class="search-container">
                           <form action="#">
                                <div class="search_box">
                                    <input placeholder="Search entire store here ..." type="text">
                                    <button type="submit"><i class="ion-ios-search-strong"></i></button> 
                                </div>
                            </form>
                        </div>
                        <div id="menu" class="text-left ">
                            <ul class="offcanvas_main_menu">
                                <li class="menu-item-has-children">
                                    <a href="<?= site_url('')?>"><i style="margin-right: 1em;" class="fa fa-home"></i>Home</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?= site_url('about')?>"><i style="margin-right: 1em;" class="fa fa-users"></i>About Us</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?= site_url('home_owner')?>"><i style="margin-right: 1em;" class="fa fa-home"></i>Home Owner</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?= site_url('project')?>"><i style="margin-right: 1em;" class="fa fa-wrench"></i> Project</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?= site_url('article')?>"><i style="margin-right: 1em;" class="fa fa-newspaper-o"></i>News</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?= site_url('about')?>"><i style="margin-right: 1em;" class="fa fa-address-card"></i>Contact</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?= site_url('my-order')?>"><i style="margin-right: 1em;" class="fa fa-folder"></i>My Order</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>