<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="display: block;text-align: center">
				<form style="display: inline-block;text-align: left;width: 80%">
          <input type="hidden" name="cat" value="design">
          <input type="hidden" name="customer_id" value="<?= $login['id']?>">
					<div class="form-group">
						<h4>Location</h4>
					</div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                  <label>Project</label>
                  <input type="text" name="project" class="form-control" readonly value="<?= $type?>">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                  <label>Province</label>
                  <select required class="form-control" id="province" name="province" onchange="dataCity(this)">
                      <option value=""> Province </option>
                  </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                  <label>City</label>
                  <select required class="form-control" id="city" name="city">
                      <option> City </option>
                  </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Request Detail</label>
                <textarea class="form-control" name="req_detail" rows="10" required></textarea>
              </div>
            </div>
          </div>
					<div class="form-group">
						<button type="button" onclick="proceed()" class="btn btn-warning btn-block btn-lg">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$(document).ready(function(){
    $.ajax({
        url: "<?php echo site_url('admin/project/province')?>",
        type: "post",
        success: function(data){
            $('#province').html('<option value=""> Province </option>');
            $('#province').append(data)
        },
        error: function(error) {
            console.log(error);
        }
    });
  })

  function dataCity(pro){
      var x = $('#province').children(":selected").attr("id");
      $.ajax({
          url: "<?php echo site_url('admin/project/city')?>",
          type: "post",
          data : {province_id : x},
          success: function(data){
              $('#city').html('<option value=""> City </option>');
              $('#city').append(data);
          },
          error: function(error) {
              console.log(error);
          }
      });
    }

    function proceed(){
      if ($('#province').val() == "" || $('#city').val() == "" || $('textarea[name=req_detail]').val() == "") {
          swal("WARNING!", "Please complete the form", "warning");
      }
      else
      {
        var detail = {
            'customer_id': $('input[name=customer_id]').val(),
            'type': $('input[name=project]').val(),
            'province': $('#province').find(":selected").text(),
            'city': $('#city').find(":selected").text(),
            'req_detail': $('textarea[name=req_detail]').val(),
        }

        console.log(detail);

        $.ajax({
            url: "<?php echo site_url('project/proceed_improve'); ?>",
            type: "POST",
            data: detail,
            success: function(status){
              if(status.success == true){
                  console.log(status);
                  var x = status.data;
                  window.location.href='<?= site_url("request_improve/"); ?>' + x;
              }else{
                  console.log(status);
              }
            },
            error: function(error) {
                console.log(error);
            }
        });
      }
    }

</script>