<style type="text/css">
  .card {
      border: 0 !important;
    }
</style>
<section id="portofolio" style="margin: 2em 0em;">
    <div class="container">
      <div style="margin: 1em 0em;text-align : center;">
          <span style="font-weight: bold;">Looking For Home Improvement / Project? ?</span>
          <hr>
      </div>

      <div class="row">
        <!--  -->
        <?php for($i = 0; $i < count($fitur); $i++) {?>
        <div class="col-md-3 col-sm-6 col-xs-6">
          <a href="<?= site_url('project/improve/'.$link[$i])?>">
            <div class="card">
                <div class="card-content-home">
                  <h4 class="margin-0"><?= $fitur[$i] ?></h4>
                </div>
            </div>
          </a>
        </div>
        <?php } ?>
        <!--  -->
      </div>

    </div>
</section>