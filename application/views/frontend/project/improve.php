<section id="portofolio" style="margin: 3em 2em 5em 2em;">
    <div class="container">
      <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-6">
            <h2 style="text-transform: uppercase;"><?= $improve['name']?></h2>
            
          </div>
      </div>
      <?php foreach($improve['component'] as $i){?>
        <div class="row" style="margin: 1em 0em;">

          <div class="col-md-4">
            <img src="<?= base_url('uploads/fimprove/'.$i['image'])?>">
          </div>

          <div class="col-md-8">
            <p>
              <?= ucfirst($i['title']) ?>
            </p>
            <p>
              <?= $i['description'] ?>
            </p>
          </div>


        </div>
      <?php } ?>
        <div style="text-align: center">
          <a href="<?= site_url('project/request/'.$improve['name'])?>" class="btn btn-warning"><i class="fa fa-plus"></i> Request Your Project</a>
        </div>
    </div>
</section>