<style type="text/css">
    .slick-prev:before, .slick-next:before {
        color: white !important;
        font-family: 'marv' !important;
        font-weight: bold;
    }
    .slick-prev:before {
        font-size: 1em;
        width: 0;
        height: 0;
        border-top: 20px solid transparent;
        border-right: 30px solid white;
        border-bottom: 20px solid transparent;
    }
    .slick-next:before {
        font-size: 1em;
        width: 0;
        height: 0;
        border-top: 20px solid transparent;
        border-left: 30px solid white;
        border-bottom: 20px solid transparent;
    }
    .slick-prev {
        left:5%;
        z-index: 9;
    }

    .slick-next {
        right: 5%;
    }

    .slick-slide {
        overflow: hidden;
        margin: 50px auto;
        position: relative;
        cursor: zoom-in;
    }

    img {
        width: 100%;
        float: left;
    }
    .zoom  {
        position: absolute;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="slider slider-for">
                <div>
                    <img src="<?= base_url('assets/webuild/img/product/classic.jpg')?>">
                </div>
                <div>
                    <img src="<?= base_url('assets/webuild/img/product/classic.jpg')?>">
                </div>
                <div>
                    <img src="<?= base_url('assets/webuild/img/product/classic.jpg')?>">
                </div>
                <div>
                    <img src="<?= base_url('assets/webuild/img/product/classic.jpg')?>">
                </div>
                <div>
                    <img src="<?= base_url('assets/webuild/img/product/classic.jpg')?>">
                </div>
            </div>
            <div class="slider slider-nav">
                <div>
                    <img style="width: 100%;" src="<?= base_url('assets/webuild/img/product/classic.jpg')?>">
                </div>
                <div>
                    <img style="width: 100%;" src="<?= base_url('assets/webuild/img/product/classic.jpg')?>">
                </div>
                <div>
                    <img style="width: 100%;" src="<?= base_url('assets/webuild/img/product/classic.jpg')?>">
                </div>
                <div>
                    <img style="width: 100%;" src="<?= base_url('assets/webuild/img/product/classic.jpg')?>">
                </div>
                <div>
                    <img style="width: 100%;" src="<?= base_url('assets/webuild/img/product/classic.jpg')?>">
                </div>  
            </div>
        </div>
    </div>

    <div class="product_d_info" style="margin: 2em 0em;">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="product_d_inner">   
                        <div class="product_info_button">    
                            <ul class="nav" role="tablist">
                                <li >
                                    <a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Description</a>
                                </li>
                                <li>
                                     <a data-toggle="tab" href="#sheet" role="tab" aria-controls="sheet" aria-selected="false">Specification</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="info" role="tabpanel" >
                                <div class="product_info_content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.</p>
                                    <p>Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo augue nisi non neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor, lorem et placerat vestibulum, metus nisi posuere nisl, in accumsan elit odio quis mi. Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget.</p>
                                </div>    
                            </div>
                            <div class="tab-pane fade" id="sheet" role="tabpanel" >
                                <div class="product_d_table">
                                   <form action="#">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td class="first_child">Compositions</td>
                                                    <td>Polyester</td>
                                                </tr>
                                                <tr>
                                                    <td class="first_child">Styles</td>
                                                    <td>Girly</td>
                                                </tr>
                                                <tr>
                                                    <td class="first_child">Properties</td>
                                                    <td>Short Dress</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div class="product_info_content">
                                    <p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman's wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>
                                </div>    
                            </div>

                        </div>
                    </div>     
                </div>
            </div>
            <div class="row" style="margin: 2em 0em;">
                <div class="col-md-12">
                    <div style="text-align: center">
                        <h3>Wanna Your Own Build?</h3>
                        <p>Please Submit Your Design</p>
                        <p>We Will Get Back To You</p>
                        <a href="<?= site_url('home_owner')?>" class="btn btn-warning">Submit My Design</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        arrows: true,
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        centerMode: true,
        focusOnSelect: true,
        responsive: [{
            breakpoint: 1024,
            settings: {
            slidesToShow: 3,
            infinite: true
            }

            }, {

            breakpoint: 600,
            settings: {
            slidesToShow: 2,
            }

            }, {

            breakpoint: 300,
            settings: "unslick" // destroys slick

        }]
    });

    $('.slider-for').mouseenter(function(){
    var src = $(this).find('img').attr("src");
    $(this).append('<img class="zoom" src="'+src+'" >');
      
        $(this).mousemove(function(event){
          
            var offset = $(this).offset();
            var left = event.pageX - offset.left;
            var top = event.pageY - offset.top;
          
            $(this).find('.zoom').css({
              width: '200%',
              opacity: 1,
              left: -left,
              top: -top
            })
        });
    });

    $('.slick-slide').mouseleave(function(){                     
       $(this).find('.zoom').css({
         width: '100%',
         opacity: 0,
         left: 0,
         top: 0
       })                               
     });
</script>