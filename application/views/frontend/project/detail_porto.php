<style type="text/css">
    .slick-prev:before, .slick-next:before {
        color: white !important;
        font-family: 'marv' !important;
        font-weight: bold;
    }
    .slick-prev:before {
        font-size: 1em;
        width: 0;
        height: 0;
        border-top: 20px solid transparent;
        border-right: 30px solid white;
        border-bottom: 20px solid transparent;
    }
    .slick-next:before {
        font-size: 1em;
        width: 0;
        height: 0;
        border-top: 20px solid transparent;
        border-left: 30px solid white;
        border-bottom: 20px solid transparent;
    }
    .slick-prev {
        left:5%;
        z-index: 9;
    }

    .slick-next {
        right: 5%;
    }

    .kanan {
        margin: 0 0 0 15%;
    }

    .slick-slide {
        overflow: hidden;
        margin: 50px auto;
        position: relative;
    }

    img {
        width: 100%;
        /*float: left;*/
    }

    .sub-row {
        font-weight: 500;
        margin-top: 1em;
    }

    .satu {
        font-size: 1.5em;
    }

    .dua {
        font-size: 2em;
    }

    #gmbr {
        padding: 0 5%;
    }

    @media (min-width:150px) and (max-width:840px) {
        .slick-prev:before {
            font-size: 1em;
            width: 0;
            height: 0;
            border-top: 10px solid transparent;
            border-right: 20px solid white;
            border-bottom: 10px solid transparent;
        }
        .slick-next:before {
            font-size: 1em;
            width: 0;
            height: 0;
            border-top: 10px solid transparent;
            border-left: 20px solid white;
            border-bottom: 10px solid transparent;
        }
        .slick-prev {
            left:3%;
            z-index: 9;
        }

        .slick-next {
            right: 3%;
        }

        .kanan {
            margin: 5% 0 0 5%
        }
    }

    
</style>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="product-details-tab" id="gmbr">

                <div id="img-1" class="zoomWrapper single-zoom">
                    <a target="_blank" href="<?= base_url('uploads/portofolio/'.$porto['image'][0]['image'])?>">
                        <img id="zoom1" style="width: 100%" src="<?= base_url('uploads/portofolio/'.$porto['image'][0]['image'])?>" data-zoom-image="<?= base_url('uploads/portofolio/'.$porto['image'][0]['image'])?>" alt="big-1">
                    </a>
                </div>

                <div class="single-zoom-thumb">
                    <ul class="s-tab-zoom owl-carousel single-product-active" id="gallery_01">
                        <?php foreach($porto['image'] as $image){?>
                        <li>
                            <a target="_blank" href="<?= base_url('uploads/portofolio/'.$image['image'])?>" class="elevatezoom-gallery active" data-update="" data-image="<?= base_url('uploads/portofolio/'.$image['image'])?>" data-zoom-image="<?= base_url('uploads/portofolio/'.$image['image'])?>">
                                <img src="<?= base_url('uploads/portofolio/'.$image['image'])?>" alt="zo-th-1"/>
                            </a>

                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
            <div class="kanan">
            <div style="font-weight: bold;font-size: 2.5em;"><?= $porto['porto']['title']?></div>
            <div class="sub-row dua">Rp <?= number_format($porto['porto']['price'])?></div>
            <div class="sub-row satu"><?= $porto['porto']['city']?> , <?= $porto['porto']['province']?></div>
            <div class="product_d_info" style="margin: 2em 0em;">
                <div class="row">
                    <div class="product_d_inner">   
                        <div class="product_info_button">    
                            <ul class="nav" role="tablist">
                                <li >
                                    <a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Description</a>
                                </li>
                                <li>
                                     <a data-toggle="tab" href="#sheet" role="tab" aria-controls="sheet" aria-selected="false">Specification</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="info" role="tabpanel" >
                                <div class="product_info_content">
                                    <p><?= $porto['porto']['description']?></p>
                                </div>    
                            </div>
                            <div class="tab-pane fade" id="sheet" role="tabpanel" >
                                <div class="product_d_table">
                                   <form action="#">
                                    <table>
                                    <tbody>
                                        <!-- <tr>
                                            <td class="first_child"><?= $comp['component']?></td>
                                            <td><?= $comp['spec']?></td>
                                        </tr> -->
                                        <?php foreach($porto['comp'] as $comp){?>
                                        <tr>
                                            <td class="first_child"><?= $comp['component']?></td>
                                            <td><?= $comp['spec']?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>     
                </div>
                <div class="row" style="margin: 1em 0em;">
                    <div class="col-md-12">
                        <hr>
                        <div style="text-align: center">
                            <h3>Wanna Your Own Build?</h3>
                            <a style="margin-top: 1em" href="<?= site_url('home_owner')?>" class="btn btn-warning">Submit My Design</a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>

    

</div>

<script type="text/javascript">
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        arrows: true,
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        centerMode: true,
        focusOnSelect: true,
        responsive: [{
            breakpoint: 1024,
            settings: {
            slidesToShow: 3,
            infinite: true
            }

            }, {

            breakpoint: 600,
            settings: {
            slidesToShow: 2,
            }

            }, {

            breakpoint: 300,
            settings: "unslick" // destroys slick

        }]
    });

</script>