<style type="text/css">
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }

    .table-responsive table thead{
        background-color: #ffd54c;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }

    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }

    [type=radio] { 
      position: absolute;
      opacity: 0;
      width: 0;
      height: 0;
    }

    /* IMAGE STYLES */
    [type=radio] + img {
      cursor: pointer;
      padding: 1em;
    }

    /* CHECKED STYLES */
    [type=radio]:checked + img {
      outline: 1px solid black;
    }

    #file {
        border : 0px;
        padding: 0px;
    }
    @media screen and (max-width:640px) {
        .mbl {
            width: 60% !important;
        }
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="text-align: left;">
            <h3 style="margin-bottom: 1em;">
                Checkout
            </h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="container">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td class="text-center mbl"><strong>Description</strong></td>
                                        <td class="text-center"><strong>Price</strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Biaya Pembuatan Design</td>
                                        <td class="text-right">Rp 400.000 </td>
                                    </tr>
                                    <tr>
                                        <td class="thick-line text-right"><strong>Subtotal</strong></td>
                                        <td class="thick-line text-right">Rp 400.000 </td>
                                    </tr>
                                    <tr>
                                        <td class="no-line text-right"><strong>Tax (10%)</strong></td>
                                        <td class="no-line text-right">Rp 40.000 </td>
                                    </tr>
                                    <tr id="rowdiscount">
                                        <td class="no-line text-right"><strong>Discount</strong></td>
                                        <td class="no-line text-right">
                                            <span id="discount">Rp 0 </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="no-line text-right"><strong>Total</strong></td>
                                        <td class="no-line text-right">
                                            <span id="total">Rp 440.000 </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group mb-3">
                        <input type="text" id="voucher" class="form-control" placeholder="Voucher" aria-label="Voucher" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-warning" type="button" onclick="voucher()">Apply Voucher</button>
                        </div>
                    </div>
                    <div style="margin-bottom: 1em;">
                        <h4>Choose Bank</h4>
                    </div>
                    <form action="" method="post">
                        <input type="hidden" name="code" id="code" value="<?= $project['code']?>">
                        <input type="hidden" name="customer_id" id="customer_id" value="<?= $project['customer_id']?>">
                        <input type="hidden" name="voucher_real" id="voucher_real">
                        <label>
                          <input type="radio" name="bank" value="bca" checked>
                          <img src="<?= base_url()?>/assets/webuild/img/icon/bca.png">
                        </label>
                        <label>
                          <input type="radio" name="bank" value="bni">
                          <img src="<?= base_url()?>/assets/webuild/img/icon/bni.png">
                        </label>
                        <label>
                          <input type="radio" name="bank" value="bri">
                          <img src="<?= base_url()?>/assets/webuild/img/icon/bri.png">
                        </label>
                        <label>
                          <input type="radio" name="bank" value="cimb">
                          <img src="<?= base_url()?>/assets/webuild/img/icon/cimb.png">
                        </label>
                        <div class="form-group" style="margin-top: 2em;">
                            <button type="button" onclick="next()" style="color: white;" class="btn btn-warning btn-lg btn-block">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#rowdiscount').hide();

    function voucher(){
        swal({
          title: "Loading",
          text: "Please wait a second",
          icon: "warning",
          buttons: false,
          dangerMode: true,
          closeOnClickOutside: false,
        });

        var voucher = $('#voucher').val();

        var form = {
            'voucher' : voucher,
        }

        $.ajax({
            url: "<?php echo site_url('voucher')?>",
            type: "post",
            data : form,
            success: function(data){
                console.log(data)
                var d = data.data;
                var nominal = d.nominal;
                if(data.status == true){
                    $('#voucher_real').val(voucher);
                    if(data.data.type == 'persen'){
                        var dc = 440000*nominal/100;
                        var totdc = 440000-dc;
                        $('#discount').text('Rp '+ dc.toLocaleString('en'));
                        $('#total').text('Rp '+ totdc.toLocaleString('en'));
                    }else{
                        var totdc = 440000-nominal;
                        nominal = nominal-0;
                        $('#discount').text('Rp '+ nominal.toLocaleString('en'));
                        $('#total').text('Rp '+ totdc.toLocaleString('en'));
                    }
                    swal.close();
                    $('#rowdiscount').show();
                }else{
                    swal(data.msg, {
                      buttons: false,
                      timer: 3000,
                    });
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function next(){
        var form = {
            'code' : $('input[name=code]').val(),
            'customer_id' : $('input[name=customer_id]').val(),
            'bank' : $('input[name=bank]:checked').val(),
            'from' : 'design',
            'voucher' : $('#voucher_real').val(),
        }
        
        swal({
          title: "Loading",
          text: "Please wait a second",
          icon: "warning",
          buttons: false,
          dangerMode: true,
          closeOnClickOutside: false,
        });

        $.ajax({
            url: "<?php echo site_url('create_invoice')?>",
            type: "post",
            data : form,
            success: function(data){
                console.log(data)
                if(data.success == true){
                    swal.close();
                    window.location.href='<?= site_url('invoice_design/')?>' + data.code;
                }else{
                    swal("Something went wrong, please try again", {
                      buttons: false,
                      timer: 3000,
                    });
                    window.location.href='<?= site_url('invoice_design/')?>' + data.code;
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }
</script>