<footer class="footer_widgets" style="background-color: #ffd54c !important;margin-top: 2em;">
    <div class="container web">  
        <div class="footer_top">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="widgets_container contact_us">
                        <div class="footer_logo">
                            <a href="#"><img src="<?= base_url()?>assets/webuild/img/logo/webuild-1.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="contact_message content">
                        <h3>contact us</h3>    
                        <ul>
                            <li><i class="fa fa-envelope-o"></i><a href="#">webuild.ptiki@gmail.com</a></li>
                            <li><i class="fa fa-phone"></i> <a href="tel:02187678987">(021)- 87678987</a></li>
                            <li><i class="fa fa-fax"></i>  Address : Jakarta indonesia</li>
                        </ul>             
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                   <div class="widgets_container">
                        <h3>Newsletter Subscribe</h3>
                        <p>We’ll never share your email address with a third-party.</p>
                        <div class="subscribe_form">
                            <form id="mc-form" class="mc-form footer-newsletter" >
                                <input id="mc-email" type="email" autocomplete="off" placeholder="Enter you email address here..." />
                                <button id="mc-submit">Subscribe</button>
                            </form>
                            <!-- mailchimp-alerts Start -->
                            <div class="mailchimp-alerts text-centre">
                                <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                            </div><!-- mailchimp-alerts end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bottom">
           <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="copyright_area">
                        <p style="text-align: center;">Copyright &copy; 2020 <a href="#">Webuild</a>  All Right Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mobile">  
        <div class="footer_top">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="widgets_container contact_us">
                        <div class="footer_logo">
                            <a href="#"><img src="<?= base_url()?>assets/webuild/img/logo/webuild-1.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="contact_message content" style="text-align: center;">
                        <hr>
                        <h3>contact us</h3>    
                        <ul>
                            <li><i class="fa fa-envelope-o"></i><a href="#">webuild.ptiki@gmail.com</a></li>
                            <li><i class="fa fa-phone"></i> <a href="tel:02187678987">(021)- 87678987</a></li>
                            <li><i class="fa fa-fax"></i>  Address : Jakarta indonesia</li>
                        </ul>             
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                   <div class="widgets_container" style="text-align: center;">
                        <h3>Newsletter Subscribe</h3>
                        <div class="subscribe_form">
                            <form id="mc-form" class="mc-form footer-newsletter" >
                                <input id="mc-email" type="email" autocomplete="off" placeholder="Enter you email address here..." />
                                <button id="mc-submit">Subscribe</button>
                            </form>
                            <!-- mailchimp-alerts Start -->
                            <div class="mailchimp-alerts text-centre">
                                <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                            </div><!-- mailchimp-alerts end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bottom">
           <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="copyright_area">
                        <p style="text-align: center;">Copyright &copy; 2020 <a href="#">Webuild</a>  All Right Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
    <!--footer area end-->
    <!-- modal area start-->
    <script src="<?= base_url()?>assets/webuild/js/plugins.js"></script>
    <script src="<?= base_url()?>assets/webuild/js/main.js"></script>
    <script type="text/javascript">
        function link(id){
            $.ajax({
                url: "<?php echo site_url('view_notif')?>",
                type: "post",
                data : {id : id},
                success: function(res){
                    // console.log(res);
                    window.location.href = res.link;
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
    </script>
    </body>
</html>