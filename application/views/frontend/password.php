<style type="text/css">
    a:hover {
        color: black !important;
        background-color: white !important;
    }
</style>

<div class="customer_login mt-32">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="display: block;text-align: center">
                <?php 
                    if($this->session->flashdata('warning')) {
                      echo '<div class="alert alert-warning">';
                      echo $this->session->flashdata('warning');
                      echo '</div>';
                    }
                ?>
                <form method="post" action="<?= site_url('proceed-password')?>" id="formlogin">
                    <div class="row">
                        <div class="col-md-12" style="text-align: center">
                            <span style="font-weight: bold;">Input Password</span>
                            <hr>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="hidden" name="id" value="<?= $user['user_id']?>">
                        <input type="text" name="password" id="password" minlength="8" required class="form-control input-mobile">
                    </div>
                    <div class="form-group">
                        <button style="color: white !important;" type="submit" class="btn btn-warning btn-block btn-lg">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>