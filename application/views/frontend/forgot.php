<style type="text/css">
    a:hover {
        color: black !important;
        background-color: white !important;
    }
</style>

<div class="customer_login mt-32">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="display: block;text-align: center">
                <?php 
                    if($this->session->flashdata('warning')) {
                      echo '<div class="alert alert-warning">';
                      echo $this->session->flashdata('warning');
                      echo '</div>';
                    }
                ?>
                <form method="post" action="<?= site_url('actlogin')?>" id="formlogin">
                    <div class="row">
                        <div class="col-md-12" style="text-align: center">
                            <span style="font-weight: bold;">Forgot Password</span>
                            <hr>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" id="email" value="" class="form-control input-mobile">
                    </div>
                    <div class="form-group">
                        <button style="color: white !important;" type="button" class="btn btn-warning btn-block btn-lg" onclick="sbmt()">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function sbmt(){
        var email = $('#email').val();
        if(email == ''){
            swal("WARNING!", "Masukkan Email Anda", "warning");
        }else{
            $.ajax({
                url: "<?php echo site_url('proceed-forgot')?>",
                type: "post",
                data : {email : email},
                success: function(data){
                    console.log(data);
                    if(data.success == false){
                        swal("WARNING!", "Email Tidak Terdaftar", "warning");
                    }else{
                        swal("Success", "Silahkan Cek Email anda", "success");
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

    }
</script>