    <footer class="footer_widgets" style="background-color: #ffd54c !important;margin-top: 4em;">
    <div class="container">  
        <div class="footer_bottom">
           <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="copyright_area">
                        <p style="text-align: center;">Copyright &copy; 2020 <a href="#">Webuild</a>  All Right Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
    <script src="<?= base_url()?>assets/webuild/js/plugins.js"></script>
    <script src="<?= base_url()?>assets/webuild/js/main.js"></script>
    <script type="text/javascript">
        function link(id){
            $.ajax({
                url: "<?php echo site_url('view_notif')?>",
                type: "post",
                data : {id : id},
                success: function(res){
                    // console.log(res);
                    window.location.href = res.link;
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
    </script>
    </body>
</html>