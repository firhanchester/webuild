<style type="text/css">
	.invoice-title h2, .invoice-title h3 {
	    display: inline-block;
	}

    .table-responsive table thead{
        background-color: #ffd54c;
    }

	.table > tbody > tr > .no-line {
	    border-top: none;
	}

	.table > thead > tr > .no-line {
	    border-bottom: none;
	}

	.table > tbody > tr > .thick-line {
	    border-top: 2px solid;
	}

    [type=radio] { 
      position: absolute;
      opacity: 0;
      width: 0;
      height: 0;
    }

    /* IMAGE STYLES */
    [type=radio] + img {
      cursor: pointer;
      padding: 3em;
    }

    /* CHECKED STYLES */
    [type=radio]:checked + img {
      outline: 1px solid black;
    }

    #file {
        border : 0px;
        padding: 0px;
    }
</style>
<div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
    		<div class="invoice-title">
            <h3>Invoice - <?= $invoice['code'] ?></h3>
            <input type="hidden" name="total" value="<?= number_format($invoice['total'])?>">
            <input type="hidden" name="code" value="<?= $invoice['code'] ?>">
    		</div>
    		<hr>
    	</div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="invoice-title">
            <table class="table">
              <thead style="background-color: #ffd54c;">
                    <tr>
                        <td colspan="2"><span style="font-size: 20px;font-weight: bold">Tagihan Pembayaran</span></td>
                    </tr>
                </thead>
              <tr>
                <td class="no-line" style="font-weight: bold">Bank</td>
                <td class="no-line" style="font-weight: bold">: <?= strtoupper($invoice['bank']) ?> an Webuild Indonesia</td>
              </tr>
              <tr>
                <td class="no-line" style="font-weight: bold">No Rekening</td>
                <td class="no-line" style="font-weight: bold">: 0000 111 222</td>
              </tr>
              <tr>
                <td class="no-line" style="font-weight: bold">Total</td>
                <td class="no-line" style="font-weight: bold">: Rp <?= number_format($invoice['total']) ?></td>
              </tr>
              <tr>
                <td class="no-line" style="font-weight: bold">Status</td>
                <?php if($invoice['status'] <= 1) {?>
                  <td class="no-line" style="font-weight: bold">: Unpaid</td>
                <?php } else {?>
                  <td class="no-line" style="font-weight: bold">: Paid</td>
                <?php }?>
              </tr>
              <tr>
                <td class="no-line" style="font-weight: bold">Transaction Date</td>
                <td class="no-line" style="font-weight: bold">: 
                  <?= date('d M Y - H:i' ,strtotime($invoice['created_date']))?>
                </td>
              </tr>
            </table>
        </div>
      </div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
        <hr>
    		<div class="panel panel-default">
    			<div class="panel-body">
    				<div class="table-responsive">
    					
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <td class="text-center mbl"><strong>Description</strong></td>
                        <td class="text-center"><strong>Price</strong></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Biaya Pembuatan Design</td>
                        <td class="text-right">Rp 400.000 </td>
                    </tr>
                    <tr>
                        <td>Biaya Jasa Perhitungan Pembangunan</td>
                        <td class="text-right">Rp 200.000 </td>
                    </tr>
                    <tr>
                        <td class="thick-line text-right"><strong>Subtotal</strong></td>
                        <td class="thick-line text-right">Rp 600,000 </td>
                    </tr>
                    <tr>
                        <td class="no-line text-right"><strong>Tax (10%)</strong></td>
                        <td class="no-line text-right">Rp <?= number_format($invoice['tax']) ?> </td>
                    </tr>
                    <?php if($invoice['discount'] > 0){?>
                      <tr>
                        <td class="no-line text-right"><strong>Discount</strong></td>
                        <td class="no-line text-right">Rp <?= number_format($invoice['discount']) ?> </td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td class="no-line text-right"><strong>Kode Unik</strong></td>
                        <td class="no-line text-right">Rp <?= number_format($invoice['uniq']) ?> </td>
                    </tr>
                    <tr>
                        <td class="no-line text-right"><strong>Total</strong></td>
                        <td class="no-line text-right">Rp <?= number_format($invoice['total']) ?> </td>
                    </tr>
                </tbody>
            </table>
              
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div style="float: right">
          <?php if($invoice['status'] == 0) {?>
            <button style="margin: .5em 0;" type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalCenter2">
              Upload Bukti Transfer
            </button>
          <?php } else {?>
            <a href="<?= site_url('generate/dnb/'.$invoice['code'])?>" style="margin: .5em 0;margin-right: 1em;" type="button" class="btn btn-warning">
              Download
            </a>
            <button style="margin: .5em 0;" type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalCenter3">
              Lihat Bukti Transfer
            </button>
          <?php }?>
        </div>
      </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Upload Bukti Transfer</h4>
      </div>
      <div class="modal-body" style="text-align: center;">
            <div class="col-md-12" style="display: block;text-align: center">
                <form style="display: inline-block;text-align: left;width: 95%" method="post" action="<?= site_url('invoice/upload')?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                              <label>Bank Pengirim</label>
                              <input type="text" name="payment" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                              <label>Nama Pengirim</label>
                              <input type="text" name="bank_account" class="form-control">
                          </div>
                        </div>
                      </div>
                    <div class="form-group">
                        <label for="staticEmail">Bukti Transfer</label>
                        <input type="hidden" name="invoice_id" value="<?= $invoice['id']?>">
                        <input type="hidden" name="invoice" value="dnb">
                        <input type="hidden" name="code" value="<?= $invoice['code']?>">
                        <input type="file" name="attachment" class="form-control" id="file">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-warning btn-block" type="submit">Submit</button>
                    </div>
                </form>
            </div>
      </div>
      <div class="modal-footer" style="background-color: white !important;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Bukti Transfer</h4>
      </div>
      <div class="modal-body" style="text-align: center;">
            <div class="col-md-12" style="display: block;text-align: center">
                <form style="display: inline-block;text-align: left;width: 95%" method="post" action="<?= site_url('invoice/upload')?>" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Bank Pengirim</label>
                            <input type="text" name="payment" class="form-control" value="<?= strtoupper($invoice['bank'])?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama Pengirim</label>
                            <input type="text" name="bank_account" class="form-control" value="<?= strtoupper($invoice['bank_account'])?>" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="staticEmail">Bukti Transfer</label>
                            <img style="width: 100%" src="<?= base_url('uploads/invoice/'.$invoice['attachment'])?>">
                        </div>
                        
                      </div>
                    </div>
                </form>
            </div>
      </div>
      <div class="modal-footer" style="background-color: white !important;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>