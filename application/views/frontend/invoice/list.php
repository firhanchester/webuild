<style type="text/css">
    .unpaid {
        background: #ff4c4c;
        padding: .3em .7em;
        color: white;
    }

    .paid {
        background: #5cd23e;
        padding: .3em .7em;
        color: white;
    }

    .waiting {
        background: #ffd54c;
        padding: .3em .7em;
    }

    #ttl {
        font-weight: bold;
    }
</style>
<section class="main_content_area">
    <div class="container">  
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li style="font-weight: bold;font-size: 1.5rem">Invoice</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="web">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Actions</th>                
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($invoice as $ls){?>
                            <tr>
                                <td><?= $ls['title']?></td>
                                <td>
                                    <?php 
                                        if($ls['status'] == 0){
                                            echo "<span class='unpaid'>Unpaid</span>";
                                        }else if($ls['status'] == 1){
                                            echo "<span class='waiting'>Waiting</span>";
                                        }else{
                                            echo "<span class='paid'>Paid</span>";
                                        }

                                    ?>
                                        
                                    </td>
                                <td>
                                    <?= date('d M Y - H:i' ,strtotime($ls['created_date']))?>
                                </td>
                                <td>
                                    <?php if($ls['tbl'] != 'progress'){?>
                                        <a href="<?= site_url('invoice_'.$ls['tbl'].'/'.$ls['code'])?>" class="btn btn-sm btn-warning">View Detail</a>
                                    <?php }else{?>
                                        <a href="<?= site_url('invoice_progress/'.$ls['id'])?>" class="btn btn-sm btn-warning">View Detail</a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <div class="mobile">
                    <?php foreach($invoice as $ls){?>
                    <?php if($ls['tbl'] != 'progress'){?>
                        <a href="<?= site_url('invoice_'.$ls['tbl'].'/'.$ls['code'])?>">
                    <?php }else{?>
                        <a href="<?= site_url('invoice_progress/'.$ls['id'])?>">
                    <?php } ?>
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                      <span style="font-weight: bold;">
                                          <?= $ls['title']?>
                                      </span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6" style="text-align: right">
                                            <?php 
                                                if($ls['status'] == 0){
                                                    echo "<span class='unpaid'>Unpaid</span>";
                                                }else if($ls['status'] == 1){
                                                    echo "<span class='waiting'>Waiting</span>";
                                                }else{
                                                    echo "<span class='paid'>Paid</span>";
                                                }

                                            ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>        	
</section>