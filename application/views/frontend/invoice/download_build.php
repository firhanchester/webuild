<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<?= base_url()?>assets/webuild/css/plugins.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/webuild/css/style.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/webuild/css/custom.css">
</head>
<body>
    <style type="text/css">
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }

    .table-responsive table thead{
        background-color: #ffd54c;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }

    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }

    .container {
        width: 90%;
    }

</style>
<div class="container">
    <div class="row">
        <div class="row" style="padding: 2em 0;">
            <div class="col-md-12">
                <div class="invoice-title">
                    <h3 style="border-left: 6px solid #ffd54c;padding-left: 10px;">Invoice</h3>
                    <div>No : <?= date('Y/m' ,strtotime($invoice['created_date']))?>/BUILD/<?= $invoice['code'] ?></div>
                </div>
                <img style="width: 20%;float: right;position: absolute;top: 0;right: 10px" src="assets/webuild/img/logo/webuild.png">
                
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="invoice-title">
            <table class="table">
              <thead style="background-color: #ffd54c;">
                    <tr>
                        <td colspan="2"><span style="font-size: 20px;font-weight: bold">Tagihan Pembayaran</span></td>
                    </tr>
                </thead>
              <tr>
                <td class="no-line" style="font-weight: bold">Bank</td>
                <td class="no-line" style="font-weight: bold">: <?= strtoupper($invoice['bank']) ?> an Webuild Indonesia</td>
              </tr>
              <tr>
                <td class="no-line" style="font-weight: bold">No Rekening</td>
                <td class="no-line" style="font-weight: bold">: 0000 111 222</td>
              </tr>
              <tr>
                <td class="no-line" style="font-weight: bold">Total</td>
                <td class="no-line" style="font-weight: bold">: Rp <?= number_format($invoice['total']) ?></td>
              </tr>
              <tr>
                <td class="no-line" style="font-weight: bold">Status</td>
                <?php if($invoice['status'] <= 1) {?>
                  <td class="no-line" style="font-weight: bold">: Unpaid</td>
                <?php } else {?>
                  <td class="no-line" style="font-weight: bold">: Paid</td>
                <?php }?>
              </tr>
              <tr>
                <td class="no-line" style="font-weight: bold">Transaction Date</td>
                <td class="no-line" style="font-weight: bold">: 
                  <?= date('d M Y - H:i' ,strtotime($invoice['created_date']))?>
                </td>
              </tr>
            </table>
        </div>
      </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
        <hr>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td class="text-center mbl"><strong>Description</strong></td>
                                    <td class="text-center"><strong>Price</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Biaya Jasa Perhitungan Pembangunan</td>
                                    <td class="text-right">Rp <?= number_format($invoice['total'] - $invoice['uniq'] - $invoice['tax']) ?> </td>
                                </tr>
                                <tr>
                                    <td class="thick-line text-right"><strong>Subtotal</strong></td>
                                    <td class="thick-line text-right">Rp <?= number_format($invoice['total'] - $invoice['uniq'] - $invoice['tax']) ?> </td>
                                </tr>
                                <tr>
                                    <td class="no-line text-right"><strong>Tax (10%)</strong></td>
                                    <td class="no-line text-right">Rp <?= number_format($invoice['tax']) ?> </td>
                                </tr>
                                <tr>
                                    <td class="no-line text-right"><strong>Kode Unik</strong></td>
                                    <td class="no-line text-right">Rp <?= number_format($invoice['uniq']) ?> </td>
                                </tr>
                                <tr>
                                    <td class="no-line text-right"><strong>Total</strong></td>
                                    <td class="no-line text-right">Rp <?= number_format($invoice['total']) ?> </td>
                                </tr>
                              </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>