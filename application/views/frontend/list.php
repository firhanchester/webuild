<section id="portofolio" style="margin: 2em 0em;">
    <div class="container">
      <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-6">
            <h2 style="text-transform: uppercase;"><?= $type?></h2>
            
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <a style="float: right;" href="<?= site_url('project/request/'.$type)?>" class="btn btn-warning"><i class="fa fa-plus"></i> Request Your Project</a>
          </div>
      </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
          <?php for($i=0 ; $i < 8 ; $i++) { ?>
            <div class="col-md-4 col-sm-6 col-xs-6" id="portofolio" style="margin: 1em 0em">
              <a href="<?= site_url('project/detail/'.$i)?>">
                <div class="card-list">
                    <img class="card-list-img-top" src="<?= base_url()?>assets/webuild/img/product/house.jpg" alt="Card image" style="width:100%">
                    <div class="card-list-body">
                      <div class="location"><?= $location?></div>
                      <div class="card-list-title web"><?= (strlen($title) > 30 ? substr($title,0,30).'...' : $title) ?></div>
                      <div class="card-list-title mobile"><?= (strlen($title) > 10 ? substr($title,0,10).'...' : $title) ?></div>
                    </div>
                </div>
              </a>
            </div>
          <?php } ?>
        </div>
    </div>
</section>
<script type="text/javascript">
  var login = <?php echo json_encode($login); ?>;
  
</script>