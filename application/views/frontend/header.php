<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>WeBuild</title>
    <meta content="webuild-id , webuild" name="description" />
    <link rel="shortcut icon" href="<?= base_url()?>assets/webuild/img/logo/wb.ico" />
    <meta content="webuild" name="author" />
    <meta content="webuild-id" name="webuild" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <link rel="stylesheet" href="<?= base_url()?>assets/webuild/css/plugins.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/webuild/css/style.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/webuild/css/custom.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/webuild/slick/slick.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/webuild/slick/slick-theme.css">

    <link rel="stylesheet" href="<?= base_url()?>assets/webuild/css/swiper-bundle.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/webuild/css/swiper-bundle.min.css">

    <script src="<?= base_url()?>assets/webuild/js/swiper-bundle.js"></script>
    <script src="<?= base_url()?>assets/webuild/js/swiper-bundle.min.js"></script>

    <script type="text/javascript" src="<?= base_url()?>assets/webuild/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets/webuild/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets/webuild/slick/slick.min.js"></script>
    
    <script src="<?= base_url()?>assets/webuild/js/sweetalert.min.js"></script>
	<a href="https://api.whatsapp.com/send?phone=+6282246215335&text=halo admin, saya ingin bertanya" class="float" target="_blank">
	    <i class="fa fa-whatsapp my-float"></i>
	</a>
</head>