<section id="portofolio" style="margin: 2em;">
    <div class="container">
        <div style="margin: 1em 0em;">
            <h2>Portofolio</h2>
            <hr>
        </div>
        <div class="row">
          <?php foreach($data as $porto) {?>
            <div class="col-md-4" id="portofolio">
              <a href="<?= site_url('portofolio-detail/'.$porto['slug'])?>">
                <div class="card">
                  <img style="width: 32em;height: 18em;" class="card-img-top" src="<?= base_url('uploads/portofolio/'.$porto['image']['0']['image'])?>" alt="Card image" style="width:100%">
                  <div class="card-body">
                    <h4 class="card-title"><?= $porto['title']?></h4>
                    <p class="card-text"><?= $porto['city']?> , <?= $porto['province']?></p>
                    <p style="font-weight: bold;">Rp <?= number_format($porto['price'])?></p>
                  </div>
                </div>
              </a>
            </div>
          <?php } ?>
        </div>
        <div class="row">
          <div class="col">
              <!--Tampilkan pagination-->
              <?php echo $pagination; ?>
          </div>
      </div>
    </div>
</section>