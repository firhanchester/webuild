<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>WeBuild - Admin Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #4 for " name="description" />
        <meta content="" name="author" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?=base_url("/assets/global/plugins/font-awesome/css/font-awesome.min.css")?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url("/assets/global/plugins/simple-line-icons/simple-line-icons.min.css")?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url("/assets/global/plugins/bootstrap/css/bootstrap.min.css")?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url("/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css")?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url("/assets/global/css/components.min.css")?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=base_url("/assets/global/css/plugins.min.css")?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url("/assets/pages/css/login.min.css")?>" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" /> </head>

    <body class=" login" style="background-color: white !important;">
        <div class="logo">
            <a href="index.html">
                <img style="width: 20%" src="https://webuild-id.com/assets/webuild/img/logo/webuild.png" alt="" /> </a>
        </div>
        <div class="content">
            <form class="login-form" action="<?= site_url('admin/auth/login')?>" method="post">
                <h3 class="form-title" style="color: black">Sign In</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" placeholder="Username" name="identity" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" placeholder="Password" name="password" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn uppercase btn-block" style="background-color: #ffd54c;color: white">Login</button>
                </div>
            </form>
        </div>
        <div class="copyright"> 2020 © WeBuild</div>
        <script src="<?=base_url("assets/global/plugins/jquery.min.js")?>" type="text/javascript"></script>
        <script src="<?=base_url("assets/global/plugins/bootstrap/js/bootstrap.min.js")?>" type="text/javascript"></script>
        <script src="<?=base_url("assets/global/plugins/js.cookie.min.js")?>" type="text/javascript"></script>
        <script src="<?=base_url("assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js")?>" type="text/javascript"></script>
        <script src="<?=base_url("assets/global/plugins/jquery.blockui.min.js")?>" type="text/javascript"></script>
        <script src="<?=base_url("assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js")?>" type="text/javascript"></script>
        <script src="<?=base_url("assets/global/scripts/app.min.js")?>" type="text/javascript"></script>
        <script src="<?=base_url("assets/pages/scripts/login.min.js")?>" type="text/javascript"></script>
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>

</html>