<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=site_url('admin/banner/store_cat')?>" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control" name="category" id="category">
                                        <option></option>
                                        <?php foreach($concept as $c){?>
                                            <option value="<?= $c['name'] ?>">
                                                <?= ucfirst($c['name']);?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group" id="porto">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn yellow">Submit</button>
                                <a href="<?= site_url("admin/banner")?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#category").change(function(){
            var option = $(this).children("option:selected").val();
            $.ajax({
                url: "<?php echo site_url('admin/banner/get_porto'); ?>",
                type: "POST",
                data: {cat : option},
                success: function(data){
                    if(data != false){
                        $('#porto').html('');
                        $('#porto').html('<label>Portofolio</label><select class="form-control" name="list_porto" id="list_porto"><option>Select Portofolio</option></select>');
                        $('#list_porto').append(data);
                    }else{
                        $('#porto').html('');
                        swal("WARNING!", "Please complete the form", "warning");
                    }
                },
                error: function(error) {
                  console.log(error);
                }
            });
        });
    });
</script>