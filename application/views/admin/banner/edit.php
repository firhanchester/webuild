<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/banner/update')?>" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="form-group">
                                    <label>Page</label>
                                    <select class="form-control" name="page">
                                        <option></option>
                                        <option <?php if($data['page'] == 'home' ){ echo "selected";}?> value="home">Home</option>
                                        <option <?php if($data['page'] == 'home_owner' ){ echo "selected";}?> value="home_owner">Home Owner</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control" name="category">
                                        <option></option>
                                        <?php foreach($concept as $c){?>
                                            <option 
                                            value="<?= $c['name'] ?>"
                                            <?php if($data['category'] == $c['name'] ){ echo "selected";}?>
                                            >
                                                <?= ucfirst($c['name']);?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Link</label>
                                    <input type="text" name="link" class="form-control" value="<?= $data['link']?>">
                                </div>
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?= base_url('uploads/banners/'.$data['banner'])?>" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="banner"> </span>
                                                <input type="hidden" name="banner2" value="<?= $data['banner']?>"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="hidden" name="id" value="<?= $data['id'] ?>">
                                <button type="submit" class="btn yellow">Submit</button>
                                <a href="<?= site_url("admin/banner")?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>