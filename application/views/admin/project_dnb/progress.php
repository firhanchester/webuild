<style type="text/css">
  .bottom {
    border-top : 0 !important;
    border-right  : 0 !important;
    border-left : 0 !important;
  }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Progress</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/build/input_progress')?>" enctype="multipart/form-data">
                          <div class="form-body">
                            <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Description</label>
                                  <textarea class="form-control" disabled><?= $data['progress']['description']?></textarea>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Progress</label>
                                  <input type="text" name="provinsi" class="form-control" readonly value="<?= $data['progress']['progress']?>">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Date</label>
                                  <input type="text" name="city" class="form-control" readonly value="<?= $data['progress']['created_date']?>">
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                        <div class="row">
                          <div class="col-md-12">
                            <?php foreach($data['image'] as $image ){?>
                              <div class="col-md-4">
                                <a target="_blank" href="<?= base_url('uploads/progress/'.$image['attachment'])?>">
                                  <img style="width: 100%" src="<?= base_url('uploads/progress/'.$image['attachment'])?>">
                                </a>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>