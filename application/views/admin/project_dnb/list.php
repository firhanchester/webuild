<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$header;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th style="width: 1px"></th>
                                    <th style="width: 2px">No</th>
                                    <th> Code </th>
                                    <th> User </th>
                                    <th> Status </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $x = 1;
                                if(!empty($data)){
                                foreach ($data as $d) { ?>
                                <tr class="odd gradeX">
                                    <td></td>
                                    <td><?= $x++ ?></td>
                                    <td><?=$d['code']?></td>
                                    <td><?=$d['first_name'].' '.$d['last_name']?></td>
                                    <td>
                                        <?php if($d['status'] == 0){
                                            echo "Belum Ada Harga";
                                        }else if($d['status'] == 1){
                                            echo "Menunggu Pembayaran";
                                        }else if($d['status'] == 2){
                                            echo "On Progress";
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-xs yellow dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-left" role="menu">
                                                <li>
                                                    <a href="<?=base_url('admin/dnb/show/').$d['code']?>">
                                                    <i class="icon-doc"></i> View Project </a>
                                                </li>
                                                <li>
                                                    <a data-toggle="modal" href="#basic<?=$d['id']?>">
                                                        <i class="fa fa-eye-slash" ></i> Hide </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <div class="modal fade" id="basic<?=$d['id']?>" tabindex="9999" role="basic" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Project <?= $d['code']?></h4>
                                                </div>
                                                <div class="modal-body"> Hide project </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                                                    <a href="<?=base_url('admin/dnb/delete/').$d['code']?>"><button type="button" class="btn red">Hide</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>