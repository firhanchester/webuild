<style type="text/css">
  .bottom {
    border-top : 0 !important;
    border-right  : 0 !important;
    border-left : 0 !important;
  }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Input Harga</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/dnb/input_harga')?>" enctype="multipart/form-data">
                            <div class="form-body">
                            <input type="hidden" name="customer_id" class="form-control" readonly value="<?= $data['data']['customer_id']?>">
                            <input type="hidden" name="project_id" class="form-control" readonly value="<?= $data['data']['id']?>">
                            <input type="hidden" name="code" class="form-control" readonly value="<?= $data['data']['code']?>">

                              <div class="row">
                                <div class="col-md-4">
                                  <div class="form-group">
                                    <label>Provinsi</label>
                                    <input type="text" name="provinsi" class="form-control" readonly value="<?= $data['data']['province']?>">
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group">
                                    <label>City</label>
                                    <input type="text" name="city" class="form-control" readonly value="<?= $data['data']['city']?>">
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group">
                                    <label>Type</label>
                                    <input type="text" name="type" class="form-control" readonly value="<?= $data['data']['type']?>">
                                  </div>
                                </div>
                              </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var total = 0;

  $(function(){
    $("input").on("input", function(){
      var nilai = $(this).val();
      var inp = $(this).attr('id');
      var jml = $("."+inp).val();
      var tot =  parseInt(nilai*jml);
      var hsl = $("input[name="+inp+"]").val(tot);
      console.log(tot);
    });
  })

  function hitung(){
    total = 0
    for (var i = 1; i < 15; i++) {
      var val = parseInt($("input[name=hasil"+i+"]").val());
      total += val;
    }
    console.log(total);
    $('input[name=total]').val(total);
  }
</script>