<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>WeBuild | Admin Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link rel="shortcut icon" href="<?= base_url()?>assets/webuild/img/logo/wb.ico" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/font-awesome/css/font-awesome.min.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/simple-line-icons/simple-line-icons.min.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/bootstrap/css/bootstrap.min.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/morris/morris.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/fullcalendar/fullcalendar.min.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/jqvmap/jqvmap/jqvmap.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/datatables/datatables.min.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/css/components.min.css")?>' rel="stylesheet" id="style_components" type="text/css" />
        <link href='<?=base_url("assets/global/css/plugins.min.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/layouts/layout4/css/layout.min.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/layouts/layout4/css/themes/default.min.css")?>' rel="stylesheet" type="text/css" id="style_color" />
        <link href='<?=base_url("assets/layouts/layout4/css/custom.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/icheck/skins/all.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css")?>' rel="stylesheet" type="text/css" />
        <link href='<?=base_url("assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css")?>' rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />

        <style type="text/css">
            .page-header.navbar {
                background-color: #ffd54c;
            }
        </style>

        <script src='<?=base_url("assets/global/plugins/jquery.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/bootstrap/js/bootstrap.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/js.cookie.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jquery.blockui.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/moment.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/morris/morris.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/morris/raphael-min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/counterup/jquery.waypoints.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/counterup/jquery.counterup.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/amcharts/amcharts/amcharts.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/amcharts/amcharts/serial.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/amcharts/amcharts/pie.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/amcharts/amcharts/radar.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/amcharts/amcharts/themes/light.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/amcharts/amcharts/themes/patterns.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/amcharts/amcharts/themes/chalk.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/amcharts/ammap/ammap.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/amcharts/ammap/maps/js/worldLow.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/amcharts/amstockcharts/amstock.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/fullcalendar/fullcalendar.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/horizontal-timeline/horizontal-timeline.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/flot/jquery.flot.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/flot/jquery.flot.resize.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/flot/jquery.flot.categories.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jquery.sparkline.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/scripts/datatable.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/datatables/datatables.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/jquery-ui/jquery-ui.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/scripts/app.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/pages/scripts/dashboard.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/pages/scripts/table-datatables-managed.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/pages/scripts/ui-modals.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/layouts/layout4/scripts/layout.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/layouts/layout4/scripts/demo.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/layouts/global/scripts/quick-sidebar.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/layouts/global/scripts/quick-nav.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/global/plugins/icheck/icheck.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/webuild/js/form-repeater.min.js")?>' type="text/javascript"></script>
        <script src='<?=base_url("assets/webuild/js/jquery.repeater.js")?>' type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <div class="page-header navbar navbar-fixed-top">
            <div class="page-header-inner ">
                <div class="page-logo">
                    <a href="index.html">
                        <img style="width: 5em;" src="<?= base_url()?>assets/webuild/img/logo/webuild-1.png" alt="logo" class="logo-default" /> </a>
                    <div class="menu-toggler sidebar-toggler">
                    </div>
                </div>
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <div class="page-top">
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="separator hide"> </li>
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> <?= $user['username'] ?> </span>
                                    <img alt="" class="img-circle" src="<?= base_url()?>assets/layouts/layout4/img/avatar9.jpg" /> </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="page_user_profile_1.html">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="app_inbox.html">
                                            <i class="icon-envelope-open"></i> My Inbox
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="app_todo_2.html">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success"> 7 </span>
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>
                                    <li>
                                        <a href="<?=base_url('logout')?>">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>