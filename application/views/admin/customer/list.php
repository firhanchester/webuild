<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Username </th>
                                    <th> Email </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($data as $d) { ?>
                                <tr class="odd gradeX">
                                    <td><?=$d['username']?></td>
                                    <td>
                                        <a href="mailto:<?=$d['email']?>"><?=$d['email']?></a>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-left" role="menu">
                                                <li>
                                                    <a href="<?=base_url('admin/customers/view/').$d['id']?>">
                                                        <i class="icon-eye"></i> View </a>
                                                </li>
                                                <li>
                                                    <a href="<?=base_url('admin/customers/edit/').$d['id']?>">
                                                        <i class="icon-pencil"></i> Update </a>
                                                </li>
                                                <li>
                                                    <a data-toggle="modal" href="#basic<?=$d['id']?>">
                                                        <i class="icon-trash" ></i> Delete </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <div class="modal fade" id="basic<?=$d['id']?>" tabindex="9999" role="basic" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Delete User</h4>
                                                </div>
                                                <div class="modal-body"> Delete user <?=$d['username']?> </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                                                    <a href="<?=base_url('admin/customers/delete/').$d['id']?>"><button type="button" class="btn red">Delete</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->