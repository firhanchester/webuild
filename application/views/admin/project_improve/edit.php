<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/project/update')?>" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="form-group">
                                    <label>Project</label>
                                    <input type="text" name="Project" class="form-control" placeholder="Project Name" required>
                                </div>
                                <div class="form-group">
                                    <label>Provinsi</label>
                                    <select required class="form-control" id="province" name="province" onchange="dataCity(this)">
                                        <option value="">-- Provinsi --</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Kota</label>
                                    <select required class="form-control" id="city" name="city" onchange="dataSubdistrict(this)">
                                        <option>-- Kota --</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <select required class="form-control" id="subdistrict" name="subdistrict">
                                        <option>-- Kecamatan --</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Concept</label>
                                    <div class="input-group col-md-12">
                                        <select class="form-control" name="concept" required>
                                            <option value="">-- Select Concept --</option>
                                            <?php foreach($concept as $c) {?>
                                                <option value="<?= $c['id']?>"><?= $c['name']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Level</label>
                                    <div class="input-group col-md-12">
                                        <select class="form-control" name="Level" required>
                                            <option value="">-- Select Page --</option>
                                            <?php foreach($levels as $l) {?>
                                                <option value="<?= $l['id']?>"><?= $l['name']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >Banner</label>
                                    <div class="input-group col-md-12">
                                        <input required type="file" class="form-control" name="banner" id="banner" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn yellow">Submit</button>
                                <a href="<?=base_url("/admin/users")?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){
        $.ajax({
            url: "<?php echo site_url('admin/project/province')?>",
            type: "post",
            success: function(data){
                $('#province').append(data)
            },
            error: function(error) {
                console.log(error);
            }
        });
    })

    function dataCity(pro){
      console.log(pro.value)
      $.ajax({
          url: "<?php echo site_url('admin/project/city')?>",
          type: "post",
          data : {province_id : pro.value},
          success: function(data){
              $('#city').html('<option value="">-- Kota --</option>');
              $('#city').append(data);
          },
          error: function(error) {
              console.log(error);
          }
      });
    }

    function dataSubdistrict(city){
      console.log(city.value)
      var prov_id = $('#province1').val();
      $.ajax({
          url: "<?php echo site_url('admin/project/subdistrict')?>",
          type: "post",
          data : {city_id : city.value , province_id : prov_id},
          success: function(data){
              $('#subdistrict').html('<option value="">-- Kecamatan --</option>');
              $('#subdistrict').append(data);
          },
          error: function(error) {
              console.log(error);
          }
      });
    }
</script>