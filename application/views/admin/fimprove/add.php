<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/fimprove/store')?>" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="email">Name Improve</label>
                                    <div class="input-group col-md-12">
                                        <input type="text" name="name" required class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mt-repeater">
                                            <div data-repeater-list="kb1">
                                                <div data-repeater-item>
                                                    <div class="row" style="margin-bottom:1em; ">
                                                        <div class="col-md-4 col-12">
                                                            <div>Image</div>
                                                            <br>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 300px; height: 220px;">
                                                                    <img src="http://www.placehold.it/300/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="file"> </span>
                                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <div>Title</div>
                                                                        <br>
                                                                        <input type="text" name="title" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12"></div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <div>Description</div>
                                                                        <br>
                                                                        <textarea class="form-control" rows="6" name="description"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </div>
                                            <a href="javascript:;" data-repeater-create class="btn btn-warning btn-sm">
                                                Add More
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-warning">Submit</button>
                                <a href="<?=base_url("/admin/fimprove")?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#tambah').click(function(){
    var div = $('#repeat');
    div.append('<div class="form-group"><label for="design">Design</label><input type="file" name="attachment[]" class="form-control" id="file"></div>');
  });
}); 
</script>