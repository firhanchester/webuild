<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/fimprove/update')?>" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="email">Name Improve</label>
                                    <div class="input-group col-md-12">
                                        <input type="text" name="name" value="<?= $data['name']?>" class="form-control">
                                        <input type="hidden" name="id" value="<?= $data['id']?>" class="form-control">
                                    </div>
                                </div>

                                <?php foreach($data['component'] as $comp){ 
                                    echo " <input type='hidden' name='img[]' class='form-control' value='".(!empty($comp['image']) ? $comp['image'] : '' )."' >";
                                } ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mt-repeater">
                                            <div data-repeater-list="kb1">
                                                <?php foreach($data['component'] as $comp){?>
                                                    <div data-repeater-item>
                                                        <div class="row" style="margin-bottom:1em; ">
                                                            <div class="col-md-4 col-12">
                                                                <div>Image</div>
                                                                <br>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 300px; height: 220px;">
                                                                        <img src="http://www.placehold.it/300/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                                        
                                                                    </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="file"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <div>Title</div>
                                                                            <br>
                                                                            <input type="hidden" value="<?= $comp['id']?>" name="id" class="form-control">
                                                                            <input type="text" value="<?= $comp['title']?>" name="title" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12"></div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <div>Description</div>
                                                                            <br>
                                                                            <textarea class="form-control" rows="6" name="description"><?= $comp['description']?></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button type="button" id="<?= $comp['id']?>" class="btn btn-danger">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <a href="javascript:;" data-repeater-create class="btn btn-warning btn-sm">
                                                Add More
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-warning">Submit</button>
                                <a href="<?=base_url("/admin/fimprove")?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('.btn-danger').click(function(){
        var id = $(this).attr('id');
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
                url: "<?php echo site_url('admin/fimprove/delete_comp'); ?>",
                type: "POST",
                data: {id : id},
                success: function(data){
                    if(data.success == true){
                        swal("Successfully Deleted");
                        window.location.reload();
                    }else{
                        swal("WARNING!", "Please Refresh The Page", "warning");
                    }
                },
                error: function(error) {
                  console.log(error);
                }
            });
          }
        });
        
    })
}); 
</script>