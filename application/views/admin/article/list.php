<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="<?= site_url('admin/article/add')?>">
                                            <button id="sample_editable_1_new" class="btn sbold yellow"> Add Article
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Title </th>
                                    <th> Thumbnail </th>
                                    <th> Author </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($data)){
                                foreach ($data as $d) { ?>
                                <tr class="odd gradeX">
                                    <td><?=$d['title']?></td>
                                    <td>
                                        <img style="width: 200px;" src="<?= base_url('uploads/thumbnails/'.$d['thumbnail'])?>">
                                    </td>
                                    <td><?=$d['author']?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-left" role="menu">
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="icon-eye"></i> View </a>
                                                </li>
                                                <li>
                                                    <a href="<?=base_url('admin/article/edit/').$d['id']?>">
                                                        <i class="icon-pencil"></i> Update </a>
                                                </li>
                                                <li>
                                                    <a data-toggle="modal" href="#basic<?=$d['id']?>">
                                                        <i class="icon-trash" ></i> Delete </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <div class="modal fade" id="basic<?=$d['id']?>" tabindex="9999" role="basic" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Delete Article</h4>
                                                </div>
                                                <div class="modal-body"> Delete Article </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                                                    <a href="<?=base_url('admin/article/delete/').$d['id']?>"><button type="button" class="btn red">Delete</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>