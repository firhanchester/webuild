<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/component/update')?>" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="email">Type</label>
                                    <div class="input-group col-md-12">
                                        <select class="form-control" name="type" required>
                                            <option value="">Select Type</option>
                                            <option value="1" <?php if($data['id'] == '1' ){ echo "selected";}?> >Luxury</option>
                                            <option value="2" <?php if($data['id'] == '2' ){ echo "selected";}?> >Premium</option>
                                            <option value="3" <?php if($data['id'] == '3' ){ echo "selected";}?> >Minimalist</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-repeater">
                                    <label>Add Component</label>
                                    <div data-repeater-list="list_component">
                                        <?php foreach($data['component'] as $comp) { ?>
                                        <div data-repeater-item class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="component">
                                                        <option value="">Select Component</option>
                                                        <option  <?php if($comp['component'] == 'floor' ){ echo "selected";}?> value="floor">Floor</option>
                                                        <option  <?php if($comp['component'] == 'ceiling' ){ echo "selected";}?> value="ceiling">ceiling</option>
                                                        <option  <?php if($comp['component'] == 'fasade' ){ echo "selected";}?> value="fasade">fasade</option>
                                                        <option  <?php if($comp['component'] == 'sanitary' ){ echo "selected";}?> value="sanitary">sanitary</option>
                                                        <option  <?php if($comp['component'] == 'atap' ){ echo "selected";}?> value="atap">atap</option>
                                                        <option  <?php if($comp['component'] == 'pintu' ){ echo "selected";}?> value="pintu">pintu</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="spec" class="form-control" placeholder="Component" value="<?= $comp['spec']?>">
                                                </div>
                                            </div>

                                        </div>
                                        <?php } ?>
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-default">
                                        Add More Component
                                    </a>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-warning">Submit</button>
                                <a href="<?=base_url("/admin/component")?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
