<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/component/store')?>" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="email">Type</label>
                                    <div class="input-group col-md-12">
                                        <input type="text" name="type" required class="form-control">
                                    </div>
                                </div>
                                <div class="mt-repeater">
                                    <label>Add Component</label>
                                    <div data-repeater-list="list_component">
                                        <div data-repeater-item class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="component">
                                                        <option value="">Select Component</option>
                                                        <option value="floor">Floor</option>
                                                        <option value="ceiling">ceiling</option>
                                                        <option value="fasade">fasade</option>
                                                        <option value="sanitary">sanitary</option>
                                                        <option value="atap">atap</option>
                                                        <option value="pintu">pintu</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="spec" class="form-control" placeholder="Component">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-default">
                                        Add More Component
                                    </a>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-warning">Submit</button>
                                <a href="<?=base_url("/admin/component")?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
