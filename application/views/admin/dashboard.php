<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Project</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="<?= site_url('admin/design/design_ongoing')?>">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-yellow-gold">
                                <span data-counter="counterup" data-value="<?= $design['total']?>"></span>
                            </h3>
                            <small>Design</small>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success yellow-gold">
                            </span>
                        </div>
                    </div>
                </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="<?= site_url('admin/build/build_pending')?>">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-yellow-lemon">
                                <span data-counter="counterup" data-value="<?= $build['total']?>"></span>
                            </h3>
                            <small>Build</small>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success yellow-lemon">
                            </span>
                        </div>
                    </div>
                </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="<?= site_url('admin/dnb/dnb_pending')?>">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-blue-sharp">
                                <span data-counter="counterup" data-value="<?= $dnb['total']?>"></span>
                            </h3>
                            <small>Design & Build</small>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success blue-sharp">
                            </span>
                        </div>
                    </div>
                </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="<?= site_url('admin/improve/improve_pending')?>">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-purple-sharp">
                                <span data-counter="counterup" data-value="<?= $improve['total']?>"></span>
                            </h3>
                            <small>Improve</small>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success purple-sharp">
                            </span>
                        </div>
                    </div>
                </div>
                </a>
            </div>
        </div>

        <div class="page-head">
            <div class="page-title">
                <h1>Invoice</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-green">
                                <span data-counter="counterup" data-value="<?= $invoice['nv']?>"></span>
                            </h3>
                            <small>Invoice Service & Design - Need Verification</small>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success green">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">

                    <div class="display">
                        <div class="number">
                            <h3 class="font-red-mint">
                                <span data-counter="counterup" data-value="<?= $invoice_dp['nv']?>"></span>
                            </h3>
                            <small>Invoice Down Payment - Need Verification</small>
                        </div>
                    </div>
                    
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success red-mint">
                            </span>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-blue-sharp">
                                <span data-counter="counterup" data-value="<?= $invoice_progress['nv']?>"></span>
                            </h3>
                            <small>Invoice Progress - Need Verification</small>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success blue-sharp">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-green">
                                <span data-counter="counterup" data-value="<?= $invoice['paid']?>"></span>
                            </h3>
                            <small>Invoice Service & Design - Paid</small>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success green">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-red-mint">
                                <span data-counter="counterup" data-value="<?= $invoice_dp['paid']?>">0</span>
                            </h3>
                            <small>Invoice Down Payment - Paid</small>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success red-mint">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-blue-sharp">
                                <span data-counter="counterup" data-value="<?= $invoice_progress['paid']?>">0</span>
                            </h3>
                            <small>Invoice Progress - Paid</small>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success blue-sharp">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
