<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('banner/store')?>" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="email">Page</label>
                                    <div class="input-group col-md-12">
                                        <select class="form-control" name="page">
                                            <option>-- Select Page --</option>
                                            <option value="home">Home</option>
                                            <option value="promotion">Promotion</option>
                                            <option value="design">Interest In Design</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password">Banner</label>
                                    <div class="input-group col-md-12">
                                        <input required type="file" class="form-control" name="banner" id="banner" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn yellow">Submit</button>
                                <a href="<?=base_url("/admin/users")?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>