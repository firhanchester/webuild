<style type="text/css">
  .bottom {
    border-top : 0 !important;
    border-right  : 0 !important;
    border-left : 0 !important;
  }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Input Progress</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                      <a class="btn yellow sbold" data-toggle="modal" href="#basic"> <i class="fa fa-plus"></i> Add Progress </a>
                        <form role="form" method="post" action="<?=base_url('admin/build/input_progress')?>" enctype="multipart/form-data">
                          <div class="form-body">
                            <input type="hidden" name="customer_id" class="form-control" readonly value="<?= $data['customer_id']?>">
                            <input type="hidden" name="project_id" class="form-control" readonly value="<?= $data['id']?>">
                            <input type="hidden" name="code" class="form-control" readonly value="<?= $data['code']?>">

                            <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>User</label>
                                  <input type="text" name="provinsi" class="form-control" readonly value="<?= $data['first_name'].' '.$data['last_name']?>">
                                </div>
                              </div>
                            </div>

                          </div>
                        </form>
                        <div class="row">
                          <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover order-column" id="sample_1">
                              <thead>
                                <th>Attachment</th>
                                <th>Progress</th>
                                <th>Description</th>
                                <th>Date</th>
                              </thead>
                              <tbody>
                                  <?php
                                  $x = 1;
                                  if(!empty($progress)){
                                  foreach ($progress as $d) { ?>
                                  <tr class="odd gradeX">
                                      <td>
                                        <img style="width: 15em;" src="<?= base_url('uploads/progress/'.$d['image'])?>">
                                      </td>
                                      <td><?=$d['progress']?> %</td>
                                      <td><?=$d['description']?></td>
                                      <td><?=$d['created_date']?></td>
                                  </tr>
                                  <?php }} ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Input Progress</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12" style="display: block;text-align: center">
                <form style="display: inline-block;text-align: left;width: 95%" method="post" action="<?= site_url('admin/project/upload_progress')?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                              <label>Date</label>
                              <input type="date" name="tanggal" class="form-control">
                              <input type="hidden" name="code" value="<?= $data['code'] ?>" class="form-control">
                              <input type="hidden" name="page" value="design" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                              <label>Progress (%)</label>
                              <input type="text" name="progress" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control">
                          </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Description</label>
                        <textarea name="description" class="form-control" rows="5"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="staticEmail">Attachment</label>
                        <input type="file" name="attachment" class="form-control" id="file">
                    </div>
                    <div class="form-group">
                        <button class="btn yellow sbold btn-block" type="submit">Submit</button>
                    </div>
                </form>
            </div>
            </div>
          </div>
      </div>
      <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
