<div class="page-content-wrapper">
<div class="page-content">
<div class="page-head">
    <div class="page-title">
        <h1><?=$title;?></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <form role="form" method="post" action="<?=site_url('admin/portofolio/store')?>" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                  <label>Provinsi</label>
                                  <select required class="form-control" id="province" name="province" onchange="dataCity(this)">
                                      <option value=""> Province </option>
                                  </select>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                  <label>Kota</label>
                                  <select required class="form-control" id="city" name="city">
                                      <option> City </option>
                                  </select>
                              </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="form-control" name="type" id="type">
                                        <option></option>
                                        <?php foreach($concept as $c){?>
                                            <option value="<?= $c['name'] ?>">
                                                <?= ucfirst($c['name']);?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Judul</label>
                                    <input type="text" class="form-control" name="title" required>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="text" class="form-control" name="title" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Luas Tanah</label>
                                    <input type="text" class="form-control" name="price" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Luas Bangunan</label>
                                    <input type="text" class="form-control" name="price" required>
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="form-group" id="lst_gmbr">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image[]"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image[]"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image[]"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image[]"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <div class="mt-repeater" style="margin-bottom: 2em;">
                            <label>Component</label>
                            <div data-repeater-list="list_component">
                                <div data-repeater-item class="row">
                                    <div class="col-md-6">
                                        <!-- <div class="form-group">
                                            <input type="text" name="spec" class="form-control" placeholder="Component">
                                        </div> -->
                                        <div class="form-group">
                                            <select class="form-control" name="component">
                                                <option value="">Select Component</option>
                                                <option value="floor">Floor</option>
                                                <option value="ceiling">ceiling</option>
                                                <option value="fasade">fasade</option>
                                                <option value="sanitary">sanitary</option>
                                                <option value="atap">atap</option>
                                                <option value="pintu">pintu</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="spec" class="form-control" placeholder="Component">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <a href="javascript:;" data-repeater-create class="btn yellow">
                                Add More Component
                            </a>
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" id="description" rows="8" name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="author" value="<?= $user['username'] ?>">
                        <button type="submit" class="btn yellow">Submit</button>
                        <a href="<?= site_url("admin/portofolio")?>"><button type="button" class="btn default">Cancel</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    $('#description').wysihtml5({
        "font-styles": false,
        "image" : false
    });
    $(document).ready(function(){
        $.ajax({
            url: "<?php echo site_url('admin/project/province')?>",
            type: "post",
            success: function(data){
                $('#province').html('<option value=""> Province </option>');
                $('#province').append(data)
            },
            error: function(error) {
                console.log(error);
            }
        });
    })

    function dataCity(pro){
      var x = $('#province').children(":selected").attr("id");
      $.ajax({
          url: "<?php echo site_url('admin/project/city')?>",
          type: "post",
          data : {province_id : x},
          success: function(data){
              $('#city').html('<option value=""> City </option>');
              $('#city').append(data);
          },
          error: function(error) {
              console.log(error);
          }
      });
    }

    $('#tambah').click(function(){
        var div = $('#lst_gmbr');
        div.append('<div class="row"><div class="col-md-3"><div class="fileinput fileinput-new" data-provides="fileinput"><div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div><div><span class="btn default btn-file"><span class="fileinput-new"> Select image </span><span class="fileinput-exists"> Change </span><input type="file" name="image[]"> </span><a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a></div></div></div><div class="col-md-3"><div class="fileinput fileinput-new" data-provides="fileinput"><div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div><div><span class="btn default btn-file"><span class="fileinput-new"> Select image </span><span class="fileinput-exists"> Change </span><input type="file" name="image[]"> </span><a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a></div></div></div><div class="col-md-3"><div class="fileinput fileinput-new" data-provides="fileinput"><div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div><div><span class="btn default btn-file"><span class="fileinput-new"> Select image </span><span class="fileinput-exists"> Change </span><input type="file" name="image[]"> </span><a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a></div></div></div><div class="col-md-3"><div class="fileinput fileinput-new" data-provides="fileinput"><div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div><div><span class="btn default btn-file"><span class="fileinput-new"> Select image </span><span class="fileinput-exists"> Change </span><input type="file" name="image[]"> </span><a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a></div></div></div></div>');
    });

</script>