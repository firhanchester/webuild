<div class="page-content-wrapper">
<div class="page-content">
<div class="page-head">
    <div class="page-title">
        <h1><?=$title;?></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <form role="form" method="post" action="<?=site_url('admin/portofolio/update/'.$data['porto']['id'])?>" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                  <label>Province</label>
                                  <input type="text" class="form-control" name="province" value="<?= $data['porto']['province']?>"readonly>
                                  <input type="hidden" name="id" value="<?= $data['porto']['id']?>"readonly>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                  <label>City</label>
                                  <input type="text" class="form-control" name="city" value="<?= $data['porto']['city']?>"readonly>
                              </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Type</label>
                                    <input type="text" class="form-control" name="type" value="<?= $data['porto']['type']?>"readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Judul</label>
                                    <input type="text" class="form-control" name="title" value="<?= $data['porto']['title']?>">
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="text" class="form-control" name="price" value="<?= $data['porto']['price']?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Luas Tanah</label>
                                    <input type="text" class="form-control" name="lt" value="<?= $data['porto']['lt']?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Luas Bangunan</label>
                                    <input type="text" class="form-control" name="lb" value="<?= $data['porto']['lb']?>">
                                </div>
                            </div>
                            
                        </div>

                        <?php foreach($data['image'] as $file){ 
                            echo " <input type='hidden' name='img[]' class='form-control' value='".(!empty($file['image']) ? $file['image'] : '' )."' >";
                        } ?>
                        
                        <div class="form-group" id="lst_gmbr">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?= base_url('uploads').'/portofolio/'.(!empty($data['image'][0]) ? $data['image'][0]['image'].'?rand='.rand(1,2000) : '' ) ?>" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image[]"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?= base_url('uploads').'/portofolio/'.(!empty($data['image'][1]) ? $data['image'][1]['image'].'?rand='.rand(1,2000) : '' ) ?>" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image[]"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?= base_url('uploads').'/portofolio/'.(!empty($data['image'][2]) ? $data['image'][2]['image'].'?rand='.rand(1,2000) : '' ) ?>" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image[]"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?= base_url('uploads').'/portofolio/'.(!empty($data['image'][3]) ? $data['image'][3]['image'].'?rand='.rand(1,2000) : '' ) ?>" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image[]"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <div class="mt-repeater" style="margin-bottom: 2em;">
                            <label>Component</label>
                            <div data-repeater-list="list_component">
                                <?php foreach($data['comp'] as $comp) { ?>
                                    <div data-repeater-item class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control" name="component">
                                                    <option value=""></option>
                                                    <option  <?php if($comp['component'] == 'floor' ){ echo "selected";}?> value="floor">Floor</option>
                                                        <option  <?php if($comp['component'] == 'ceiling' ){ echo "selected";}?> value="ceiling">ceiling</option>
                                                        <option  <?php if($comp['component'] == 'fasade' ){ echo "selected";}?> value="fasade">fasade</option>
                                                        <option  <?php if($comp['component'] == 'sanitary' ){ echo "selected";}?> value="sanitary">sanitary</option>
                                                        <option  <?php if($comp['component'] == 'atap' ){ echo "selected";}?> value="atap">atap</option>
                                                        <option  <?php if($comp['component'] == 'pintu' ){ echo "selected";}?> value="pintu">pintu</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="spec" class="form-control" placeholder="Component" value="<?= $comp['spec']?>">
                                            </div>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                            <a href="javascript:;" data-repeater-create class="btn yellow">
                                Add More Component
                            </a>
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" id="description" rows="8" name="description"><?= $data['porto']['description']?></textarea>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="author" value="<?= $user['username'] ?>">
                        <button type="submit" class="btn yellow">Submit</button>
                        <a href="<?= site_url("admin/portofolio")?>"><button type="button" class="btn default">Cancel</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    $('#description').wysihtml5({
        "font-styles": false,
        "image" : false
    });
    $(document).ready(function(){
        $.ajax({
            url: "<?php echo site_url('admin/project/province')?>",
            type: "post",
            success: function(data){
                $('#province').html('<option value=""> Province </option>');
                $('#province').append(data)
            },
            error: function(error) {
                console.log(error);
            }
        });
    })

    function dataCity(pro){
      var x = $('#province').children(":selected").attr("id");
      $.ajax({
          url: "<?php echo site_url('admin/project/city')?>",
          type: "post",
          data : {province_id : x},
          success: function(data){
              $('#city').html('<option value=""> City </option>');
              $('#city').append(data);
          },
          error: function(error) {
              console.log(error);
          }
      });
    }

</script>