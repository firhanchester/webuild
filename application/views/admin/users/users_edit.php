<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/auth/edit_user/').$data['id']?>">
                            <div class="form-body">
                                <input required value="<?=$data['id']?>" type="hidden" class="form-control" name="id" id="id" placeholder="id">
                                <div class="form-group">
                                    <label for="identity">Username</label>
                                    <div class="input-group col-md-12">
                                        <input readonly value="<?=$data['username']?>" type="text" class="form-control" name="identity" id="identity" placeholder="Username">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <div class="input-group col-md-12">
                                        <input required value="<?=$data['first_name']?>" type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <div class="input-group col-md-12">
                                        <input required value="<?=$data['last_name']?>" type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <div class="input-group col-md-12">
                                        <input readonly value="<?=$data['email']?>" type="email" class="form-control" name="email" id="email" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password ( 8 character )</label>
                                    <div class="input-group col-md-12">
                                        <input value="" type="password" class="form-control" name="password" id="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password_confirm">Password Confirm ( 8 character )</label>
                                    <div class="input-group col-md-12">
                                        <input value="" type="password" class="form-control" name="password_confirm" id="password" placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn yellow">Submit</button>
                                <a href="<?=base_url("admin/users/admin")?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->