<style type="text/css">
  .bottom {
    border-top : 0 !important;
    border-right  : 0 !important;
    border-left : 0 !important;
  }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Input Harga</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/build/input_harga')?>" enctype="multipart/form-data">
                            <div class="form-body">
                            <input type="hidden" name="customer_id" class="form-control" readonly value="<?= $data['customer_id']?>">
                            <input type="hidden" name="project_id" class="form-control" readonly value="<?= $data['id']?>">
                            <input type="hidden" name="code" class="form-control" readonly value="<?= $data['code']?>">

                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Provinsi</label>
                                    <input type="text" name="provinsi" class="form-control" readonly value="<?= $data['province']?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>City</label>
                                    <input type="text" name="city" class="form-control" readonly value="<?= $data['city']?>">
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="row">

                              <div class="col-md-3">
                                <div style="margin: 1em 0;">Site Plan</div>
                                <img style="width: 100%" src="<?= base_url('uploads/banners/'.$data['site_plan'])?>">
                              </div>

                              <div class="col-md-3">
                                <div style="margin: 1em 0;">Architect Drawing</div>
                                <img style="width: 100%" src="<?= base_url('uploads/banners/'.$data['architect_drawing'])?>">
                              </div>

                              <div class="col-md-3">
                                <div style="margin: 1em 0;">Structure Drawing</div>
                                <img style="width: 100%" src="<?= base_url('uploads/banners/'.$data['structure_drawing'])?>">
                              </div>

                              <div class="col-md-3">
                                <div style="margin: 1em 0;">MEP Drawing</div>
                                <img style="width: 100%" src="<?= base_url('uploads/banners/'.$data['mep_drawing'])?>">
                              </div>

                            </div>

                            <div class="row">
                              
                              <div class="col-md-3">
                                <div style="text-align: center;margin: 1em 0;">
                                  <a href="" type="button" class="btn btn-primary btn-block btn-lg yellow">Download</a>
                                </div>
                              </div>

                              <div class="col-md-3">
                                <div style="text-align: center;margin: 1em 0;">
                                  <a href="" type="button" class="btn btn-primary btn-block btn-lg yellow">Download</a>
                                </div>
                              </div>

                              <div class="col-md-3">
                                <div style="text-align: center;margin: 1em 0;">
                                  <a href="" type="button" class="btn btn-primary btn-block btn-lg yellow">Download</a>
                                </div>
                              </div>

                              <div class="col-md-3">
                                <div style="text-align: center;margin: 1em 0;">
                                  <a href="" type="button" class="btn btn-primary btn-block btn-lg yellow">Download</a>
                                </div>
                              </div>

                            </div>

                            <div class="row">
                              <div class="col-md-12">
                              <hr>
                                <div class="form-group">
                                  <label>Estimasi Harga</label>
                                  <input type="text" value="<?php if($data['total'] > 0){echo $data['total']; }else{echo '0';}?>" name="total" class="form-control bottom" placeholder="Jumlah" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <button type="submit" class="btn sbold yellow "> Submit</button>
                                </div>
                              </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var total = 0;

  $(function(){
    $("input").on("input", function(){
      var nilai = $(this).val();
      var inp = $(this).attr('id');
      var jml = $("."+inp).val();
      var tot =  parseInt(nilai*jml);
      var hsl = $("input[name="+inp+"]").val(tot);
      console.log(tot);
    });
  })

  function hitung(){
    total = 0
    for (var i = 1; i < 15; i++) {
      var val = parseInt($("input[name=hasil"+i+"]").val());
      total += val;
    }
    console.log(total);
    $('input[name=total]').val(total);
  }
</script>