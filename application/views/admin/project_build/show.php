<style type="text/css">
  .bottom {
    border-top : 0 !important;
    border-right  : 0 !important;
    border-left : 0 !important;
  }
  #file {
    border : 0px;
    padding: 0px;
  }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
          <div class="page-title">
            <h1>Detail Project</h1>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="portlet light bordered">
              <div class="portlet-body form">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                      <input type="hidden" name="customer_id" class="form-control" readonly value="<?= $data['data']['customer_id']?>">
                      <input type="hidden" name="project_id" class="form-control" readonly value="<?= $data['data']['id']?>">
                      <input type="hidden" name="code" class="form-control" readonly value="<?= $data['data']['code']?>">

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Customer</label>
                            <input type="text" name="provinsi" class="form-control" readonly value="<?= $data['data']['first_name'].' '.$data['data']['last_name'] ?>">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control" readonly value="<?= $data['data']['email']?>">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="phone" class="form-control" readonly value="<?= $data['data']['phone']?>">
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Provinsi</label>
                            <input type="text" name="provinsi" class="form-control" readonly value="<?= $data['data']['province']?>">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>City</label>
                            <input type="text" name="city" class="form-control" readonly value="<?= $data['data']['city']?>">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Type</label>
                            <input type="text" name="type" class="form-control" readonly value="<?= $data['data']['type']?>">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Luas Tanah</label>
                          <input type="text" name="harga_tanah" class="form-control" readonly value="<?= $data['data']['luas_tanah']?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Luas Bangunan</label>
                          <input type="text" name="harga_bangunan" class="form-control" readonly value="<?= $data['data']['luas_bangunan']?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Jumlah Lantai</label>
                          <input type="text" name="floor" class="form-control" readonly value="<?= $data['data']['floor']?>">
                        </div>
                      </div>
                    </div>

                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">

                  <div class="row" style="margin-bottom: 1em;">
                    <div class="col-md-12">
                      <?php if(is_null($data['invoice_dp'])){?>
                        <a class="btn yellow sbold" data-toggle="modal" href="#basicdp2"> <i class="fa fa-plus"></i> Submit Design </a>
                        <a class="btn yellow sbold" data-toggle="modal" href="#basicdp"> <i class="fa fa-plus"></i> Input DP </a>
                      <?php } else {?>
                        <?php if($data['data']['status'] != 3){?>
                          <?php if($data['invoice_dp']['status'] == 2){?>
                            <a class="btn yellow sbold" data-toggle="modal" href="#basic"> <i class="fa fa-plus"></i> Add Progress </a>
                          <?php } else {?>
                            <div style="margin-top: 1em;">
                              <a class="btn yellow sbold" data-toggle="modal" href="#basicdp2"> <i class="fa fa-plus"></i> Submit Design </a>
                              <a class="btn yellow sbold">Note: Customer Belum Membayar DP </a>
                            </div>
                          <?php }?>
                        <?php }else{?>
                          <a class="btn yellow sbold" data-toggle="modal" href="#basic"> <i class="fa fa-plus"></i> Add To Portofolio </a>
                        <?php }} ?>
                    </div>
                  </div>

                    <div class="row">
                      <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover order-column" id="sample_1">
                          <thead>
                            <th>Attachment</th>
                            <th>Date</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                              <?php
                              $x = 1;
                              if(!empty($design)){
                              foreach ($design as $d) { ?>
                              <tr class="odd gradeX">
                                  <td>
                                    <img style="width: 15em;" src="<?= base_url('uploads/design/'.$d['file'])?>">
                                  </td>
                                  <td><?=$d['created_date']?></td>
                                  <td>
                                    <a href="" class="btn btn-sm btn-warning"><i class="fa fa-download"></i></a>
                                  </td>
                              </tr>
                              <?php }} ?>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <h2>Progress</h2>
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                          <thead>
                            <th>Progress</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                              <?php
                              $x = 1;
                              if(!empty($progress)){
                              foreach ($progress as $d) { ?>
                              <tr class="odd gradeX">
                                  <td><?=$d['progress']?> %</td>
                                  <td><?=$d['description']?></td>
                                  <td><?=$d['created_date']?></td>
                                  <td>
                                    <a class="btn yellow" href="<?= site_url('admin/dnb/progress/'.$d['id'])?>">View</a>
                                  </td>
                              </tr>
                              <?php }} ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

    </div>
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Input Progress</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12" style="display: block;text-align: center">
                <form style="display: inline-block;text-align: left;width: 95%" method="post" action="<?= site_url('admin/dnb/input_progress')?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                              <label>Date</label>
                              <input type="date" name="tanggal" class="form-control">
                              <input type="hidden" name="code" value="<?= $data['data']['code'] ?>" class="form-control">
                              <input type="hidden" name="customer_id" value="<?= $data['data']['customer_id'] ?>" class="form-control">
                              <input type="hidden" name="bank" value="<?= $data['invoice']['bank']?>" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                              <label>Progress (%)</label>
                              <input type="text" name="progress" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control">
                          </div>
                        </div>
                    </div>
                      <div class="form-group">
                        <label>Description Progress</label>
                        <textarea name="description" class="form-control" rows="5"></textarea>
                      </div>
                    <div class="form-group">
                      <label for="staticEmail">Next Payment</label>
                      <input type="text" name="total" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control">
                    </div>
                    <div id="repeat2">
                      <div class="form-group">
                        <label for="design">Design</label>
                        <input type="file" name="attachment[]" class="form-control" id="file">
                      </div>
                    </div>
                    <div class="form-group" style="text-align: right;">
                        <a class="btn btn-default mt-1 mb-1" id="tambah2">Add More</a>
                    </div>
                    <div class="form-group">
                        <button class="btn yellow sbold btn-block" type="submit">Submit</button>
                    </div>
                </form>
            </div>
            </div>
          </div>
      </div>
      <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="basicdp" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Submit RAB</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12" style="display: block;text-align: center">
              <form style="display: inline-block;text-align: left;width: 95%" method="post" action="<?= site_url('admin/dnb/input_dp')?>" enctype="multipart/form-data">
                  <input type="hidden" name="code" value="<?= $data['data']['code'] ?>" class="form-control">
                  <input type="hidden" name="customer_id" value="<?= $data['data']['customer_id'] ?>" class="form-control">
                  <input type="hidden" name="bank" value="<?= $data['invoice']['bank']?>" class="form-control">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Upload RAB</label>
                        <input type="file" name="attachment" class="form-control" id="file">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Total Biaya Pengerjaan</label>
                          <input type="text" name="total_dnb" class="form-control" id="currency1">

                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>DP (%)</label>
                          <input type="text" name="dp" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control" value="5">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                      <button class="btn yellow sbold btn-block" type="submit">Submit</button>
                  </div>
              </form>
          </div>
          </div>
        </div>
    </div>
      <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="basicdp2" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Submit Design</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12" style="display: block;text-align: center">
              <form style="display: inline-block;text-align: left;width: 95%" method="post" action="<?= site_url('admin/dnb/submit_design')?>" enctype="multipart/form-data">
                <input type="hidden" name="code" value="<?= $data['data']['code']?>">
                <input type="hidden" name="customer_id" value="<?= $data['data']['customer_id'] ?>" class="form-control">
                <div id="repeat">
                  <div class="form-group">
                    <label for="design">Design</label>
                    <input type="file" name="attachment[]" class="form-control" id="file">
                  </div>
                </div>
                <div class="form-group" style="text-align: right;">
                    <a class="btn btn-default mt-1 mb-1" id="tambah">Add More</a>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn yellow sbold">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
      <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('#tambah').click(function(){
    var div = $('#repeat');
    div.append('<div class="form-group"><label for="design">Design</label><input type="file" name="attachment[]" class="form-control" id="file"></div>');
  });

  $('#tambah2').click(function(){
    var div = $('#repeat2');
    div.append('<div class="form-group"><label for="design">Design</label><input type="file" name="attachment[]" class="form-control" id="file"></div>');
  });
}); 
</script>