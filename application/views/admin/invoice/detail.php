<style type="text/css">
  .bottom {
    border-top : 0 !important;
    border-right  : 0 !important;
    border-left : 0 !important;
  }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Verification - <?= $data['code']?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=base_url('admin/invoice/accept_payment')?>" enctype="multipart/form-data">
                            <div class="form-body">
                              <input type="hidden" name="code" class="form-control" readonly value="<?= $data['code']?>">
                              <input type="hidden" name="customer_id" class="form-control" readonly value="<?= $data['customer_id']?>">
                              <input type="hidden" name="tbl" class="form-control" readonly value="<?= $data['tbl']?>">
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                  <div class="form-group">
                                    <label>Payment Method</label>
                                    <input type="text" name="payment" class="form-control" readonly value="<?= $data['bank']?>">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="form-group">
                                    <label>Bank Pengirim</label>
                                    <input type="text" name="harga_bangunan" class="form-control" readonly value="<?= $data['payment']?>">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="form-group">
                                    <label>Nama Pengirim</label>
                                    <input type="text" name="floor" class="form-control" readonly value="<?= $data['bank_account']?>">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" name="total" class="form-control" readonly value="Rp <?= number_format($data['total'])?>">
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-3">
                                  <label>Attachment File</label>
                                  <a href="<?= base_url('uploads/invoice/'.$data['attachment'])?>" target="_blank">
                                    <img style="width: 100%" src="<?= base_url('uploads/invoice/'.$data['attachment'])?>">
                                  </a>
                                </div>
                              </div>
                            </div>
                            <?php if($data['status'] == 1){?>
                            <div class="row">
                              <div class="col-md-4">
                                <div class="form-group" style="margin: 1em 0; ">
                                  <button type="submit" class="btn sbold yellow"> Accept</button>
                                </div>
                              </div>
                            </div>
                          <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>