<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th> No </th>
                                    <th> Code </th>
                                    <th> User </th>
                                    <th> Total </th>
                                    <th> Status </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $x = 1;
                                if(!empty($data)){
                                foreach ($data as $d) { ?>
                                <tr class="odd gradeX">
                                    <td><?= $x++ ?></td>
                                    <td><?=$d['code']?></td>
                                    <td><?=$d['first_name'].' '.$d['last_name']?></td>
                                    <td>Rp <?= number_format($d['total'])?></td>
                                    <td>
                                        <?php 
                                            if($d['status'] == 1){
                                                echo "Need Verification";
                                            }else{
                                                echo "Paid";
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs yellow dropdown-toggle" href="<?=base_url('admin/invoice/show/').$d['code']?>">Payment Check</a>
                                    </td>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>