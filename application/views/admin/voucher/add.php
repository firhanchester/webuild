<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1><?=$title;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="<?=site_url('admin/voucher/store')?>" enctype="multipart/form-data">
                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nama Voucher</label>
                                            <input type="text" class="form-control" name="voucher" required placeholder="contoh : discount20k , discount20">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tipe Voucher</label>
                                            <select class="form-control" name="type">
                                                <option value="nominal">Nominal</option>
                                                <option value="persen">Persen</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nominal / persen</label>
                                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control" name="nominal" required placeholder="untuk tipe nominal : 20000 untuk persen : 20">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Limit</label>
                                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control" name="limit" required placeholder="limit penggunaan">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tanggal Mulai</label>
                                            <input type="date" class="form-control" name="start" required >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tanggal Berakhir</label>
                                            <input type="date" class="form-control" name="end" required >
                                        </div>
                                    </div>
                                </div>

                                
                            </div>

                            <div class="form-actions">
                                <button type="submit" class="btn yellow">Submit</button>
                                <a href="<?= site_url("admin/banner")?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#content').wysihtml5({
        "font-styles": false,
        "image" : false
    });
</script>