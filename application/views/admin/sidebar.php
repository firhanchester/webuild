<div class="page-container">
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            <ul class="page-sidebar-menu  page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="nav-item start <?= $menu == 'dashboard' ? 'active open' : '' ?>">
                    <a href="<?=base_url('admin')?>" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li class="heading">
                    <h3 class="uppercase">Transaction</h3>
                </li>
                <li class="nav-item <?= $title == 'design' || $title == 'build' || $title == 'dnb' || $title == 'improve' ? 'active open' : '' ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <span class="title">Project</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?= $menu == 'design_ongoing' || $menu == 'design_verify' || $menu == 'design_pay' || $menu == 'design_detail' ? 'active' : '' ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Design
                                    <?php if($count_design != 0){?>
                                        <span class="badge badge-warning" style="float: right;">
                                            <?= $count_design ?>
                                        </span>
                                    <?php } ?>
                                </span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?= $menu == 'design_ongoing' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/design/design_ongoing')?>" class="nav-link ">
                                        <span class="title">On Going
                                            <?php if($count_design != 0){?>
                                                <span class="badge badge-warning" style="float: right;">
                                                    <?= $count_design ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item <?= $menu == 'design_pay' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/design/design_pay')?>" class="nav-link ">
                                        <span class="title">Complete</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item <?= $menu == 'build_pending' || $menu == 'build_verify' || $menu == 'build_pay' || $menu == 'build_detail' ? 'active' : '' ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Build
                                    <?php if($count_build != 0){?>
                                        <span class="badge badge-warning" style="float: right;">
                                            <?= $count_build ?>
                                        </span>
                                    <?php } ?>
                                </span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?= $menu == 'build_pending' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/build/build_pending')?>" class="nav-link ">
                                        <span class="title">On Going
                                            <?php if($count_build != 0){?>
                                                <span class="badge badge-warning" style="float: right;">
                                                    <?= $count_build ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item <?= $menu == 'build_pay' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/build/build_pay')?>" class="nav-link ">
                                        <span class="title">Complete</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item <?= $menu == 'dnb_pending' || $menu == 'dnb_verify' || $menu == 'dnb_pay' || $menu == 'dnb_detail' ? 'active' : '' ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Design And Build
                                    <?php if($count_dnb != 0){?>
                                        <span class="badge badge-warning" style="float: right;">
                                            <?= $count_dnb ?>
                                        </span>
                                    <?php } ?>
                                </span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?= $menu == 'dnb_pending' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/dnb/dnb_pending')?>" class="nav-link ">
                                        <span class="title">On Going
                                            <?php if($count_dnb != 0){?>
                                                <span class="badge badge-warning" style="float: right;">
                                                    <?= $count_dnb ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item <?= $menu == 'dnb_pay' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/dnb/dnb_pay')?>" class="nav-link ">
                                        <span class="title">Complete</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item <?= $menu == 'improve_pending' || $menu == 'improve_verify' || $menu == 'improve_pay' || $menu == 'improve_detail' ? 'active' : '' ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Improve
                                    <?php if($count_improve != 0){?>
                                        <span class="badge badge-warning" style="float: right;">
                                            <?= $count_improve ?>
                                        </span>
                                    <?php } ?>
                                </span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?= $menu == 'improve_pending' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/improve/improve_pending')?>" class="nav-link ">
                                        <span class="title">On Going
                                            <?php if($count_improve != 0){?>
                                                <span class="badge badge-warning" style="float: right;">
                                                    <?= $count_improve ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item <?= $menu == 'improve_pay' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/improve/improve_pay')?>" class="nav-link ">
                                        <span class="title">Complete</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?= $title == 'services' || $title == 'dp' || $title == 'progress' ? 'active open' : '' ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <span class="title">Invoice
                        <?php 
                            $c = $invoice['nv'] + $invoice_dp['nv'] + $invoice_progress['nv'];
                        ?>
                        <?php if($c != 0){?>
                            <span class="badge badge-warning" style="float: right;">
                                <?= $c ?>
                            </span>
                        <?php } ?>
                        </span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?= $menu == 'verification-services' || $menu == 'pay-services' ? 'active' : '' ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Services & Design

                                <?php if($invoice['nv'] != 0){?>
                                    <span class="badge badge-warning" style="float: right;">
                                        <?= $c ?>
                                    </span>
                                <?php } ?>
                                </span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?= $menu == 'verification-services' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/invoice/services/verification')?>" class="nav-link ">
                                        <span class="title">Need Verification 
                                            <?php if($invoice['nv'] != 0){?>
                                                <span class="badge badge-warning" style="float: right;">
                                                    <?= $c ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item <?= $menu == 'pay-services' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/invoice/services/pay')?>" class="nav-link">
                                        <span class="title">Paid
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item <?= $menu == 'verification-dp' || $menu == 'pay-dp' ? 'active' : '' ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Down Payment
                                    <?php if($invoice_dp['nv'] != 0){?>
                                        <span class="badge badge-warning" style="float: right;">
                                            <?= $c ?>
                                        </span>
                                    <?php } ?>
                                </span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?= $menu == 'verification-dp' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/invoice/dp/verification')?>" class="nav-link ">
                                        <span class="title">Need Verification
                                            <?php if($invoice_dp['nv'] != 0){?>
                                        <span class="badge badge-warning" style="float: right;">
                                            <?= $c ?>
                                        </span>
                                    <?php } ?>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item <?= $menu == 'pay-dp' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/invoice/dp/pay')?>" class="nav-link ">
                                        <span class="title">Paid
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item <?= $menu == 'verification-progress' || $menu == 'pay-progress' ? 'active' : '' ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Progress
                                    <?php if($invoice_progress['nv'] != 0){?>
                                        <span class="badge badge-warning" style="float: right;">
                                            <?= $c ?>
                                        </span>
                                    <?php } ?>
                                </span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?= $menu == 'verification-progress' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/invoice/progress/verification')?>" class="nav-link ">
                                        <span class="title">Need Verification
                                            <?php if($invoice_progress['nv'] != 0){?>
                                        <span class="badge badge-warning" style="float: right;">
                                            <?= $c ?>
                                        </span>
                                    <?php } ?>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item <?= $menu == 'pay-progress' ? 'active' : '' ?>">
                                    <a href="<?=base_url('admin/invoice/progress/pay')?>" class="nav-link ">
                                        <span class="title">Paid
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <li class="nav-item <?= $menu == 'users' || $menu == 'customer' ? 'active' : '' ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <span class="title">Users</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?= $menu == 'users' ? 'active' : '' ?>">
                            <a href="<?=base_url('admin/users')?>" class="nav-link ">
                                <span class="title">Admin</span>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'customer' ? 'active' : '' ?>">
                            <a href="<?=base_url('admin/customers')?>" class="nav-link ">
                                <span class="title">Customer</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?= $title == 'hide_design' || $title == 'hide_build' || $title == 'hide_dnb' || $title == 'hide_improve' ? 'active open' : '' ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <span class="title">Hidden Project</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?= $menu == 'hide_design' ? 'active' : '' ?>">
                            <a href="<?=base_url('admin/hide/design')?>" class="nav-link ">
                                <span class="title">Design</span>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'hide_build' ? 'active' : '' ?>">
                            <a href="<?=base_url('admin/hide/build')?>" class="nav-link ">
                                <span class="title">Build</span>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'hide_dnb' ? 'active' : '' ?>">
                            <a href="<?=base_url('admin/hide/dnb')?>" class="nav-link ">
                                <span class="title">Design & Build</span>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'hide_improve' ? 'active' : '' ?>">
                            <a href="<?=base_url('admin/hide/improve')?>" class="nav-link ">
                                <span class="title">Improve</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="heading">
                    <h3 class="uppercase">Content</h3>
                </li>
                <li class="nav-item <?= $menu == 'fimprove' ? 'active' : '' ?>">
                    <a href="<?=base_url('admin/fimprove')?>" class="nav-link ">
                        <span class="title">Improve Content</span>
                    </a>
                </li>
                <li class="nav-item <?= $menu == 'voucher' ? 'active' : '' ?>">
                    <a href="<?=base_url('admin/voucher')?>" class="nav-link ">
                        <span class="title">Voucher</span>
                    </a>
                </li>
                <li class="nav-item <?= $menu == 'portofolio' ? 'active' : '' ?>">
                    <a href="<?=base_url('admin/portofolio')?>" class="nav-link ">
                        <span class="title">Portofolio</span>
                    </a>
                </li>
                <li class="nav-item <?= $menu == 'banner' || $menu == 'banner_home' ? 'active' : '' ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <span class="title">Banner</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?= $menu == 'banner_home' ? 'active' : '' ?>">
                            <a href="<?=base_url('admin/banner_home')?>" class="nav-link ">
                                <span class="title">Banner Home</span>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'banner' ? 'active' : '' ?>">
                            <a href="<?=base_url('admin/banner')?>" class="nav-link ">
                                <span class="title">Banner Category</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?= $menu == 'component' ? 'active' : '' ?>">
                    <a href="<?=base_url('admin/component')?>" class="nav-link ">
                        <span class="title">Component</span>
                    </a>
                </li>
                <li class="nav-item <?= $menu == 'article' ? 'active' : '' ?>">
                    <a href="<?=base_url('admin/article')?>" class="nav-link ">
                        <span class="title">Article</span>
                    </a>
                </li>
                
            </ul>
        </div>
    </div>