<?php

class Modvoucher extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    function get_data($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id',$id);
        }
        $query = $this->db->get('voucher');
        if($query->num_rows() > 0)
            if (is_null($id)) {
                return $query->result_array();
            }else{
                return $query->row_array();
            }
        else
            return false;
    }

    function get_voucher($voucher){
        return $this->db->get_where('voucher',['voucher' => $voucher])->row_array();
    }


    function add($data){
        $this->db->insert('voucher',$data);
        $insert_id = $this->db->insert_id();
        if($this->db->affected_rows() > 0){
            return ['success' => true , 'id' => $insert_id ];
        }else{
            return ['success' => false];
        }
    }

    function update($data){
        $this->db->where('id',$data['id']);
        $this->db->update('voucher',$data);

        if($this->db->affected_rows() > 0){
            return ['success' => true];
        }else{
            return ['success' => false];
        }
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('voucher');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

}
?>