<?php

class Users_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    function get_data($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id',$id);
        }
        $query = $this->db->get('users');
        if($query->num_rows() > 0)
            if (is_null($id)) {
                return $query->result_array();
            }else{
                return $query->row_array();
            }
        else
            return false;
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('users');

        if($this->db->affected_rows() > 0){
            $this->db->where('user_id', $id);
            $this->db->delete('users_groups');
            return true;
        }else{
            return false;
        }
    }
}
?>
