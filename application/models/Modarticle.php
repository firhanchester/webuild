<?php

class Modarticle extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    function get_data($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id',$id);
        }
        $query = $this->db->get('article');
        if($query->num_rows() > 0)
            if (is_null($id)) {
                return $query->result_array();
            }else{
                return $query->row_array();
            }
        else
            return false;
    }

    function add($data){
        $this->db->insert('article',$data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function update($id,$data){
        $this->db->where('id',$id);
        $this->db->update('article',$data);

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('article');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
}
?>
