<?php

class Modcomponent extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    function get_data($id = null)
    {
        if (!is_null($id)) {
            $this->db->select('*');
            $this->db->from('levels');
            $this->db->where('id',$id);
            $l = $this->db->get()->row_array();
            $comp = $this->db->get_where("component",['type' => $l['id']])->result_array();
            $com = [
                'id' => $l['id'],
                'name' => $l['name'],
                'component' => $comp,
                
            ];
            return $com;
        }else{
            $this->db->select('l.*');
            $this->db->from('levels l');
            $this->db->join('component c','l.id = c.type');
            $this->db->group_by('l.id');
            $query = $this->db->get();
            if($query->num_rows() > 0)
                if (is_null($id)) {
                    return $query->result_array();
                }else{
                    return $query->result_array();
                }
            else
                return null;
        }
    }

    function get_data_list($id){
        $this->db->select('*');
        $this->db->from('levels');
        $this->db->where('id',$id);
        $l = $this->db->get()->row_array();
        $comp = $this->db->get_where("component",['type' => $l['id']])->result_array();
        $com = [
            'id' => $l['id'],
            'name' => $l['name'],
            'component' => $comp,
            
        ];
        return $com;
    }

    function add($data){
        $this->db->insert('component',$data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function delete($id){
        $this->db->where('type', $id);
        $this->db->delete('component');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function get_list_improve(){
        return $this->db->get('improve')->result_array();
    }

    function get_detail_improve($id){
        $this->db->select('*');
        $this->db->from('improve');
        $this->db->where('id',$id);
        $this->db->or_where('name',$id);
        $i = $this->db->get()->row_array();
        $comp = $this->db->get_where("improve_comp",['improve_id' => $i['id']])->result_array();
        $com = [
            'id' => $i['id'],
            'name' => $i['name'],
            'component' => $comp,
            
        ];
        return $com;
    }

    function add_improve($data){
        $this->db->insert('improve',$data);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function add_improve_comp($data){
        $this->db->insert('improve_comp',$data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function update_improve($data){
        $this->db->where('id',$data['id']);
        $this->db->update('improve',$data);
        if($this->db->affected_rows() > 0){
            return $data['id'];
        }else{
            return false;
        }
    }

    function update_improve_comp($data){
        $this->db->where('id',$data['id']);
        $this->db->update('improve_comp',$data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function delete_improve($id){
        $this->db->where('id', $id);
        $this->db->delete('improve');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function delete_comp($id){
        $this->db->where('id', $id);
        $this->db->delete('improve_comp');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
}
?>
