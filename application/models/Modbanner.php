<?php

class Modbanner extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    function get_data($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id',$id);
        }

        $query = $this->db->get('banner');
        if($query->num_rows() > 0)
            if (is_null($id)) {
                return $query->result_array();
            }else{
                return $query->row_array();
            }
        else
            return false;
    }

    function get_data_cat($id = null, $cat = null)
    {
        $this->db->select('bc.* , p.title , pi.image');
        $this->db->from('banner_cat bc');
        $this->db->join('portofolio p','bc.portofolio_id = p.id');
        $this->db->join('portofolio_image pi','bc.portofolio_id = pi.porto_id');
        $this->db->group_by('bc.portofolio_id');
        if (!is_null($id)) {
            $this->db->where('bc.id',$id);
        }else if(!is_null($cat)){
            $this->db->where('bc.category',$cat);
        }
        $query = $this->db->get();

        if($query->num_rows() > 0)
            if (is_null($id)) {
                return $query->result_array();
            }else{
                return $query->row_array();
            }
        else
            return false;
    }

    function add($data){
        $this->db->insert('banner',$data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function add_cat($data){
        $this->db->insert('banner_cat',$data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function update($id,$data){
        $this->db->where('id',$id);
        $this->db->update('banner',$data);

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('banner');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function delete_cat($id){
        $this->db->where('id', $id);
        $this->db->delete('banner_cat');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function get_porto($cat){
        $this->db->like('type', $cat, 'both');  
        $q = $this->db->get('portofolio');

        if($this->db->affected_rows() > 0){
            return ['success' => true , 'data' => $q->result_array() ];
        }else{
            return ['success' => false];
        }
    }
}
?>
