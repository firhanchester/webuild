<?php

class Customer extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    function get_data($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id',$id);
        }
        $query = $this->db->get('customer');
        if($query->num_rows() > 0)
            if (is_null($id)) {
                return $query->result_array();
            }else{
                return $query->row_array();
            }
        else
            return false;
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('customer');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function sosmed_user($data){
        $now = date('Y-m-d H:i:s'); 
        $this->db->select('*');
        $this->db->where('uniq_id',$data['uniq_id']);
        $this->db->or_where('email', $data['email']);
        $q = $this->db->get('customer');
        if($q->num_rows() == 0){
            $password   = SHA1($data['name']);
            $data_insert = [
                'username' => $data['name'],
                'password' => $password,
                'email' => $data['email'],
                'last_login' => $now,
                'created_date' => $now,
                'picture' => $data['picture'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'uniq_id' => $data['uniq_id'],
            ];
            $this->db->insert('customer',$data_insert);
            if ($this->db->affected_rows() > 0) {
                return [ 'success' => true, 'message' => '' ];
            } else {
                return [ 'success' => false, 'message' => $this->db->error()['message'] ];
            }
        }
    }

    function login($data){
        $this->db->select('*');
        $this->db->where('email', $data['email']);
        $q = $this->db->get('customer');
        if($q->num_rows() > 0){
            $this->db->select('*');
            $this->db->where('email', $data['email']);
            $this->db->where('password', $data['password']);
            $q = $this->db->get('customer');
            if($q->num_rows() > 0){
                return ['success' => true , 'msg' => ''];
            }else{
                return ['success' => false , 'msg' => 'Password anda Salah'];
            }
        }else{
                return ['success' => false , 'msg' => 'Email Tidak Terdaftar'];
        }
    }

    function cek_email($email){
        $this->db->select('*');
        $this->db->where('email', $email);
        $q = $this->db->get('customer');
        if($q->num_rows() > 0){
            return ['success' => true , 'data' => $q->row_array()];
        }else{
            return ['success' => false];
        }
    }

    function add_token($data){
        $this->db->insert('token_password',$data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function add_token_regis($data){
        $this->db->insert('token_regis',$data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function get_user_by_token($token){
        return $this->db->get_where('token_password',['token' => $token])->row_array();
    }

    function get_user_by_token_regis($token){
        return $this->db->get_where('token_regis',['token' => $token])->row_array();
    }

    function update_password($data){
        $this->db->where('id',$data['id']);
        $this->db->update('customer',['password' => $data['password'] ]);
        if($this->db->affected_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }

    function regis($data){
        $this->db->insert('customer',$data);
        if($this->db->affected_rows() > 0){
            $q =  $this->db->get_where('customer',['uniq_id' => $data['uniq_id'] ]);
            return ['success' => true , 'data' => $q->row_array()];
        }else{
            return false;
        }
    }

    function get_identity($data){
        $this->db->select('*');
        $this->db->where('uniq_id',$data['uniq_id']);
        $this->db->or_where('email', $data['email']);
        $q = $this->db->get('customer');
        return $q->row_array();
    }

    function get_user($id){
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('uniq_id', $id);
        $this->db->or_where('email', $id);
        $q = $this->db->get()->row_array();
        return $q;
    }

    function notif($id){
        return $this->db->get_where('notif',['id' => $id])->row_array();
    }

    function update_notif($id){
        $this->db->where('id',$id);
        $this->db->update('notif',['status' => 1]);
        if($this->db->affected_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }

    public function sendmail_forgot($params) {
        $this->load->library('parser');
        $this->db->select('content');
        $this->db->where('type','forgot');
        $body = $this->db->get('config')->row()->content;

        $data = array(
            'link' => $params['link'],
        );

        $content = $this->parser->parse_string($body,$data);
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.googlemail.com',
            'smtp_user' => 'webuild.ptiki@gmail.com',
            'smtp_pass' => 'webuildsm123',
            'smtp_port' => 587,
            'smtp_crypto' => 'tls',
            'charset'   => 'utf-8'
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->from('webuild.ptiki@gmail.com', 'WeBuild Indonesia');
        $this->email->to($params['email']);
        $this->email->subject('Forgot Password');
        $this->email->message($content);
        $this->email->send();

    }

    public function sendmail_regis($params) {
        $this->load->library('parser');
        $this->db->select('content');
        $this->db->where('type','forgot');
        $body = $this->db->get('config')->row()->content;

        $data = array(
            'link' => $params['link'],
        );

        $content = $this->parser->parse_string($body,$data);
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.googlemail.com',
            'smtp_user' => 'webuild.ptiki@gmail.com',
            'smtp_pass' => 'webuildsm123',
            'smtp_port' => 587,
            'smtp_crypto' => 'tls',
            'charset'   => 'utf-8'
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->from('webuild.ptiki@gmail.com', 'WeBuild Indonesia');
        $this->email->to($params['email']);
        $this->email->subject('Forgot Password');
        $this->email->message($content);
        $this->email->send();

    }
}
?>
