<?php

class Modportofolio extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    function get_data()
    {
        $query = $this->db->get('portofolio');
        if($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
    }

    function get_all_portofolio(){
        $this->db->limit(5);
        $porto = $this->db->get('portofolio');
        if ($porto->num_rows() > 0) {
            foreach($porto->result_array() as $p){
                $image = $this->db->get_where('portofolio_image',['porto_id' => $p['id'] ])->result_array();
                $res[] = [
                    'id' => $p['id'],
                    'title' => $p['title'],
                    'price' => $p['price'],
                    'slug' => $p['slug'],
                    'province' => $p['province'],
                    'city' => $p['city'],
                    'image' => $image,
                ];
            }
            return [
                'success' => true,
                'message' => 'data found',
                'total' => $porto->num_rows(),
                'data' => $res,
            ];
        }else{
            return [
                'success' => false,
                'message' => 'data not found',
                'data' => '',
            ];
        }
    }

    function get_all_portofolio_pagination($limit=null ,$offset=null){
        $porto = $this->db->get('portofolio',$limit,$offset);
        // var_dump($porto->result_array());exit;
        if ($porto->num_rows() > 0) {
            foreach($porto->result_array() as $p){
                $image = $this->db->get_where('portofolio_image',['porto_id' => $p['id'] ])->result_array();
                $res[] = [
                    'id' => $p['id'],
                    'title' => $p['title'],
                    'price' => $p['price'],
                    'slug' => $p['slug'],
                    'province' => $p['province'],
                    'city' => $p['city'],
                    'image' => $image,
                ];
            }
            return [
                'success' => true,
                'message' => 'data found',
                'total' => $porto->num_rows(),
                'data' => $res,
            ];
        }else{
            return [
                'success' => false,
                'message' => 'data not found',
                'data' => '',
            ];
        }
    }

    function get_image_by_id($id){
        return $this->db->get_where('portofolio_image',['porto_id' => $id])->result_array();
    }

    function get_portofolio($slug){
        $porto = $this->db->get_where('portofolio',['slug' => $slug])->row_array();
        $image = $this->db->get_where('portofolio_image',['porto_id' => $porto['id'] ])->result_array();
        $comp = $this->db->get_where('portofolio_component',['porto_id' => $porto['id'] ])->result_array();
        return [
            'porto' => $porto,
            'image' => $image,
            'comp' => $comp,
        ];
    }

    function get_portofolio_by_id($id){
        $porto = $this->db->get_where('portofolio',['id' => $id])->row_array();
        $image = $this->db->get_where('portofolio_image',['porto_id' => $id ])->result_array();
        $comp = $this->db->get_where('portofolio_component',['porto_id' => $id ])->result_array();
        return [
            'porto' => $porto,
            'image' => $image,
            'comp' => $comp,
        ];
    }

    function add($data){
        $this->db->insert('portofolio',$data);
        $insert_id = $this->db->insert_id();
        if($this->db->affected_rows() > 0){
            return ['success' => true , 'id' => $insert_id ];
        }else{
            return ['success' => false];
        }
    }

    function add_image($data){
        $this->db->insert('portofolio_image',$data);
        if($this->db->affected_rows() > 0){
            return ['success' => true];
        }else{
            return ['success' => false];
        }
    }

    function update_image($data,$update = null){
        //ini fungsi kalo add product
        if ($update == null) {
            $this->db->insert('portofolio_image',$data);
            if ($this->db->affected_rows() > 0) {
                return [ 'success' => true, 'message' => ''];
            } else {
                if ($this->db->error()['code'] !== 0) {
                    return [ 'success' => false, 'message' => $this->db->error()['message'] ];
                } else {
                    return [ 'success' => false, 'message' => 'Data failed inserted!' ];
                }
            }
        } else {
            // ini buat update.
            $this->db->like('image', $update);
            $this->db->update('portofolio_image', $data);
            if ($this->db->affected_rows() > 0) {
                return [ 'success' => true, 'message' => ''];
            } else {
                if ($this->db->error()['code'] !== 0) {
                    return [ 'success' => false, 'message' => $this->db->error()['message'] ];
                } else {
                    return [ 'success' => false, 'message' => 'Data failed updated!' ];
                }
            }
        }

    }

    function add_comp($data){
        $this->db->insert('portofolio_component',$data);
        if($this->db->affected_rows() > 0){
            return ['success' => true];
        }else{
            return ['success' => false];
        }
    }

    function update($id , $data){
        $this->db->where('id',$id);
        $this->db->update('portofolio',$data);

        if($this->db->affected_rows() > 0){
            return ['success' => true];
        }else{
            return ['success' => false];
        }
    }

    function update_comp($id,$data){
        $this->db->delete('portofolio_component',['porto_id' => $id]);

        foreach ($data as $comp) {
            $this->db->insert('portofolio_component',['porto_id' => $id, 'component' => $comp['component'] , 'spec' => $comp['spec']]);
        }

        if ($this->db->affected_rows()) {
            return [
                'success' => true,
                'message' => 'data has inserted',
            ];
        }else{
            return [
                'success' => false,
                'message' => 'data has not inserted',
            ];
        }
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('portofolio');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

}
?>