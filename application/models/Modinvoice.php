<?php

class Modinvoice extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    function add($data){
        $this->db->insert('invoice',$data);
        if($this->db->affected_rows() > 0){
            return ['success' => true , 'code' => $data['code'] ];
        }else{
            return ['success' => false];
        }
    }

    function update($code , $data){
        $this->db->where('code',$code);
        $this->db->update('invoice',$data);

        if($this->db->affected_rows() > 0){
            return ['success' => true];
        }else{
            return ['success' => false];
        }
    }

    function update_dp($code , $data){
        $this->db->where('code',$code);
        $this->db->update('invoice_dp',$data);

        if($this->db->affected_rows() > 0){
            return ['success' => true];
        }else{
            return ['success' => false];
        }
    }

    function update_progress($id , $data){
        $this->db->where('id',$id);
        $this->db->update('invoice_progress',$data);

        if($this->db->affected_rows() > 0){
            return ['success' => true];
        }else{
            return ['success' => false];
        }
    }

    function update_progress_by_id($id , $data){
        $this->db->where('id',$id);
        $this->db->update('invoice_progress',$data);

        if($this->db->affected_rows() > 0){
            return ['success' => true];
        }else{
            return ['success' => false];
        }
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('invoice');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function get_invoice($status){
        $this->db->select('i.* , c.first_name , c.last_name');
        $this->db->from('invoice i');
        $this->db->join('customer c','c.id = i.customer_id');
        $this->db->where('i.status',$status);
        if($status == 1){
            $this->db->where('i.bank !=',NULL);
        }
        $this->db->order_by('id','asc');
        $q = $this->db->get();
        return $q->result_array();
    }

    function get_invoice_dp($status){
        $this->db->select('i.* , c.first_name , c.last_name');
        $this->db->from('invoice_dp i');
        $this->db->join('customer c','c.id = i.customer_id');
        $this->db->where('i.status',$status);
        if($status == 1){
            $this->db->where('i.bank !=',NULL);
        }

        $q = $this->db->get();
        return $q->result_array();
    }

    function get_dp_by_code($code){
        return $this->db->get_where('invoice_dp',['code' => $code])->row_array();
    }

    function get_invoice_progress($status){
        $this->db->select('i.* , c.first_name , c.last_name');
        $this->db->from('invoice_progress i');
        $this->db->join('customer c','c.id = i.customer_id');
        $this->db->where('i.status',$status);
        if($status == 1){
            $this->db->where('i.bank !=',NULL);
        }

        $q = $this->db->get();
        return $q->result_array();
    }

    function get_invoice_by_code($code){
        $this->db->where('code',$code);
        $q = $this->db->get('invoice');
        if($q->num_rows() > 0){
            return $q->row_array();
        }else{
            return false;
        }
    }

    function get_invoice_dp_by_code($code){
        $this->db->where('code',$code);
        $q = $this->db->get('invoice_dp');
        if($q->num_rows() > 0){
            return $q->row_array();
        }else{
            return false;
        }
    }

    function get_invoice_progress_by_id($id){
        $this->db->where('id',$id);
        $q = $this->db->get('invoice_progress');
        if($q->num_rows() > 0){
            return $q->row_array();
        }else{
            return false;
        }
    }    

    function get_invoice_improve_by_code($code){
        $this->db->select('i.* , pi.type');
        $this->db->from('invoice i');
        $this->db->join('project_improve pi','pi.code = i.code');
        $this->db->where('i.code',$code);
        $q = $this->db->get();
        if($q->num_rows() > 0){
            return $q->row_array();
        }else{
            return false;
        }
    }

    function add_invoice_dp($data){
        $this->db->insert('invoice_dp',$data);
        if ($this->db->affected_rows() > 0) {
            return ['success' => true , 'code' => $data['code'] ];
        }else{
            return ['success' => false];
        }
    }

    function add_invoice_progress($data){
        $this->db->insert('invoice_progress',$data);
        if ($this->db->affected_rows() > 0) {
            return ['success' => true , 'code' => $data['code'] ];
        }else{
            return ['success' => false];
        }
    }

    function get_all_invoice_by_code($code){
        $d = [];
        $this->db->select('code,customer_id,tbl,status,created_date');
        $this->db->from('invoice');
        $this->db->where('code',$code);
        $dat1 = $this->db->get()->result_array();
        // $d[] = $dat1;
        foreach ($dat1 as $invoice) {
            $invoice['title'] = 'invoice Service';
            $d[] = $invoice;
        }

        $this->db->select('code,customer_id,status,created_date');
        $this->db->from('invoice_dp');
        $this->db->where('code',$code);
        $dat2 = $this->db->get()->result_array();
        // $d[] = $dat2;
        foreach ($dat2 as $key => $invoice_dp) {
            $invoice_dp['title'] = 'invoice DP';
            $invoice_dp['tbl'] = 'dp';
            $no = intval($key + 1);
            $d[] = $invoice_dp;
        }

        $this->db->select('id,code,customer_id,status,created_date');
        $this->db->from('invoice_progress');
        $this->db->where('code',$code);
        $dat3 = $this->db->get()->result_array();
        // $d += $dat3;
        foreach ($dat3 as $key => $progress) {
            $no = intval($key + 1);
            $progress['title'] = 'invoice Progress '.$no;
            $progress['tbl'] = 'progress';
            $d[] = $progress;
        }

        return $d;
    }

    function get_count_invoice_by_code($code){
        $d = [];
        $this->db->select('code,customer_id,tbl,status,created_date');
        $this->db->from('invoice');
        $this->db->where('code',$code);
        $this->db->where('status',0);
        $dat1 = $this->db->get()->result_array();
        // $d[] = $dat1;
        foreach ($dat1 as $invoice) {
            $invoice['title'] = 'invoice Service';
            $d[] = $invoice;
        }

        $this->db->select('code,customer_id,status,created_date');
        $this->db->from('invoice_dp');
        $this->db->where('code',$code);
        $this->db->where('status',0);
        $dat2 = $this->db->get()->result_array();
        // $d[] = $dat2;
        foreach ($dat2 as $key => $invoice_dp) {
            $invoice_dp['title'] = 'invoice DP';
            $invoice_dp['tbl'] = 'dp';
            $no = intval($key + 1);
            $d[] = $invoice_dp;
        }

        $this->db->select('id,code,customer_id,status,created_date');
        $this->db->from('invoice_progress');
        $this->db->where('code',$code);
        $this->db->where('status',0);
        $dat3 = $this->db->get()->result_array();
        // $d += $dat3;
        foreach ($dat3 as $key => $progress) {
            $no = intval($key + 1);
            $progress['title'] = 'invoice Progress '.$no;
            $progress['tbl'] = 'progress';
            $d[] = $progress;
        }

        return $d;
    }

}

?>