<?php

class Modproject extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    function get_data($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id',$id);
        }
        $query = $this->db->get('project');
        if($query->num_rows() > 0)
            if (is_null($id)) {
                return $query->result_array();
            }else{
                return [
                    'success' => true , 
                    'data' => $query->row_array()
                ];
            }
        else
            return false;
    }

    function get_all_design(){
        $this->db->select('count(id) as total');
        $this->db->where('status >',1);
        $this->db->where('cat','design');
        $q = $this->db->get('project');
        return $q->row_array();
    }

    function get_count_design(){
        $this->db->select('count(id) as total');
        $this->db->where('status',2);
        $this->db->where('cat','design');
        $q = $this->db->get('project');
        return $q->row_array()['total'];
    }

    function get_all_build(){
        $this->db->select('count(id) as total');
        $this->db->where('status >',1);
        $q = $this->db->get('project_design');
        return $q->row_array();
    }

    function get_count_build(){
        $this->db->select('count(id) as total');
        $this->db->where('status',2);
        $q = $this->db->get('project_design');
        return $q->row_array()['total'];
    }

    function get_all_dnb(){
        $this->db->select('count(id) as total');
        $this->db->where('status >',1);
        $this->db->where('cat','dnb');
        $q = $this->db->get('project');
        return $q->row_array();
    }

    function get_count_dnb(){
        $this->db->select('count(id) as total');
        $this->db->where('status',2);
        $this->db->where('cat','dnb');
        $q = $this->db->get('project');
        return $q->row_array()['total'];
    }

    function get_all_improve(){
        $this->db->select('count(id) as total');
        $this->db->where('status >',1);
        $q = $this->db->get('project_improve');
        return $q->row_array();
    }

    function get_count_improve(){
        $this->db->select('count(id) as total');
        $this->db->where('status',2);
        $q = $this->db->get('project_improve');
        return $q->row_array()['total'];
    }

    function get_all_invoice(){
        $this->db->select('count(id) as total');
        $this->db->where('status',1);
        $q = $this->db->get('invoice');
        $res['nv'] = $q->row_array()['total'];

        $this->db->select('count(id) as total');
        $this->db->where('status >',1);
        $q = $this->db->get('invoice');
        $res['paid'] = $q->row_array()['total'];

        $this->db->select('count(id) as total');
        $this->db->where('status >',0);
        $q = $this->db->get('invoice');
        $res['total'] = $q->row_array()['total'];

        return $res;
    }

    function get_all_invoice_dp(){
        $this->db->select('count(id) as total');
        $this->db->where('status',1);
        $q = $this->db->get('invoice_dp');
        $res['nv'] = $q->row_array()['total'];

        $this->db->select('count(id) as total');
        $this->db->where('status >',1);
        $q = $this->db->get('invoice_dp');
        $res['paid'] = $q->row_array()['total'];

        $this->db->select('count(id) as total');
        $this->db->where('status >',0);
        $q = $this->db->get('invoice_dp');
        $res['total'] = $q->row_array()['total'];

        return $res;
    }
    function get_all_invoice_progress(){
        $this->db->select('count(id) as total');
        $this->db->where('status',1);
        $q = $this->db->get('invoice_progress');
        $res['nv'] = $q->row_array()['total'];

        $this->db->select('count(id) as total');
        $this->db->where('status >',1);
        $q = $this->db->get('invoice_progress');
        $res['paid'] = $q->row_array()['total'];

        $this->db->select('count(id) as total');
        $this->db->where('status >',0);
        $q = $this->db->get('invoice_progress');
        $res['total'] = $q->row_array()['total'];

        return $res;
    }

    // backend

    function get_design($status){
        $this->db->select('p.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project p');
        $this->db->join('customer c','p.customer_id = c.id');
        $this->db->where('p.cat','design');
        if($status == 0){
            $this->db->where('p.status <',2);
        }else{
            $this->db->where('p.status',$status);
        }
        $this->db->order_by('p.id','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_dnb($status){
        $this->db->select('p.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project p');
        $this->db->join('customer c','p.customer_id = c.id');
        $this->db->where('p.cat','dnb');
        if($status == 0){
            $this->db->where('p.status <',2);
        }else{
            $this->db->where('p.status',$status);
        }
        $this->db->order_by('p.id','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_improve($status){
        $this->db->select('pi.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project_improve pi');
        $this->db->join('customer c','pi.customer_id = c.id');
        if($status == 0){
            $this->db->where('pi.status <',2);
        }else{
            $this->db->where('pi.status',$status);
        }
        $this->db->order_by('pi.id','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_build($status){
        $this->db->select('pd.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project_design pd');
        $this->db->join('customer c','pd.customer_id = c.id');
        if($status == 0){
            $this->db->where('pd.status <',2);
        }else{
            $this->db->where('pd.status',$status);
        }
        $this->db->order_by('pd.id','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_design_by_id($code){
        $this->db->select('p.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project_design p');
        $this->db->join('customer c','p.customer_id = c.id');
        $this->db->where('p.code',$code);
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_design_by_code($code){
        $this->db->select('p.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project_design p');
        $this->db->join('customer c','p.customer_id = c.id');
        $this->db->where('p.code',$code);
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_filedesign_by_code($code){
        return $this->db->get_where('project_file',['code' => $code ])->result_array();
    }

    function get_project_build_by_code($code){
        $this->db->select('pd.* , c.first_name , c.last_name , i.bank');
        $this->db->from('project_design pd');
        $this->db->join('customer c','pd.customer_id = c.id');
        $this->db->join('invoice i','i.code = pd.code');
        $this->db->where('pd.code',$code);
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_by_code($code){
        $this->db->select('p.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project p');
        $this->db->join('customer c','p.customer_id = c.id');
        $this->db->where('p.code',$code);
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_project_improve_by_code($code){
        $this->db->select('pi.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project_improve pi');
        $this->db->join('customer c','pi.customer_id = c.id');
        $this->db->where('pi.code',$code);
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_project_improve_by_codes($code){
        $this->db->select('p.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project_improve p');
        $this->db->join('customer c','p.customer_id = c.id');
        $this->db->where('p.code',$code);
        $query = $this->db->get()->row_array();
        $inv = $this->db->get_where('invoice',['code' => $code ])->row_array();
        $inv_dp = $this->db->get_where('invoice_dp',['code' => $code ])->row_array();
        $pi = $this->db->get_where('project_file',['code' => $code ])->result_array();
        return [
            'data' => $query , 
            'invoice' => $inv,
            'invoice_dp' => $inv_dp,
            'file' => $pi,
        ];
    }
    
    function get_project_by_code($code){
        $this->db->select('p.* , p.created_date as c_date, pv.*, c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project p');
        $this->db->join('customer c','p.customer_id = c.id');
        $this->db->join('project_comp pv','p.code = pv.code','left');
        $this->db->where('p.code',$code);
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_project_req($code){
        $this->db->select('p.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project p');
        $this->db->join('customer c','p.customer_id = c.id');
        $this->db->where('p.code',$code);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            $this->db->select('pd.* , c.first_name , c.last_name , c.email , c.phone');
            $this->db->from('project_design pd');
            $this->db->join('customer c','pd.customer_id = c.id');
            $this->db->where('pd.code',$code);
            $query = $this->db->get();
            return $query->row_array();
        }
    }

    function get_p_by_code($code){
        $this->db->select('p.* , c.first_name , c.last_name , c.email , c.phone');
        $this->db->from('project p');
        $this->db->join('customer c','p.customer_id = c.id');
        $this->db->where('p.code',$code);
        $query = $this->db->get()->row_array();
        $pro = $this->db->get_where('project_comp',['code' => $code ])->result_array();
        $inv = $this->db->get_where('invoice',['code' => $code ])->row_array();
        $inv_dp = $this->db->get_where('invoice_dp',['code' => $code ])->row_array();
        $pi = $this->db->get_where('project_file',['code' => $code ])->result_array();
        return [
            'data' => $query , 
            'comp' => $pro,
            'invoice' => $inv,
            'invoice_dp' => $inv_dp,
            'file' => $pi,
        ];
    }

    function get_levels(){
        $query = $this->db->get('levels');
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return false;   
    }

    function get_concept(){
        $query = $this->db->get('concept');
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return false;   
    }

    function get_list_improve($input = null){
        if($input != null){
            $this->db->where('name',$input);
        }
        $query = $this->db->get('improve');
        if($query->num_rows() > 0)
            if($input != null){
                return $query->row_array();
            }else{
                return $query->result_array();
            }
        else
            return false;   
    }

    function notif($customer_id){
        $this->db->where('customer_id',$customer_id);
        $this->db->where('status','0');
        $this->db->order_by('id','desc');
        $this->db->limit(5);
        $query = $this->db->get('notif');
        return $query->result_array();
    }

    function new_notif($customer_id){
        $this->db->where('customer_id',$customer_id);
        $this->db->where('status',0);
        $query = $this->db->get('notif');
        return $query->result_array();
    }

    function add($data){
        $this->db->insert('project',$data);
        if($this->db->affected_rows() > 0){
            return [
                'success' => true,
                'data' => $data['code'],
            ];
        }else{
            return [
                'success' => false,
                'data' => '',
            ];
        }
    }

    function add_file($data){
        $this->db->insert('project_file',$data);
        if($this->db->affected_rows() > 0){
            return [
                'success' => true,
                'data' => $data['code'],
            ];
        }else{
            return [
                'success' => false,
                'data' => '',
            ];
        }
    }

    function add_progress_image($data){
        $this->db->insert('progress_image',$data);
        if($this->db->affected_rows() > 0){
            return [
                'success' => true,
                'data' => $data['code'],
            ];
        }else{
            return [
                'success' => false,
                'data' => '',
            ];
        }
    }

    function add_rab($data){
        $this->db->insert('project_rab',$data);
        if($this->db->affected_rows() > 0){
            return [
                'success' => true,
                'data' => $data['code'],
            ];
        }else{
            return [
                'success' => false,
                'data' => '',
            ];
        }
    }

    function add_comp($data){
        $this->db->insert('project_comp',$data);
        if($this->db->affected_rows() > 0){
            return [
                'success' => true,
                'data' => $data['code'],
            ];
        }else{
            return [
                'success' => false,
                'data' => '',
            ];
        }
    }

    function add_improve($data){
        $this->db->insert('project_improve',$data);
        if($this->db->affected_rows() > 0){
            return [
                'success' => true,
                'data' => $data['code'],
            ];
        }else{
            return [
                'success' => false,
                'data' => '',
            ];
        }
    }

    function add_image($data){
        $this->db->insert('project_design',$data);
        if($this->db->affected_rows() > 0){
            return [
                'success' => true,
            ];
        }else{
            return [
                'success' => false,
            ];
        }
    }

    function input_harga($data){
        $this->db->insert('project_value',$data);
        if($this->db->affected_rows() > 0){
            $this->db->where('code',$data['code']);
            $this->db->update('project',['total' => $data['total'] , 'status' => $data['status'] ]);
            if($this->db->affected_rows() > 0){
                return [
                    'success' => true,
                ];
            }else{
                return [
                    'success' => false,
                ];    
            }
        }else{
            return [
                'success' => false,
            ];
        }   
    }

    function input_harga_improve($data){
        $this->db->where('code',$data['code']);
        $this->db->update('project_improve',['total' => $data['total'] , 'status' => $data['status'] ]);
        if($this->db->affected_rows() > 0){
            return [
                'success' => true,
            ];
        }else{
            return [
                'success' => false,
            ];
        }   
    }

    function input_harga_build($data){
        $this->db->where('code',$data['code']);
        $this->db->update('project_design',['total' => $data['total'] , 'status' => $data['status']]);
        if($this->db->affected_rows() > 0){
            return [
                'success' => true,
            ];
        }else{
            return [
                'success' => false,
            ];
        }   
    }

    function add_progress($data){
        $this->db->insert('project_progress',$data);
        $insert_id = $this->db->insert_id();
        if($this->db->affected_rows() > 0){
            return ['success' => true , 'data' => $insert_id];
        }else{
            return ['success' => false , 'data' => ''];
        }
    }

    function add_notif($data){
        $this->db->insert('notif',$data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function add_image_progress($data){
        $this->db->insert('project_image',$data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function get_progress($code){
        $q = $this->db->get_where('project_progress',['code' => $code]);
        return $q->result_array();
    }

    function get_progress_id($id){
        $p = $this->db->get_where('project_progress',['id' => $id])->row_array();
        $pi = $this->db->get_where('progress_image',['progress_id' => $id ])->result_array();
        return [
            'progress' => $p , 
            'image' => $pi,
        ];
    }

    function get_last_progress($code){
        $this->db->select('count(id) as total');
        $this->db->where('code',$code);
        $q = $this->db->get('invoice_progress');
        return $q->row_array(); 
    }

    function get_invoice_progress_fe($code){
        $q = $this->db->get_where('invoice_progress',['code' => $code]);
        return $q->result_array();
    }

    function get_inv_progress_by_id($id){
        $q = $this->db->get_where('invoice_progress',['id' => $id]);
        return $q->row_array();
    }

    function update_status($code,$data){
        $q = $this->db->get_where('project',['code' => $code]);
        if($q->num_rows() > 0){
            $this->db->where('code',$code);
            $this->db->update('project',['status' => $data['status'] ]);
            if($this->db->affected_rows() > 0){
                return true;
            }else{
                return false;
            }
        }else{
            $q = $this->db->get_where('project_improve',['code' => $code]);
            if($q->num_rows() > 0){
                $this->db->where('code',$code);
                $this->db->update('project_improve',['status' => $data['status'] ]);
                if($this->db->affected_rows() > 0){
                    return true;
                }else{
                    return false;
                }
            }else{
                $q = $this->db->get_where('project_design',['code' => $code]);
                if($q->num_rows() > 0){
                    $this->db->where('code',$code);
                    $this->db->update('project_design',['status' => $data['status'] ]);
                    if($this->db->affected_rows() > 0){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
        }
    }

    function hide($code){
        $q = $this->db->get_where('project',['code' => $code]);
        if($q->num_rows() > 0){
            $this->db->where('code',$code);
            $this->db->update('project',['status' => 5 ]);
            if($this->db->affected_rows() > 0){
                return true;
            }else{
                return false;
            }
        }else{
            $q = $this->db->get_where('project_improve',['code' => $code]);
            if($q->num_rows() > 0){
                $this->db->where('code',$code);
                $this->db->update('project_improve',['status' => 5 ]);
                if($this->db->affected_rows() > 0){
                    return true;
                }else{
                    return false;
                }
            }else{
                $q = $this->db->get_where('project_design',['code' => $code]);
                if($q->num_rows() > 0){
                    $this->db->where('code',$code);
                    $this->db->update('project_design',['status' => 5 ]);
                    if($this->db->affected_rows() > 0){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
        }
    }

    function show($code){
        $q = $this->db->get_where('project',['code' => $code]);
        if($q->num_rows() > 0){
            $this->db->where('code',$code);
            $this->db->update('project',['status' => 1 ]);
            if($this->db->affected_rows() > 0){
                return true;
            }else{
                return false;
            }
        }else{
            $q = $this->db->get_where('project_improve',['code' => $code]);
            if($q->num_rows() > 0){
                $this->db->where('code',$code);
                $this->db->update('project_improve',['status' => 1 ]);
                if($this->db->affected_rows() > 0){
                    return true;
                }else{
                    return false;
                }
            }else{
                $q = $this->db->get_where('project_design',['code' => $code]);
                if($q->num_rows() > 0){
                    $this->db->where('code',$code);
                    $this->db->update('project_design',['status' => 1 ]);
                    if($this->db->affected_rows() > 0){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
        }
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('project');

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function get_list($type){
        $this->db->select('p.*,pi.image');
        $this->db->from('project p');
        $this->db->join('project_image pi','p.code = pi.code','left');
        $this->db->where('p.type',$type);
        $query = $this->db->get();
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return null;
    }

    public function search($input){
        $this->db->select('p.*,pi.image');
        $this->db->from('portofolio p');
        $this->db->join('portofolio_image pi','p.id = pi.porto_id');
        $this->db->like('p.title', $input, 'both');
        $this->db->or_like('p.province', $input, 'both');
        $this->db->or_like('p.city', $input, 'both');
        $this->db->or_like('p.type', $input, 'both');
        $this->db->order_by('p.id','desc');
        $this->db->group_by('pi.porto_id');
        $query = $this->db->get();
        if($query->num_rows() > 0){

            return [
                'success' => true , 
                'data' => $query->result_array()
            ];

        }else{
            
            return [
                'success' => false , 
                'data' => ''
            ];
        }
    }

    public function sendmail_invoice_services($params,$type) {
        $this->load->library('parser');
        $this->db->select('content');
        if($type == 'design'){
            $this->db->where('type','invoice_design');
        }else if($type == 'dnb'){
            $this->db->where('type','invoice_dnb');
        }else if($type == 'build'){
            $this->db->where('type','invoice_build');
        }else if($type == 'improve'){
            $this->db->where('type','invoice_improve');
        }
        $body = $this->db->get('config')->row()->content;
        $data = array(
            'code' => $params['code'],
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            'total' => $params['total'],
            'tax' => $params['tax'],
            'uniq' => $params['uniq'],
            'bank' => $params['bank'],
        );

        $content = $this->parser->parse_string($body,$data);
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.googlemail.com',
            'smtp_user' => 'webuild.ptiki@gmail.com',
            'smtp_pass' => 'webuildsm123',
            'smtp_port' => 587,
            'smtp_crypto' => 'tls',
            'charset'   => 'utf-8'
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->from('webuild.ptiki@gmail.com', 'WeBuild Indonesia');
        $this->email->to($params['email']);
        $this->email->subject('Project - '.$params['code']);
        $this->email->message($content);
        $this->email->send();

    }

    public function sendmail_invoice_dp($params) {
        $this->load->library('parser');
        $this->db->select('content');
        $this->db->where('type','invoice_dp');
        $body = $this->db->get('config')->row()->content;
        $data = array(
            'code' => $params['code'],
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            'subtotal' => $params['subtotal'],
            'dp' => $params['dp'],
            'total' => $params['total'],
            'tax' => $params['tax'],
            'uniq' => $params['uniq'],
            'bank' => $params['bank'],
        );

        $content = $this->parser->parse_string($body,$data);
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.googlemail.com',
            'smtp_user' => 'webuild.ptiki@gmail.com',
            'smtp_pass' => 'webuildsm123',
            'smtp_port' => 587,
            'smtp_crypto' => 'tls',
            'charset'   => 'utf-8'
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->from('webuild.ptiki@gmail.com', 'WeBuild Indonesia');
        $this->email->to($params['email']);
        $this->email->subject('Project - '.$params['code']);
        $this->email->message($content);
        $this->email->send();

    }

    public function sendmail_invoice_progress($params) {
        $this->load->library('parser');
        $this->db->select('content');
        $this->db->where('type','invoice_dp');
        $body = $this->db->get('config')->row()->content;
        $data = array(
            'code' => $params['code'],
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            'subtotal' => $params['subtotal'],
            'dp' => $params['dp'],
            'total' => $params['total'],
            'tax' => $params['tax'],
            'uniq' => $params['uniq'],
            'bank' => $params['bank'],
        );

        $content = $this->parser->parse_string($body,$data);
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.googlemail.com',
            'smtp_user' => 'webuild.ptiki@gmail.com',
            'smtp_pass' => 'webuildsm123',
            'smtp_port' => 587,
            'smtp_crypto' => 'tls',
            'charset'   => 'utf-8'
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->from('webuild.ptiki@gmail.com', 'WeBuild Indonesia');
        $this->email->to($params['email']);
        $this->email->subject('Project - '.$params['code']);
        $this->email->message($content);
        $this->email->send();

    }

    public function sendmail_accept($params) {
        $this->load->library('parser');
        $this->db->select('content');
        $this->db->where('type','confirm_payment');
        $body = $this->db->get('config')->row()->content;

        $data = array(
            'code' => $params['code'],
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'total' => $params['total'],
        );

        $content = $this->parser->parse_string($body,$data);
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.googlemail.com',
            'smtp_user' => 'webuild.ptiki@gmail.com',
            'smtp_pass' => 'webuildsm123',
            'smtp_port' => 587,
            'smtp_crypto' => 'tls',
            'charset'   => 'utf-8'
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->from('webuild.ptiki@gmail.com', 'WeBuild Indonesia');
        $this->email->to($params['email']);
        $this->email->subject('Pembayaran '.$params['code'].' Sudah Dikonfirmasi');
        $this->email->message($content);
        $this->email->send();

    }

    public function sendmail_admin($params) {
        $this->db->select('email');
        $this->db->from('users');
        $email = $this->db->get()->result_array();

        foreach ($email as $admin) {
            $this->load->library('parser');
            $this->db->select('content');
            $this->db->where('type','admin');
            $body = $this->db->get('config')->row()->content;

            $data = array(
                'code' => $params['code'],
                'src' => $params['src'],
            );

            $content = $this->parser->parse_string($body,$data);
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'smtp.googlemail.com',
                'smtp_user' => 'webuild.ptiki@gmail.com',
                'smtp_pass' => 'webuildsm123',
                'smtp_port' => 587,
                'smtp_crypto' => 'tls',
                'charset'   => 'utf-8'
            );
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->set_mailtype("html");
            $this->email->set_newline("\r\n");

            $this->email->from('webuild.ptiki@gmail.com', 'WeBuild Indonesia');
            $this->email->to($admin['email']);
            $this->email->subject(''.$params['code'].' Telah Mengupload Bukti Pembayaran'.$params['src']);
            $this->email->message($content);
            $this->email->send();
        }

    }

    public function update_build($data){
        $this->db->where('code',$data['code']);
        $this->db->update('project_design',['total' => $data['total'] ]);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            $this->db->where('code',$data['code']);
            $this->db->update('project',['total' => $data['total'] ]);
            if($this->db->affected_rows() > 0){
                return true;
            }else{
                $this->db->where('code',$data['code']);
                $this->db->update('project_improve',['total' => $data['total'] ]);
                if($this->db->affected_rows() > 0){
                    return true;
                }else{
                    $this->db->where('code',$data['code']);
                    $this->db->update('project_design',['total' => $data['total'] ]);
                    if($this->db->affected_rows() > 0){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
        }
    }

    public function get_file_design($name,$code){
        $this->db->select($name);
        $this->db->where('code',$code);
        $q = $this->db->get('project_design');
        return $q->row_array()[$name];
    }

    public function get_file_image($id){
        $this->db->select('image');
        $this->db->where('id',$id);
        $q = $this->db->get('project_image');
        return $q->row_array()['image'];
    }

    public function get_file_result($id){
        $this->db->select('file');
        $this->db->where('id',$id);
        $q = $this->db->get('project_file');
        return $q->row_array()['file'];
    }

    public function get_file_rab($code){
        $this->db->select('file');
        $this->db->where('code',$code);
        $q = $this->db->get('project_rab');
        return $q->row_array();
    }

    public function get_design_by_user($id){
        $this->db->select('p.* , i.total');
        $this->db->from('project p');
        $this->db->join('invoice i','p.code = i.code');
        $this->db->where('p.customer_id', $id);
        $this->db->where('p.cat','design');
        $q = $this->db->get();
        return $q->result_array();
    }

    public function get_build_by_user($id){
        $this->db->select('pd.* , i.total');
        $this->db->from('project_design pd');
        $this->db->join('invoice i','pd.code = i.code');
        $this->db->where('pd.customer_id', $id);
        $q = $this->db->get();
        return $q->result_array();
    }

    public function get_dnb_by_user($id){
        $this->db->select('p.* , i.total');
        $this->db->from('project p');
        $this->db->join('invoice i','p.code = i.code');
        $this->db->where('p.customer_id', $id);
        $this->db->where('p.cat','dnb');
        $q = $this->db->get();
        return $q->result_array();
    }

    public function get_improve_by_user($id){
        $this->db->select('pi.* , i.total');
        $this->db->from('project_improve pi');
        $this->db->join('invoice i','pi.code = i.code');
        $this->db->where('pi.customer_id', $id);
        $q = $this->db->get();
        return $q->result_array();
    }
}