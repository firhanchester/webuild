<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = 'error_404';
$route['translate_uri_dashes'] = FALSE;
// $route['admin'] = 'backend/admin';

// // login backend
$route['admin'] = 'admin/admin';
$route['admin/login'] = 'admin/auth/login';
$route['admin/logout'] = 'admin/auth/logout';
$route['admin/banner_home'] = 'admin/banner/banner_home';

// login frontend
$route['login'] = 'frontend/auth';
$route['forgot-password'] = 'frontend/auth/forgot';
$route['forgot/(:any)'] = 'frontend/auth/forgot/$1';
$route['proceed-forgot'] = 'frontend/auth/proceed_forgot';
$route['proceed-password'] = 'frontend/auth/proceed_password';
$route['register'] = 'frontend/auth/register';
$route['logout'] = 'frontend/auth/logout';

// Backend users
// $route['admin/users'] = 'backend/users';
// $route['admin/auth/create_user'] = 'backend/auth/create_user';
// $route['admin/auth/edit_user/(:any)'] = 'backend/auth/edit_user/$1';
// $route['admin/users/add'] = 'backend/users/add';
// $route['admin/users/save'] = 'backend/users/save';
// $route['admin/users/edit/(:any)'] = 'backend/users/edit/$1';
// $route['admin/users/delete/(:any)'] = 'backend/users/delete/$1';

// $route['admin/customer'] = 'backend/customers';
// $route['admin/customer/save'] = 'backend/customers/save';
// $route['admin/customer/view/(:any)'] = 'backend/customers/view/$1';
// $route['admin/customer/edit/(:any)'] = 'backend/customers/edit/$1';
// $route['admin/customer/delete/(:any)'] = 'backend/customers/delete/$1';

// // project improve
// $route['admin/improve/improve_pending'] = 'backend/improve/improve_pending';
// $route['admin/improve/improve_pay'] = 'backend/improve/improve_pay';
// $route['admin/improve/progress/(:any)'] = 'backend/improve/progress/$1';
// $route['admin/improve/show/(:any)'] = 'backend/improve/show/$1';
// $route['admin/improve/input_dp'] = 'backend/improve/input_dp';
// $route['admin/improve/input_progress'] = 'backend/improve/input_progress';
// $route['admin/improve/delete/(:any)'] = 'backend/improve/delete/$1';

// // project build
// $route['admin/build/build_pending'] = 'backend/build/build_pending';
// $route['admin/build/build_pay'] = 'backend/build/build_pay';
// $route['admin/build/input_harga'] = 'backend/build/input_harga';
// $route['admin/build/input/(:any)'] = 'backend/build/input_detail/$1';
// $route['admin/build/show/(:any)'] = 'backend/build/show/$1';
// $route['admin/build/progress/(:any)'] = 'backend/build/progress/$1';
// $route['admin/build/delete/(:any)'] = 'backend/build/delete/$1';

// $route['admin/build/input_dp'] = 'backend/build/input_dp';
// $route['admin/build/input_progress'] = 'backend/build/input_progress';

// // project dnb
// $route['admin/dnb/dnb_pending'] = 'backend/dnb/dnb_pending';
// $route['admin/dnb/dnb_pay'] = 'backend/dnb/dnb_pay';
// $route['admin/dnb/input_harga'] = 'backend/dnb/input_harga';
// $route['admin/dnb/submit_design'] = 'backend/dnb/submit_design';
// $route['admin/dnb/show/(:any)'] = 'backend/dnb/show/$1';
// $route['admin/dnb/progress/(:any)'] = 'backend/dnb/progress/$1';
// $route['admin/dnb/input_dp'] = 'backend/dnb/input_dp';
// $route['admin/dnb/input_progress'] = 'backend/dnb/input_progress';
// $route['admin/dnb/progress/(:any)'] = 'backend/dnb/progress/$1';
// $route['admin/dnb/delete/(:any)'] = 'backend/dnb/delete/$1';

// // project design
// $route['admin/design/design_ongoing'] = 'backend/design/design_ongoing';
// $route['admin/design/design_pay'] = 'backend/design/design_pay';
// $route['admin/design/submit_design'] = 'backend/design/submit_design';
// $route['admin/design/input/(:any)'] = 'backend/design/input_detail/$1';
// $route['admin/design/progress/(:any)'] = 'backend/design/progress/$1';
// $route['admin/design/show/(:any)'] = 'backend/design/show/$1';
// $route['admin/design/delete/(:any)'] = 'backend/design/delete/$1';

// // project
// $route['admin/project/city'] = 'backend/project/city';
// $route['admin/project/province'] = 'backend/project/province';
// $route['admin/project/subdistrict'] = 'backend/project/subdistrict';
// $route['admin/project/edit/(:any)'] = 'backend/project/edit/$1';
// $route['admin/project/delete/(:any)'] = 'backend/project/delete/$1';
// $route['admin/project/upload_progress'] = 'backend/project/upload_progress';

// // hide
// $route['admin/hide/(:any)'] = 'backend/hide/$1';
// $route['admin/hide/show/(:any)/(:any)'] = 'backend/hide/show/$1/$2';
// // 

// $route['admin/invoice/services/(:any)'] = 'backend/invoice/services/$1';
// $route['admin/invoice/dp/(:any)'] = 'backend/invoice/dp/$1';
// $route['admin/invoice/progress/(:any)'] = 'backend/invoice/progress/$1';

// $route['admin/invoice/update'] = 'backend/invoice/update';
// $route['admin/invoice/accept_payment'] = 'backend/invoice/accept_payment';
// $route['admin/invoice/accept_payment_dp'] = 'backend/invoice/accept_payment_dp';
// $route['admin/invoice/accept_payment_progress'] = 'backend/invoice/accept_payment_progress';
// $route['admin/invoice/void_payment'] = 'backend/invoice/void_payment';

// $route['admin/invoice_progress/check/(:any)'] = 'backend/invoice/check_progress/$1';
// $route['admin/invoice_dp/check/(:any)'] = 'backend/invoice/check_dp/$1';
// $route['admin/invoice/check/(:any)'] = 'backend/invoice/check/$1';
// $route['admin/invoice/show/(:any)'] = 'backend/invoice/show/$1';
// $route['admin/invoice/edit/(:any)'] = 'backend/invoice/edit/$1';
// $route['admin/invoice/delete/(:any)'] = 'backend/invoice/delete/$1';
// // backend article
// $route['admin/article'] = 'backend/article';
// $route['admin/article/add'] = 'backend/article/add';
// $route['admin/article/store'] = 'backend/article/store';
// $route['admin/article/update'] = 'backend/article/update';
// $route['admin/article/edit/(:any)'] = 'backend/article/edit/$1';
// $route['admin/article/delete/(:any)'] = 'backend/article/delete/$1';
// // backend banner
// $route['admin/banner'] = 'backend/banner';
// $route['admin/banner/add'] = 'backend/banner/add';
// $route['admin/banner/add_home'] = 'backend/banner/add_home';
// $route['admin/banner/store'] = 'backend/banner/store';
// $route['admin/banner/update'] = 'backend/banner/update';
// $route['admin/banner/edit/(:any)'] = 'backend/banner/edit/$1';
// $route['admin/banner/delete/(:any)'] = 'backend/banner/delete/$1';
// // backend component
// $route['admin/component'] = 'backend/component';
// $route['admin/component/add'] = 'backend/component/add';
// $route['admin/component/store'] = 'backend/component/store';
// $route['admin/component/update'] = 'backend/component/update';
// $route['admin/component/edit/(:any)'] = 'backend/component/edit/$1';
// $route['admin/component/delete/(:any)'] = 'backend/component/delete/$1';
// // Fimprove
// $route['admin/fimprove'] = 'backend/fimprove';
// $route['admin/fimprove/add'] = 'backend/fimprove/add';
// $route['admin/fimprove/store'] = 'backend/fimprove/store';
// $route['admin/fimprove/update'] = 'backend/fimprove/update';
// $route['admin/fimprove/edit/(:any)'] = 'backend/fimprove/edit/$1';
// $route['admin/fimprove/delete/(:any)'] = 'backend/fimprove/delete/$1';
// // porto
// $route['admin/portofolio'] = 'backend/portofolio';
// $route['admin/portofolio/add'] = 'backend/portofolio/add';
// $route['admin/portofolio/store'] = 'backend/portofolio/store';
// $route['admin/portofolio/update'] = 'backend/portofolio/update';
// $route['admin/portofolio/add_porto/(:any)'] = 'backend/portofolio/add_project/$1';
// $route['admin/portofolio/edit/(:any)'] = 'backend/portofolio/edit/$1';
// $route['admin/portofolio/delete/(:any)'] = 'backend/portofolio/delete/$1';

// $route['admin/voucher'] = 'backend/voucher';
// $route['admin/voucher/add'] = 'backend/voucher/add';
// $route['admin/voucher/store'] = 'backend/voucher/store';
// $route['admin/voucher/update'] = 'backend/voucher/update';
// $route['admin/voucher/edit/(:any)'] = 'backend/voucher/edit/$1';
// $route['admin/voucher/delete/(:any)'] = 'backend/voucher/delete/$1';

// frontend
$route['contact'] = 'home/contact';
$route['interest'] = 'frontend/interest';
$route['article'] = 'frontend/article';
$route['about'] = 'frontend/about';
$route['fblogin'] = 'frontend/auth/fblogin';
$route['fbcallback'] = 'frontend/auth/fbcallback';
$route['glogin'] = 'frontend/auth/google_login';
$route['gcallback'] = 'frontend/auth/gcallback';
$route['actlogin'] = 'frontend/auth/act_login';
$route['actregis'] = 'frontend/auth/act_regis';
//profile
$route['my-account'] = 'frontend/profile/myaccount';
$route['my-order'] = 'frontend/profile/myorder';

// portofolio
$route['portofolio'] = 'frontend/portofolio';
$route['portofolio-detail/(:any)'] = 'frontend/portofolio/detail/$1';

// project
$route['project'] = 'frontend/project';

$route['project/proceed'] = 'frontend/project/proceed';
$route['project/proceed_second'] = 'frontend/project/proceed_second';
$route['project/proceed_improve'] = 'frontend/project/proceed_improve';
$route['project/upload'] = 'frontend/project/upload';
$route['project/click'] = 'frontend/project/click';

$route['project/detail/(:any)'] = 'frontend/project/detail/$1';

$route['my-order/(:any)'] = 'frontend/profile/order/$1';

$route['my-order/show_build/(:any)'] = 'frontend/profile/show_build/$1';
$route['my-order/show_design/(:any)'] = 'frontend/profile/show_design/$1';
$route['my-order/show_dnb/(:any)'] = 'frontend/profile/show_dnb/$1';
$route['my-order/show_improve/(:any)'] = 'frontend/profile/show_improve/$1';
$route['progress/(:any)'] = 'frontend/profile/progress/$1';

$route['project/improve/(:any)'] = 'frontend/project/improve/$1';
$route['project/request/(:any)'] = 'frontend/project/project_request/$1';
$route['project/show/(:any)'] = 'frontend/project/show/$1';

$route['download/(:any)/(:any)'] = 'frontend/project/download_file/$1/$2';
$route['download_rab/(:any)'] = 'frontend/project/download_rab/$1';
$route['download_image/(:any)'] = 'frontend/project/download_image/$1';
$route['download_result/(:any)'] = 'frontend/project/download_result/$1';

$route['metode/(:any)'] = 'frontend/invoice/metode/$1';
$route['list_invoice/(:any)'] = 'frontend/invoice/list/$1';
$route['invoice_design/(:any)'] = 'frontend/invoice/invoice_design/$1';
$route['invoice_improve/(:any)'] = 'frontend/invoice/invoice_improve/$1';
$route['invoice_build/(:any)'] = 'frontend/invoice/invoice_build/$1';
$route['invoice_dnb/(:any)'] = 'frontend/invoice/invoice_dnb/$1';
$route['invoice_dp/(:any)'] = 'frontend/invoice/invoice_dp/$1';
$route['invoice_progress/(:any)'] = 'frontend/invoice/invoice_progress/$1';

$route['invoice/upload'] = 'frontend/invoice/upload';
$route['invoice/upload_dp'] = 'frontend/invoice/upload_dp';
$route['invoice/upload_progress'] = 'frontend/invoice/upload_progress';

$route['request/(:any)'] = 'frontend/project/request/$1';
$route['request_design/(:any)'] = 'frontend/project/request_design/$1';
$route['request_improve/(:any)'] = 'frontend/project/request_improve/$1';
$route['request_dnb/(:any)'] = 'frontend/project/request_dnb/$1';

$route['design-and-build'] = 'frontend/project/design_and_build';
$route['design'] = 'frontend/project/design';
$route['home_owner'] = 'frontend/project/home_owner';
$route['home_owner/(:any)'] = 'frontend/project/home_owner/$1';
$route['search'] = 'frontend/project/search';

$route['create_invoice'] = 'frontend/invoice/create_invoice';
$route['view_notif'] = 'home/notif';
$route['voucher'] = 'home/voucher';
$route['generate/(:any)/(:any)'] = 'frontend/generate/$1/$2';