<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_404 extends CI_Controller {

	public function index() {
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->users_model->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_sosmed_user($this->session->userdata('user_sosmed'));
        }else{
        	$data['login'] = '';
        }

		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/404',$data);
		$this->load->view('frontend/footer');
	}

}
