<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Build extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}

	public function build_pending(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'build_pending',
				'title' => 'build',
				'header' => 'On Going',
				'data' => $this->modproject->get_build(2),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_build/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function build_pay(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'build_pay',
				'title' => 'build',
				'header' => 'Complete',
				'data' => $this->modproject->get_build(3),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_build/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function show($code){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'build_show',
				'title' => 'build',
				'header' => 'Detail Project',
				'data' => $this->modproject->get_project_build_by_code($code),
				'get_invoice_dp' => $this->modinvoice->get_dp_by_code($code),
				'progress' => $this->modproject->get_progress($code),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_build/show',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function input($code){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'build_detail',
				'title' => 'build',
				'header' => 'Detail Project',
				'data' => $this->modproject->get_design_by_id($code),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_build/input_harga',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function progress($code){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'build_progress',
				'title' => 'build',
				'header' => 'Project Progress',
				'data' => $this->modproject->get_project_build_by_code($code),
				'get_invoice_dp' => $this->modinvoice->get_dp_by_code($code),
				'progress' => $this->modproject->get_progress($code),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			// var_dump($this->data['get_invoice_dp']);exit;

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_build/progress',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function input_harga() {
		$post = $this->input->post();
		$now = date('Y-m-d H:i:s');
		$data = array(
			'code' => $post['code'],
			'status' => '1',
			'total' => $post['total'],
		);
		$exe = $this->modproject->input_harga_build($data);
		$src_inv = array(
			'customer_id' => $post['customer_id'],
			'code' => $post['code'],
			'total' => $post['total'],
			'update_at' => $now,
			'status' => '0',
		);
		$inv = $this->modinvoice->add($src_inv);
		$user = $this->customer->get_data($post['customer_id']);
		$params = array(
            'email' => $user['email'],
            'code' => $post['code'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'type' => '',
            'city' => $post['city'],
            'total' => $post['total'],
        );
		$send = $this->modproject->sendmail_invoice($params,'build');
		if ($exe['success'] == true) {
			redirect('admin/build/build_pending');
		}else{
			return 0;
		}
	}

	public function input_dp(){
		$post = $this->input->post();
		$this->load->helper('string');
		$post = $this->input->post();
		
		$file1['file_name'] = '';

		$config['upload_path']          = './uploads/rab/';
        $config['allowed_types']        = '*';
        $config['max_size']             = '999999999';
        $config['overwrite']			= TRUE;
		$this->load->library('upload', $config);

		if($this->upload->do_upload('attachment')){
			$file1 = $this->upload->data();
		}

		$data = array(
			'code' => $post['code'],
			'file' => $file1['file_name'],
		);
		$exe = $this->modproject->add_rab($data);

		$post['tax'] = 0;
		$slice1 = str_replace('.00', '', $post['total_build']);
		$total = str_replace(',', '', $slice1);
		$uniq = random_string('nozero', 3);

		$dp = $total*($post['dp']/100);
		$total_dp = intval($dp)+intval($uniq);
		$data = array(
			'code' => $post['code'],
			'customer_id' => $post['customer_id'],
			'dp' => $total_dp,
			'total' => $total,
			'bank' => $post['bank'],
			'uniq' => $uniq, 
		);
		$exe = $this->modinvoice->add_invoice_dp($data);
		$user = $this->customer->get_data($post['customer_id']);
		if($exe['success'] == true){
			$params = array(
				'code' => $post['code'],
				'first_name' => $user['first_name'],
				'last_name' => $user['last_name'],
				'email' => $user['email'],
				'total' => number_format($total),
				'subtotal' => number_format(intval($total_dp) - intval($uniq)),
				'dp' => number_format($total_dp),
				'tax' => number_format($post['tax']),
				'uniq' => $uniq, 
				'bank' => $post['bank'],
			);
			$sendmail = $this->modproject->sendmail_invoice_dp($params);
			$params = array(
				'customer_id' => $post['customer_id'],
				'title' => 'Tagihan DP Pembayaran '.$post['code'],
				'link' => "https://webuild-id.com/invoice_build/".$post['code'],
				'status' => 0,
			);
			$notif = $this->modproject->add_notif($params);
			if($exe['success'] == true){
				redirect('admin/build/show/'.$post['code']);
			}else{
				redirect('admin/build/show/'.$post['code']);
			}
		}
	}

	public function input_progress(){
		$this->load->helper('string');
		$post = $this->input->post();
		$recent = $this->modproject->get_last_progress($post['code']);
		$last = 'Invoice Progress '. ($recent['total'] + 1);

		$file1['file_name'] = '';

		$config['upload_path']          = './uploads/progress/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = '999999999';
        $config['overwrite']			= TRUE;
		$this->load->library('upload', $config);

		if($this->upload->do_upload('attachment')){
			$file1 = $this->upload->data();
		}

		$now = date('Y-m-d H:i:s');
		
		$data = array(
			'code' => $post['code'],
			'progress' => $post['progress'],
			'description' =>  $post['description'],
			'created_date' =>  $now,
		);

		$exe1 = $this->modproject->add_progress($data);
		if ($exe1['success'] == true) {
			$last_id = $exe1['data'];
		}else{
			exit;
		}

		$number_of_files = sizeof($_FILES['attachment']['name']);
		$filex = $_FILES['attachment'];

        for ($i=0; $i < $number_of_files; $i++) {

        	if (!empty($_FILES['attachment']['name'][$i])) {
            	$_FILES['attachment']['name'] = $filex['name'][$i];
				$_FILES['attachment']['type'] = $filex['type'][$i];
				$_FILES['attachment']['tmp_name'] = $filex['tmp_name'][$i];
				$_FILES['attachment']['error'] = $filex['error'][$i];
				$_FILES['attachment']['size'] = $filex['size'][$i];

            	$config['upload_path']          = './uploads/progress/';
	            $config['allowed_types']        = '*';
	            $config['max_size']             = '999999999';
	            $config['overwrite']			= TRUE;

				$this->load->library('upload',$config);
	            $this->upload->initialize($config);

                if ($this->upload->do_upload('attachment')){
                    $dats['uploads'] = $this->upload->data();
                    $name_file = $dats['uploads']['file_name'];
                    $data_attachment = array(
						'code' => $post['code'],
						'progress_id' => $last_id,
						'attachment' => $name_file,
						'created_date' => $now,
					);

					$exec = $this->modproject->add_progress_image($data_attachment);

                }else{
                	$error = array('error' => $this->upload->display_errors());
                }
            }else{
            	echo "error";
            }
        }

		if($post['total'] != 0){
			$data1 = array(
				'code' => $post['code'],
				'customer_id' => $post['customer_id'],
				'total' => $post['total'],
				'description' => $last,
				'bank' => $post['bank'],
			);
			$exe = $this->modinvoice->add_invoice_progress($data1);

			$params = array(
				'customer_id' => $post['customer_id'],
				'title' => 'Tagihan Pembayaran '.($recent['total'] + 2).' Progress '.$post['code'],
				'link' => base_url('my-order/show_dnb/'.$post['code']),
				'status' => 0,
			);
			$notif = $this->modproject->add_notif($params);

			$user = $this->customer->get_data($post['customer_id']);
			$params = array(
				'code' => $post['code'],
				'first_name' => $user['first_name'],
				'last_name' => $user['last_name'],
				'email' => $user['email'],
				'total' => number_format($post['total']),
				'bank' => $post['bank'],
			);
			$sendmail = $this->modproject->sendmail_invoice_progress($params);

        }else{
        	$data = array(
				'status' => 3,
				'update_at' => $now,
			);

			$exe2 = $this->modproject->update_status($post['code'],$data);
        }
        
		if($exe['success'] == true){
			redirect('admin/build/show/'.$post['code']);
		}else{
			redirect('admin/build/show/'.$post['code']);
		}
	}

	public function delete($code){
		$this->modproject->hide($code);
		redirect('admin/build/build_pending');
	}
}