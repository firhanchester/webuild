<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'customer',
				'title' => 'Customer',
				'data' => $this->customer->get_data(),
				'user' => $this->customer->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/customer/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function view($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'customer',
				'title' => 'View customer',
				'user' => $this->customer->get_data($id)
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/customer/view',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function edit($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'customer',
				'title' => 'Edit customer',
				'data' => $this->customer->get_data($id),
				'user' => $this->customer->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/customer/edit',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function delete($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			if ($this->customer->delete($id)) {
				redirect('admin/customers', 'refresh');
			}else{
				redirect('admin/customers', 'refresh');
			}
		}
	}
}