<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Improve extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	}

	public function improve_pending(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'improve-pending',
				'title' => 'improve',
				'header' => 'On Going',
				'data' => $this->modproject->get_improve(2),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_improve/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function improve_pay(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'improve_detail',
				'title' => 'improve',
				'header' => 'Complete',
				'data' => $this->modproject->get_improve(3),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_improve/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function show($code){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'improve_detail',
				'title' => 'improve',
				'header' => 'Detail Project',
				'data' => $this->modproject->get_project_improve_by_codes($code),
				'progress' => $this->modproject->get_progress($code),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_improve/show',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function progress($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'improve_progress',
				'title' => 'improve',
				'header' => 'Progress Project',
				'data' => $this->modproject->get_progress_id($id),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_improve/progress',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function input_dp(){
		$post = $this->input->post();
		$this->load->helper('string');
		$post = $this->input->post();
		
		$file1['file_name'] = '';

		$config['upload_path']          = './uploads/rab/';
        $config['allowed_types']        = '*';
        $config['max_size']             = '999999999';
        $config['overwrite']			= TRUE;
		$this->load->library('upload', $config);

		if($this->upload->do_upload('attachment')){
			$file1 = $this->upload->data();
		}

		$data = array(
			'code' => $post['code'],
			'file' => $file1['file_name'],
		);
		$exe = $this->modproject->add_rab($data);

		$post['tax'] = 0;
		$slice1 = str_replace('.00', '', $post['total_improve']);
		$total = str_replace(',', '', $slice1);
		$uniq = random_string('nozero', 3);

		$dp = $total*($post['dp']/100);
		$total_dp = intval($dp)+intval($uniq);
		$data = array(
			'code' => $post['code'],
			'customer_id' => $post['customer_id'],
			'dp' => $total_dp,
			'total' => $total,
			'bank' => $post['bank'],
			'uniq' => $uniq, 
		);
		$exe = $this->modinvoice->add_invoice_dp($data);
		$user = $this->customer->get_data($post['customer_id']);
		if($exe['success'] == true){
			$params = array(
				'code' => $post['code'],
				'first_name' => $user['first_name'],
				'last_name' => $user['last_name'],
				'email' => $user['email'],
				'total' => number_format($total),
				'subtotal' => number_format(intval($total_dp) - intval($uniq)),
				'dp' => number_format($total_dp),
				'tax' => number_format($post['tax']),
				'uniq' => $uniq, 
				'bank' => $post['bank'],
			);
			$sendmail = $this->modproject->sendmail_invoice_dp($params);
			$params = array(
				'customer_id' => $post['customer_id'],
				'title' => $post['code'].' File RAB Sudah Diupload',
				'link' => base_url('my-order/show_improve/'.$post['code']),
				'status' => 0,
			);
			$notif = $this->modproject->add_notif($params);

			
			$params = array(
				'customer_id' => $post['customer_id'],
				'title' => 'Tagihan DP Pembayaran '.$post['code'],
				'link' => base_url('my-order/show_improve/'.$post['code']),
				'status' => 0,
			);
			$notif = $this->modproject->add_notif($params);
			if($exe['success'] == true){
				redirect('admin/improve/show/'.$post['code']);
			}else{
				redirect('admin/improve/show/'.$post['code']);
			}
		}
	}


	public function input_progress(){
		$this->load->helper('string');
		$post = $this->input->post();
		$recent = $this->modproject->get_last_progress($post['code']);
		$last = 'Invoice Progress '. ($recent['total'] + 1);

		$now = date('Y-m-d H:i:s');
		
		$data = array(
			'code' => $post['code'],
			'progress' => $post['progress'],
			'description' =>  $post['description'],
			'created_date' =>  $now,
		);

		$exe1 = $this->modproject->add_progress($data);
		if ($exe1['success'] == true) {
			$last_id = $exe1['data'];
		}else{
			exit;
		}

		$number_of_files = sizeof($_FILES['attachment']['name']);
		$filex = $_FILES['attachment'];

        for ($i=0; $i < $number_of_files; $i++) {

        	if (!empty($_FILES['attachment']['name'][$i])) {
            	$_FILES['attachment']['name'] = $filex['name'][$i];
				$_FILES['attachment']['type'] = $filex['type'][$i];
				$_FILES['attachment']['tmp_name'] = $filex['tmp_name'][$i];
				$_FILES['attachment']['error'] = $filex['error'][$i];
				$_FILES['attachment']['size'] = $filex['size'][$i];

            	$config['upload_path']          = './uploads/progress/';
	            $config['allowed_types']        = '*';
	            $config['max_size']             = '999999999';
	            $config['overwrite']			= TRUE;

				$this->load->library('upload',$config);
	            $this->upload->initialize($config);

                if ($this->upload->do_upload('attachment')){
                    $dats['uploads'] = $this->upload->data();
                    $name_file = $dats['uploads']['file_name'];
                    $data_attachment = array(
						'code' => $post['code'],
						'progress_id' => $last_id,
						'attachment' => $name_file,
						'created_date' => $now,
					);

					$exec = $this->modproject->add_progress_image($data_attachment);

                }else{
                	$error = array('error' => $this->upload->display_errors());
                }
            }else{
            	echo "error";
            }
        }

        if($post['total'] != 0){
			$data1 = array(
				'code' => $post['code'],
				'customer_id' => $post['customer_id'],
				'total' => $post['total'],
				'description' => $last,
				'bank' => $post['bank'],
			);
			$exe = $this->modinvoice->add_invoice_progress($data1);

			$params = array(
				'customer_id' => $post['customer_id'],
				'title' => 'Tagihan Pembayaran '.($recent['total'] + 2).' Progress '.$post['code'],
				'link' => base_url('my-order/show_improve/'.$post['code']),
				'status' => 0,
			);
			$notif = $this->modproject->add_notif($params);

			$user = $this->customer->get_data($post['customer_id']);
			$params = array(
				'code' => $post['code'],
				'first_name' => $user['first_name'],
				'last_name' => $user['last_name'],
				'email' => $user['email'],
				'total' => number_format($post['total']),
				'bank' => $post['bank'],
			);
			$sendmail = $this->modproject->sendmail_invoice_progress($params);

        }else{
        	$data = array(
				'status' => 3,
				'update_at' => $now,
			);

			$exe2 = $this->modproject->update_status($post['code'],$data);
        }

		$params = array(
			'customer_id' => $post['customer_id'],
			'title' => 'Progress pembangunan '.($recent['total'] + 1).' project '.$post['code'],
			'link' => base_url('my-order/show_improve/'.$post['code']),
			'status' => 0,
		);
		
		$notif = $this->modproject->add_notif($params);

		if($exe['success'] == true){
			redirect('admin/improve/show/'.$post['code']);
		}else{
			redirect('admin/improve/show/'.$post['code']);
		}
	}

	public function delete($code){
		$this->modproject->hide($code);
		redirect('admin/improve/improve_pending');
	}

}