<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Banner extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'banner',
				'title' => 'Banner',
				'data' => $this->modbanner->get_data_cat(),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/banner/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function banner_home()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'banner_home',
				'title' => 'Banner',
				'data' => $this->modbanner->get_data(),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/banner/list_home',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function add(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'concept' => $this->modproject->get_concept(),
				'menu' => 'banner',
				'title' => 'Add Banner',
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/banner/add',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function add_home(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'concept' => $this->modproject->get_concept(),
				'menu' => 'banner_home',
				'title' => 'Add Banner',
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/banner/add_home',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function edit($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'concept' => $this->modproject->get_concept(),
				'concept' => $this->modproject->get_concept(),
				'menu' => 'banner',
				'title' => 'Edit Banner',
				'data' => $this->modbanner->get_data($id),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/banner/edit',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function store(){
		$uniq = random_string('alnum', 6);
		$filename = $uniq.'.jpg';

		$config['upload_path']          = './uploads/banners/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = '4096';
        $config['overwrite']			= TRUE;
        $config['file_name']			= $filename;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('banner')) {
            $data = array('upload_data' => $this->upload->data());
        }

		$post = $this->input->post();
		$input = array(
			'page' => $post['page'],
			'category' => $post['category'],
			'link' => $post['link'],
			'banner' => $filename,
		);
		$exe = $this->modbanner->add($input);

        if($exe == true){
        	redirect('admin/banner_home');
        }
	}

	public function store_cat(){
		$post = $this->input->post();
		$input = array(
			'category' => $post['category'],
			'portofolio_id' => $post['list_porto'],
		);
		$exe = $this->modbanner->add_cat($input);

        if($exe == true){
        	redirect('admin/banner');
        }
	}

	public function update(){
		$datas = $this->input->post();

		$uniq = random_string('alnum', 6);
		$size = $_FILES['banner']['size'];
		if($size == 0){
			$filename = $datas['banner2'];
		}else{
			unlink('uploads/banners/'.$datas['banner2']);
			$filename = $uniq.'.jpg';
		}
		
		$config['upload_path']          = './uploads/banners/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = '4096';
        $config['file_name']			= $filename;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('banner')) {
            $data = array('upload_data' => $this->upload->data());
        }

		$input = array(
			'page' => $datas['page'],
			'category' => $datas['category'],
			'link' => $datas['link'],
			'banner' => $filename,
		);

		$exe = $this->modbanner->update($datas['id'],$input);
		
        if($exe == true){
        	redirect('admin/banner');
        }else{
        	redirect('admin/banner');
        }
	}

	public function delete($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$get_banner = $this->modbanner->get_data($id);
			unlink('uploads/banners/'.$get_banner['banner']);
			if ($this->modbanner->delete($id)) {
				redirect('admin/banner_home', 'refresh');
			}else{
				redirect('admin/banner_home', 'refresh');
			}
		}
	}

	public function delete_cat($id){
		$del = $this->modbanner->delete_cat($id);
		if ($del == true) {
			redirect('admin/banner', 'refresh');
		}else{
			redirect('admin/banner', 'refresh');
		}
	}

	public function get_porto(){
		$cat = $this->input->post('cat');
		$exe = $this->modbanner->get_porto($cat);
		if($exe['success'] == true){
			foreach ($exe['data'] as $value) {
				echo "<option value='".$value['id']."'>".$value['title']."</option>";
			}
		}else{
			return false;
		}
	}
}