<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portofolio extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'portofolio',
				'title' => 'portofolio',
				'data' => $this->modportofolio->get_data(),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/portofolio/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function add(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'concept' => $this->modproject->get_concept(),
				'menu' => 'portofolio',
				'title' => 'Add portofolio',
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/portofolio/add',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function edit($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'portofolio',
				'title' => 'Edit portofolio',
				'data' => $this->modportofolio->get_portofolio_by_id($id),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			// var_dump($this->data['data']);exit;

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/portofolio/edit',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function store(){
		$this->load->helper('string');
		$post = $this->input->post();
		$now = date('Y-m-d H:i:s');
		$slug = str_replace(' ', '-', $post['title']);
		
		$data = array(
			'slug' => strtolower($slug),
			'type' => $post['type'],
			'province' => $post['province'],
			'city' => $post['city'],
			'title' => $post['title'],
			'lt' => $post['lt'],
			'lb' => $post['lb'],
			'price' => $post['price'],
			'description' => $post['description'],
			'created_date' =>  $now,
		);

		$exe1 = $this->modportofolio->add($data);

		$data = $this->input->post();
		foreach ($data['list_component'] as $value) {
			$input = array(
				'porto_id' => $exe1['id'],
				'component' => $value['component'],
				'spec' => $value['spec'],
			);
			$exe = $this->modportofolio->add_comp($input);
		}

		$number_of_files = sizeof($_FILES['image']['name']);
		$filex = $_FILES['image'];

        for ($i=0; $i < $number_of_files; $i++) {

        	if (!empty($_FILES['image']['name'][$i])) {
            	$_FILES['image']['name'] = $filex['name'][$i];
				$_FILES['image']['type'] = $filex['type'][$i];
				$_FILES['image']['tmp_name'] = $filex['tmp_name'][$i];
				$_FILES['image']['error'] = $filex['error'][$i];
				$_FILES['image']['size'] = $filex['size'][$i];

            	$config['upload_path']          = './uploads/portofolio/';
	            $config['allowed_types']        = '*';
	            $config['max_size']             = '999999999';
	            $config['overwrite']			= TRUE;

				$this->load->library('upload',$config);
	            $this->upload->initialize($config);

                if ($this->upload->do_upload('image')){
                    $dats['uploads'] = $this->upload->data();
                    $name_file = $dats['uploads']['file_name'];
                    $data_image = array(
						'porto_id' => $exe1['id'],
						'image' => $name_file,
						'created_date' => $now,
					);

					$exec = $this->modportofolio->add_image($data_image);

                }else{
                	$error = array('error' => $this->upload->display_errors());
                }
            }else{
            	echo "error";
            }
        }

		redirect('admin/portofolio');
	}

	public function update($id){
		$this->load->helper('string');
		$post = $this->input->post();
		// var_dump($post);exit;
		$number_of_files = sizeof($_FILES['image']['name']);
		$filex = $_FILES['image'];
		$old_img = $this->input->post('img');
		$now = date('Y-m-d H:i:s');
		$slug = str_replace(' ', '-', $post['title']);
		$price = str_replace(',', '', $post['price']);
		
		$data = array(
			'slug' => strtolower($slug),
			'title' => $post['title'],
			'lt' => $post['lt'],
			'lb' => $post['lb'],
			'price' => $price,
			'description' => $post['description'],
			'created_date' =>  $now,
		);

		$exe1 = $this->modportofolio->update($id,$data);

		$compupdate = $this->modportofolio->update_comp($id,$post['list_component']);

		$number_of_files = sizeof($_FILES['image']['name']);
		$filex = $_FILES['image'];

        for ($i=0; $i < $number_of_files; $i++) {

        	if (!empty($_FILES['image']['name'][$i])) {
            	$_FILES['image']['name'] = $filex['name'][$i];
				$_FILES['image']['type'] = $filex['type'][$i];
				$_FILES['image']['tmp_name'] = $filex['tmp_name'][$i];
				$_FILES['image']['error'] = $filex['error'][$i];
				$_FILES['image']['size'] = $filex['size'][$i];

            	$config['upload_path']          = './uploads/portofolio/';
	            $config['allowed_types']        = '*';
	            $config['max_size']             = '999999999';
	            $config['overwrite']			= TRUE;

				$this->load->library('upload',$config);
	            $this->upload->initialize($config);

                if ($this->upload->do_upload('image')){
                    $dats['uploads'] = $this->upload->data();
                    $name_file = $dats['uploads']['file_name'];
                    $data_image = array(
						'porto_id' => $id,
						'image' => $name_file,
						'created_date' => $now,
					);

					if ($old_img[$i] != '') {
						$update = $old_img[$i]; // ini buat acuan update di table, sesuai nama image lama
						$exec = $this->modportofolio->update_image($data_image,$update);
					} else {
						$exec = $this->modportofolio->update_image($data_image);
					}


                }else{
                	$error = array('error' => $this->upload->display_errors());
                }
            }else{
            	echo "error";
            }
        }

		redirect('admin/portofolio');
	}

	public function delete($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			if ($this->modportofolio->delete($id)) {
				redirect('admin/portofolio', 'refresh');
			}else{
				redirect('admin/portofolio', 'refresh');
			}
		}
	}
}
