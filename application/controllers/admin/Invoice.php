<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

	public function services($var)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($var == 'verification'){
				$status = 1;
				$menu = 'verification-services';
			}else if($var == 'pay'){
				$status = 2;
				$menu = 'pay-services';
			}
			
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => $menu,
				'title' => 'services',
				'data' => $this->modinvoice->get_invoice($status),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/invoice/jasa',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function dp($var)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($var == 'verification'){
				$status = 1;
				$menu = 'verification-dp';
			}else if($var == 'pay'){
				$status = 2;
				$menu = 'pay-dp';
			}

			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => $menu,
				'title' => 'dp',
				'data' => $this->modinvoice->get_invoice_dp($status),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/invoice/dp',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function progress($var)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($var == 'verification'){
				$status = 1;
				$menu = 'verification-progress';
			}else if($var == 'pay'){
				$status = 2;
				$menu = 'pay-progress';
			}

			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => $menu,
				'title' => 'progress',
				'data' => $this->modinvoice->get_invoice_progress($status),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/invoice/progress',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function show($code)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'show-invoice',
				'title' => 'Invoice',
				'data' => $this->modinvoice->get_invoice_by_code($code),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/invoice/detail',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function check($code)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'show-invoice',
				'title' => 'Invoice',
				'xx' => 'service',
				'data' => $this->modinvoice->get_invoice_by_code($code),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/invoice/detail',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function check_dp($code)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'show-invoice',
				'title' => 'Invoice',
				'xx' => 'dp',
				'data' => $this->modinvoice->get_invoice_dp_by_code($code),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			// var_dump($this->data['data']);exit;

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/invoice/detail_dp',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function check_progress($id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'show-invoice',
				'title' => 'Invoice',
				'xx' => 'dp',
				'data' => $this->modinvoice->get_invoice_progress_by_id($id),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			// var_dump($this->data['data']);exit;

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/invoice/invoice_progress',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function accept_payment(){
		$post = $this->input->post();
		// var_dump($post);exit;
		$now = date('Y-m-d H:i:s');
		$data = array(
			'status' => 2,
			'update_at' => $now,
		);
		$exe = $this->modinvoice->update($post['code'],$data);
		$exe2 = $this->modproject->update_status($post['code'],$data);
		$user = $this->customer->get_data($post['customer_id']);
		$params = array(
            'email' => $user['email'],
            'code' => $post['code'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'total' => $post['total'],
        );
		$exes = $this->modproject->sendmail_accept($params);
		$params = array(
			'customer_id' => $post['customer_id'],
			'title' => 'Pembayaran Sukses - '.$post['code'],
			'link' => base_url('invoice_'.$post['tbl']."/".$post['code']),
			'status' => 0,
		);
		$notif = $this->modproject->add_notif($params);
		if($exe['success'] == true){
			redirect(site_url('admin/invoice/services/pay'));
		}else{
			// redirect(site_url('admin/invoice/verify'));
		}
	}

	public function accept_payment_dp(){
		$post = $this->input->post();
		$now = date('Y-m-d H:i:s');
		$data = array(
			'status' => 2,
			'update_at' => $now,
		);
		$exe = $this->modinvoice->update_dp($post['code'],$data);
		$user = $this->customer->get_data($post['customer_id']);
		$params = array(
            'email' => $user['email'],
            'code' => $post['code'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'total' => $post['dp'],
        );
		$exes = $this->modproject->sendmail_accept($params);
		$params = array(
			'customer_id' => $post['customer_id'],
			'title' => 'Pembayaran Dp '.$post['code'].' Telah Dikonfirmasi',
			'link' => base_url('invoice_dp/'.$post['code']),
			'status' => 0,
		);
		$notif = $this->modproject->add_notif($params);
		if($exe['success'] == true){
			redirect(site_url('admin/invoice/dp/pay'));
		}else{
			// redirect(site_url('admin/invoice/verify'));
		}
	}

	public function accept_payment_progress(){
		$post = $this->input->post();
		$now = date('Y-m-d H:i:s');
		$data = array(
			'status' => 2,
			'update_at' => $now,
		);
		$exe = $this->modinvoice->update_progress($post['id'],$data);
		$user = $this->customer->get_data($post['customer_id']);
		$params = array(
            'email' => $user['email'],
            'code' => $post['code'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'total' => $post['total'],
        );
		$exes = $this->modproject->sendmail_accept($params);
		$params = array(
			'customer_id' => $post['customer_id'],
			'title' => 'Pembayaran Proses Pembangunan '.$post['code'].' Telah Dikonfirmasi',
			'link' => base_url('invoice_progress/'.$post['code']),
			'status' => 0,
		);
		$notif = $this->modproject->add_notif($params);
		if($exe['success'] == true){
			redirect(site_url('admin/invoice/progress/pay'));
		}else{
			// redirect(site_url('admin/invoice/verify'));
		}
	}

	public function void_payment(){
		$post = $this->input->post();
		$now = date('Y-m-d H:i:s');
		$data = array(
			'update_at' => $now,
			'status' => 4,
		);
		$exe = $this->modinvoice->update($post['code'],$data);
		if($exe['success'] == true){
			redirect(site_url('admin/invoice/pending'));
		}else{
			redirect(site_url('admin/invoice/pending'));
		}
	}
}
