<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'voucher',
				'title' => 'voucher',
				'data' => $this->modvoucher->get_data(),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/voucher/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function add(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'voucher',
				'title' => 'Add voucher',
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/voucher/add',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function edit($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'voucher',
				'title' => 'Edit voucher',
				'data' => $this->modvoucher->get_data($id),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/voucher/edit',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function store(){
		$datas = $this->input->post();

		$exe = $this->modvoucher->add($datas);

        if($exe == true){
        	redirect('admin/voucher');
        }else{
        	redirect('admin/voucher');
        }
	}

	public function update(){
		$datas = $this->input->post();
		$exe = $this->modvoucher->update($datas);

        if($exe == true){
        	redirect('admin/voucher');
        }else{
        	redirect('admin/voucher');
        }
	}

	public function delete($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			if ($this->modvoucher->delete($id)) {
				redirect('admin/voucher', 'refresh');
			}else{
				redirect('admin/voucher', 'refresh');
			}
		}
	}
}
