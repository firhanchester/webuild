<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Design extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	}

	public function design_ongoing() {
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'design_ongoing',
				'title' => 'design',
				'header' => 'On Going',
				'data' => $this->modproject->get_design(2),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_design/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function design_pay() {
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'design_pay',
				'title' => 'design',
				'header' => 'Complete',
				'data' => $this->modproject->get_design(3),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_design/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function input_detail($code){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'design_detail',
				'title' => 'design',
				'header' => 'Detail Project',
				'data' => $this->modproject->get_p_by_code($code),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_design/input_harga',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function progress($code){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'design_progress',
				'title' => 'design',
				'header' => 'Progress Project',
				'data' => $this->modproject->get_by_code($code),
				'progress' => $this->modproject->get_progress($code),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_design/progress',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function show($code){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'design_progress',
				'title' => 'design',
				'header' => 'Progress Project',
				'data' => $this->modproject->get_p_by_code($code),
				'progress' => $this->modproject->get_progress($code),
				'design' => $this->modproject->get_filedesign_by_code($code),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/project_design/show',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function submit_design() {
		$post = $this->input->post();

		$number_of_files = sizeof($_FILES['attachment']['name']);
		$filex = $_FILES['attachment'];

        for ($i=0; $i < $number_of_files; $i++) {

        	if (!empty($_FILES['attachment']['name'][$i])) {
            	$_FILES['attachment']['name'] = $filex['name'][$i];
				$_FILES['attachment']['type'] = $filex['type'][$i];
				$_FILES['attachment']['tmp_name'] = $filex['tmp_name'][$i];
				$_FILES['attachment']['error'] = $filex['error'][$i];
				$_FILES['attachment']['size'] = $filex['size'][$i];

            	$config['upload_path']          = './uploads/design/';
	            $config['allowed_types']        = '*';
	            $config['max_size']             = '999999999';
	            $config['overwrite']			= TRUE;

				$this->load->library('upload',$config);
	            $this->upload->initialize($config);

                if ($this->upload->do_upload('attachment')){
                    $dats['uploads'] = $this->upload->data();
                    $name_file = $dats['uploads']['file_name'];
                    $data_attachment = array(
						'code' => $post['code'],
						'file' => $name_file,
					);

					$exec = $this->modproject->add_file($data_attachment);

                }else{
                	$error = array('error' => $this->upload->display_errors());
                }
            }else{
            	echo "error";
            }
        }

        $now = date('Y-m-d H:i:s');
		$src = array(
			'status' => 3,
			'update_at' => $now,
		);
		$exe2 = $this->modproject->update_status($post['code'],$src);

        $params = array(
			'customer_id' => $post['customer_id'],
			'title' => 'Design Rumah Sudah Diupload',
			'link' => base_url('my-order/show_design/'.$post['code']),
			'status' => 0,
		);
		
		$notif = $this->modproject->add_notif($params);
        redirect('admin/design/show/'.$post['code']);
	}

	public function delete($code){
		$this->modproject->hide($code);
		redirect('admin/design/design_ongoing');
	}
}