<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'article',
				'title' => 'article',
				'data' => $this->modarticle->get_data(),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/article/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function add(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'article',
				'title' => 'Add article',
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/article/add',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function edit($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'article',
				'title' => 'Edit article',
				'data' => $this->modarticle->get_data($id),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/article/edit',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function store(){
		$datas = $this->input->post();
		$uniq = random_string('alnum', 8);
		$filename = $uniq.'.jpg';

		$config['upload_path']          = './uploads/article/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = '4096';
        $config['overwrite']			= TRUE;
        $config['file_name']			= $filename;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('thumbnail')) {
            $data = array('upload_data' => $this->upload->data());
        }

        if(!empty($datas['publish'])){
        	$publish = 1;
        }else{
        	$publish = 0;
        }

		$input = array(
			'title' => $datas['title'],
			'content' => $datas['content'],
			'thumbnail' => $filename,
			'publish' => $publish,
			'author' => $datas['author'],
		);
		$exe = $this->modarticle->add($input);

        if($exe == true){
        	redirect('admin/article');
        }else{
        	redirect('admin/article');
        }
	}

	public function update(){
		$datas = $this->input->post();

		$config['upload_path']          = './uploads/article/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = '4096';
        $config['overwrite']			= TRUE;
        $config['file_name']			= $datas['thumbnail'];

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('thumbnail')) {
            $data = array('upload_data' => $this->upload->data());
        }

        if(!empty($datas['publish'])){
        	$publish = 1;
        }else{
        	$publish = 0;
        }

		$input = array(
			'title' => $datas['title'],
			'content' => $datas['content'],
			'thumbnail' => $datas['thumbnail'],
			'publish' => $publish,
			'author' => $datas['author'],
		);
		$exe = $this->modarticle->update($datas['id'],$input);

        if($exe == true){
        	redirect('admin/article');
        }else{
        	redirect('admin/article');
        }
	}

	public function delete($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			if ($this->modarticle->delete($id)) {
				redirect('admin/article', 'refresh');
			}else{
				redirect('admin/article', 'refresh');
			}
		}
	}
}
