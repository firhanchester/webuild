<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fimprove extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'fimprove',
				'title' => 'fimprove',
				'data' => $this->modcomponent->get_list_improve(),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/fimprove/list',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function add(){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'fimprove',
				'title' => 'Add fimprove',
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/fimprove/add',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function edit($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data = [
				'invoice' => $this->modproject->get_all_invoice(),
				'invoice_dp' => $this->modproject->get_all_invoice_dp(),
				'invoice_progress' => $this->modproject->get_all_invoice_progress(),
				'count_design' => $this->modproject->get_count_design(),
				'count_dnb' => $this->modproject->get_count_dnb(),
				'count_build' => $this->modproject->get_count_build(),
				'count_improve' => $this->modproject->get_count_improve(),
				'menu' => 'fimprove',
				'title' => 'Edit fimprove',
				'data' => $this->modcomponent->get_detail_improve($id),
				'user' => $this->users_model->get_data($this->ion_auth->get_user_id())
			];

			// var_dump($this->data['data']);exit;

			$this->load->view('admin/header',$this->data);
			$this->load->view('admin/sidebar',$this->data);
			$this->load->view('admin/fimprove/edit',$this->data);
			$this->load->view('admin/footer');
		}
	}

	public function store(){
		$post = $this->input->post();
		$improve = array(
			'name' => $post['name']
		);
		$post1 = $this->modcomponent->add_improve($improve);

		$cnt_kb1 = sizeof($_FILES['kb1']['name']);
		$file_kb1 = $_FILES['kb1'];

		// var_dump($cnt_kb1);exit;

		if($cnt_kb1 > 0){

			for ($i=0; $i < $cnt_kb1; $i++) {

	        	$_FILES['kb1']['name'] = $file_kb1['name'][$i]['file'];
				$_FILES['kb1']['type'] = $file_kb1['type'][$i]['file'];
				$_FILES['kb1']['tmp_name'] = $file_kb1['tmp_name'][$i]['file'];
				$_FILES['kb1']['error'] = $file_kb1['error'][$i]['file'];
				$_FILES['kb1']['size'] = $file_kb1['size'][$i]['file'];

	        	$config['upload_path']          = './uploads/fimprove/';
	            $config['allowed_types']        = '*';
	            $config['max_size']             = '999999999';
	            $config['overwrite']			= TRUE;

				$this->load->library('upload',$config);
	            $this->upload->initialize($config);

	            if ($this->upload->do_upload('kb1')){
	                $dats['uploads'] = $this->upload->data();
	                $name_file = $dats['uploads']['file_name'];
	    			$improve_comp = array(
	    				'improve_id' => $post1,
						'title' => $post['kb1'][$i]['title'],
						'description' => $post['kb1'][$i]['description'],
						'image' => $name_file,
					);
					$post2 = $this->modcomponent->add_improve_comp($improve_comp);
	            }else{
	            	$post2 = array('error' => $this->upload->display_errors());
	            }
	        }

		}

        if($exe == true){
        	redirect('admin/fimprove');
        }else{
        	redirect('admin/fimprove');
        }
	}

	public function update(){
		$post = $this->input->post();

		$cnt_kb1 = sizeof($_FILES['kb1']['name']);
		$file_kb1 = $_FILES['kb1'];

		$old_img = $this->input->post('img');
		$cnt_old = sizeof($old_img);
		// var_dump($cnt_old);exit;
		$improve = array(
			'id' => $post['id'],
			'name' => $post['name']
		);
		$post1 = $this->modcomponent->update_improve($improve);
		
		for ($i=0; $i < $cnt_old; $i++) {
			if(!empty($file_kb1['name'][$i]['file']) || $file_kb1['name'][$i]['file'] != ''){
	        	$_FILES['kb1']['name'] = $file_kb1['name'][$i]['file'];
				$_FILES['kb1']['type'] = $file_kb1['type'][$i]['file'];
				$_FILES['kb1']['tmp_name'] = $file_kb1['tmp_name'][$i]['file'];
				$_FILES['kb1']['error'] = $file_kb1['error'][$i]['file'];
				$_FILES['kb1']['size'] = $file_kb1['size'][$i]['file'];

	        	$config['upload_path']          = './uploads/fimprove/';
	            $config['allowed_types']        = '*';
	            $config['max_size']             = '999999999';
	            $config['overwrite']			= TRUE;

				$this->load->library('upload',$config);
	            $this->upload->initialize($config);

	            if ($this->upload->do_upload('kb1')){
	                $dats['uploads'] = $this->upload->data();
	                $name_file = $dats['uploads']['file_name'];
					$improve_comp = array(
						'id' => $post['kb1'][$i]['id'],
	    				'improve_id' => $post['id'],
						'title' => $post['kb1'][$i]['title'],
						'description' => $post['kb1'][$i]['description'],
						'image' => $name_file,
					);
					$post2 = $this->modcomponent->update_improve_comp($improve_comp);
	            }else{
	            	$post2 = array('error' => $this->upload->display_errors());
	            }
			}else{
				$improve_comp = array(
					'id' => $post['kb1'][$i]['id'],
    				'improve_id' => $post['id'],
					'title' => $post['kb1'][$i]['title'],
					'description' => $post['kb1'][$i]['description'],
					'image' => $old_img[$i],
				);
				$post2 = $this->modcomponent->update_improve_comp($improve_comp);
			}
        }

        if($exe == true){
        	redirect('admin/fimprove');
        }else{
        	redirect('admin/fimprove');
        }
	}

	public function delete_comp(){
		$id = $this->input->post('id');
		$del = $this->modcomponent->delete_comp($id);
		if($del == true){
			$res = array('success' => true);
		}else{
			$res = array('success' => false);
		}

		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($res));
	}

	public function delete($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			if ($this->modcomponent->delete_improve($id)) {
				redirect('admin/fimprove', 'refresh');
			}else{
				redirect('admin/fimprove', 'refresh');
			}
		}
	}
}