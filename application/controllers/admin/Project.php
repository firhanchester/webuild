<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}

	public function delete($id){
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$get_project = $this->modproject->get_data($id);
			unlink('uploads/projects/'.$get_project['project']);
			if ($this->modproject->delete($id)) {
				redirect('project', 'refresh');
			}else{
				redirect('project', 'refresh');
			}
		}
	}

	public function upload_progress(){
		$post = $this->input->post();
		
		$file1['file_name'] = '';

		$config['upload_path']          = './uploads/progress/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = '999999999';
        $config['overwrite']			= TRUE;
		$this->load->library('upload', $config);

		if($this->upload->do_upload('attachment')){
			$file1 = $this->upload->data();
		}

		$data = array(
			'code' =>  $post['code'],
			'progress' => $post['progress'],
			'description' => $post['description'],
			'created_date' =>  $post['tanggal'],
		);

		$img = array(
			'code' =>  $post['code'],
			'image' =>  $file1['file_name'],
		);

		$exe = $this->modproject->add_progress($data);
		$exe2 = $this->modproject->add_image_progress($img);

		if($exe == true){
			return redirect('admin/'.$post['page'].'/progress/'.$post['code']);
		}else{

		}
		
	}

	public function province()
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://pro.rajaongkir.com/api/province",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: a607a4d120de45a7febff4cf02bddf7d"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		// $data = json_decode($res)

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
			$data = json_decode($response, true);
			for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
				echo "<option id='".$data['rajaongkir']['results'][$i]['province_id']."' value='".$data['rajaongkir']['results'][$i]['province']."'>".$data['rajaongkir']['results'][$i]['province']."</option>";
			}
		}

	}

	public function city()
	{
		$city_id = $this->input->post('city_id');
		$prov_id = $this->input->post('province_id');
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=".$city_id."&province=".$prov_id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: a607a4d120de45a7febff4cf02bddf7d"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
			$data = json_decode($response, true);
			for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
				echo "<option value='".$data['rajaongkir']['results'][$i]['city_name']."'>".$data['rajaongkir']['results'][$i]['city_name']."</option>";
			}
		}
	}

	public function subdistrict()
	{
		$city_id = $this->input->post('city_id');
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?city=".$city_id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: a607a4d120de45a7febff4cf02bddf7d"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
			$data = json_decode($response, true);
			for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
				echo "<option value='".$data['rajaongkir']['results'][$i]['subdistrict_id']."'>".$data['rajaongkir']['results'][$i]['subdistrict_name']."</option>";
			}
		}
	}
}