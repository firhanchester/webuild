<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $data['banner'] = $this->modbanner->get_data();
        $data['improve'] = $this->modproject->get_list_improve();
        $data['concept'] = $this->modproject->get_concept();
        $data['porto'] = $this->modportofolio->get_all_portofolio()['data'];
        // var_dump($data['porto']);exit;
        if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
		    $data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/master/home',$data);
		$this->load->view('frontend/footer');
	}

	public function contact()
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }
        $data['banner'] = $this->modbanner->get_data();
        if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
		    $data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/master/contact',$data);
		$this->load->view('frontend/footer');
	}

	public function notif(){
		$id = $this->input->post('id');
		$notif = $this->customer->notif($id);
		$update = $this->customer->update_notif($id);
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($notif));
	}

	public function voucher(){
		$now = date('Y-m-d');
		$voucher = $this->input->post('voucher');
		$exe = $this->modvoucher->get_voucher($voucher);
		if($now < $exe['end'] && $now >= $exe['start'] ){
			if($exe['limit'] == 0){
				$return = array(
					'status' => false,
					'msg' => 'Voucher Telah Habis',
				);
			}else{
				$return = array(
					'status' => true,
					'msg' => 'Voucher Tersedia',
					'data' => $exe,
				);
			}
		}else{
			$return = array(
				'status' => false,
				'msg' => 'Voucher Tidak Tersedia'
			);
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
	}
}
