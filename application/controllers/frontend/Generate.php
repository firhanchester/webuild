<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generate extends CI_Controller {

	public function dnb($code){

	    $data['invoice'] = $this->modinvoice->get_invoice_by_code($code);

	    $this->load->library('pdf');

	    $this->pdf->setPaper('A3', 'potrait');
	    $this->pdf->filename = "invoice-".$code.".pdf";
	    $this->pdf->load_view('frontend/invoice/download_dnb', $data);
	}

	public function build($code){

	    $data['invoice'] = $this->modinvoice->get_invoice_by_code($code);

	    $this->load->library('pdf');

	    $this->pdf->setPaper('A3', 'potrait');
	    $this->pdf->filename = "invoice-".$code.".pdf";
	    $this->pdf->load_view('frontend/invoice/download_build', $data);
	}

	public function design($code){

	    $data['invoice'] = $this->modinvoice->get_invoice_by_code($code);

	    $this->load->library('pdf');

	    $this->pdf->setPaper('A3', 'potrait');
	    $this->pdf->filename = "invoice-".$code.".pdf";
	    $this->pdf->load_view('frontend/invoice/download_design', $data);
	}

	public function improve($code){

	    $data['invoice'] = $this->modinvoice->get_invoice_improve_by_code($code);

	    $this->load->library('pdf');

	    $this->pdf->setPaper('A3', 'potrait');
	    $this->pdf->filename = "invoice-".$code.".pdf";
	    $this->pdf->load_view('frontend/invoice/download_improve', $data);
	}

	public function dp($code){

	    $data['invoice'] = $this->modinvoice->get_invoice_dp_by_code($code);

	    $this->load->library('pdf');

	    $this->pdf->setPaper('A3', 'potrait');
	    $this->pdf->filename = "invoice-dp-".$code.".pdf";
	    $this->pdf->load_view('frontend/invoice/download_dp', $data);
	}

	public function progress($id){

	    $data['invoice'] = $this->modinvoice->get_invoice_progress_by_id($id);

	    $this->load->library('pdf');

	    $this->pdf->setPaper('A3', 'potrait');
	    $this->pdf->filename = "invoice-progress-".$id.".pdf";
	    $this->pdf->load_view('frontend/invoice/download_progress', $data);
	}

}