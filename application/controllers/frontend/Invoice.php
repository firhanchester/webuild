<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Pdf');
    }

	public function list($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['invoice'] = $this->modinvoice->get_all_invoice_by_code($code);
        // var_dump($data['invoice']);exit;

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
		$data['jml_notif'] = sizeof($data['new_notif']);
		}

		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/invoice/list',$data);
		$this->load->view('frontend/nofooter');
	}

	public function metode($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $data['project'] = $this->modproject->get_project_req($code);

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
		$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/invoice/metode',$data);
		$this->load->view('frontend/nofooter');
	}

	public function invoice_progress($id)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		$this->session->set_userdata('code',explode("/", $this->input->server('REQUEST_URI'))[2]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $data['invoice'] = $this->modinvoice->get_invoice_progress_by_id($id);

        // var_dump($data['invoice']);exit;

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
		$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/invoice/invoice_progress',$data);
		$this->load->view('frontend/minifooter');
	}

	public function invoice_dp($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		$this->session->set_userdata('code',explode("/", $this->input->server('REQUEST_URI'))[2]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $data['invoice'] = $this->modinvoice->get_invoice_dp_by_code($code);

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
		$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/invoice/invoice_dp',$data);
		$this->load->view('frontend/minifooter');
	}

	public function invoice_design($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		$this->session->set_userdata('code',explode("/", $this->input->server('REQUEST_URI'))[2]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $data['invoice'] = $this->modinvoice->get_invoice_by_code($code);
        // var_dump($data['invoice']);exit;

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
		$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/invoice/invoice_design',$data);
		$this->load->view('frontend/minifooter');
	}

	public function invoice_build($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		$this->session->set_userdata('code',explode("/", $this->input->server('REQUEST_URI'))[2]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

    	$data['invoice'] = $this->modinvoice->get_invoice_by_code($code);

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
		$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/invoice/invoice_build',$data);
		$this->load->view('frontend/minifooter');
	}

	public function invoice_dnb($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		$this->session->set_userdata('code',explode("/", $this->input->server('REQUEST_URI'))[2]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

    	$data['invoice'] = $this->modinvoice->get_invoice_by_code($code);

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
		$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		// $this->load->view('frontend/invoice/download_dnb',$data);
		$this->load->view('frontend/invoice/invoice_dnb',$data);
		$this->load->view('frontend/minifooter');
	}

	public function invoice_improve($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		$this->session->set_userdata('code',explode("/", $this->input->server('REQUEST_URI'))[2]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $data['invoice'] = $this->modinvoice->get_invoice_improve_by_code($code);

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
		$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/invoice/invoice_improve',$data);
		$this->load->view('frontend/minifooter');
	}

	public function create_invoice(){
		$this->load->helper('string');
		$post = $this->input->post();

		if($post['from'] == 'dnb'){
			$post['subtotal'] = 600000;
		}else if($post['from'] == 'design'){
			$post['subtotal'] = 400000;
		}else if($post['from'] == 'build'){
			$post['subtotal'] = 200000;
		}else if($post['from'] == 'improve'){
			$post['subtotal'] = 100000;
		}


		$uniq = random_string('nozero', 3);

		$post['tax'] = intval($post['subtotal']*(10/100));

		$subtotal = intval($post['subtotal']) + intval($uniq);
		
		$post['total'] = $subtotal + $post['tax'];
		$total = $post['subtotal'] + $post['tax'];

		$discount = 0;

		if($post['voucher'] != ''){
			$exe = $this->modvoucher->get_voucher($post['voucher']);
			if($exe['type'] == 'persen'){
				$discount = $total * $exe['nominal']/100;
				$post['total'] = ($total-$discount)+intval($uniq);
			}else{
				$discount = $exe['nominal'];
				$post['total'] = ($total-$discount)+intval($uniq);
			}
		}

		//update total project
		$update = $this->modproject->update_build($post);
		//create invoice
		$now = date('Y-m-d H:i:s');
		//array post
		$inv = array(
			'code' => $post['code'],
			'customer_id' => $post['customer_id'],
			'total' => $post['total'],
			'tax' => $post['tax'],
			'uniq' => $uniq,
			'bank' => $post['bank'],
			'tbl' => $post['from'],
			'discount' => $discount,
			'voucher' => $post['voucher'],
		);
		// var_dump($inv);exit;
		$proceed = $this->modinvoice->add($inv);
		$user = $this->customer->get_data($post['customer_id']);
		if($proceed['success'] == true){
			$params = array(
				'code' => $post['code'],
				'first_name' => $user['first_name'],
				'last_name' => $user['last_name'],
				'email' => $user['email'],
				'total' => number_format($post['total']),
				'tax' => number_format($post['tax']),
				'uniq' => $uniq, 
				'bank' => $post['bank'],
			);
			$sendmail = $this->modproject->sendmail_invoice_services($params,$post['from']);
			$params = array(
				'customer_id' => $post['customer_id'],
				'title' => 'Tagihan pembayaran '.$post['code'],
				'link' => base_url("invoice_".$post['from']."/".$post['code']),
				'status' => 0,
			);
			$notif = $this->modproject->add_notif($params);
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($proceed));
	}

	public function upload(){
		$this->load->helper('string');
		$post = $this->input->post();
		
		$file1['file_name'] = '';

		$config['upload_path']          = './uploads/invoice/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = '999999999';
        $config['overwrite']			= TRUE;
		$this->load->library('upload', $config);

		if($this->upload->do_upload('attachment')){
			$file1 = $this->upload->data();
		}

		$data = array(
			'payment' => $post['payment'],
			'bank_account' => $post['bank_account'],
			'attachment' =>  $file1['file_name'],
			'status' =>  '1',
		);

		$exe = $this->modinvoice->update($post['code'],$data);
		$xx = array(
			'code' => $post['code'],
			'src' => 'invoice service and design'
		);
		$sendmail = $this->modproject->sendmail_admin($xx);

		return redirect('invoice_'.$post['invoice'].'/'.$post['code']);
		// if($exe['success'] == true){
		// }else{

		// }
		
	}

	public function upload_dp(){
		$this->load->helper('string');
		$post = $this->input->post();
		
		$file1['file_name'] = '';

		$config['upload_path']          = './uploads/invoice/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = '999999999';
        $config['overwrite']			= TRUE;
		$this->load->library('upload', $config);

		if($this->upload->do_upload('attachment')){
			$file1 = $this->upload->data();
		}

		$now = date('Y-m-d H:i:s');

		$data = array(
			'payment' => $post['payment'],
			'bank_account' => $post['bank_account'],
			'attachment' =>  $file1['file_name'],
			'status' =>  '1',
			'update_at' =>  $now,
		);

		$exe = $this->modinvoice->update_dp($post['code'],$data);
		$xx = array(
			'code' => $post['code'],
			'src' => 'invoice dp'
		);
		$sendmail = $this->modproject->sendmail_admin($xx);

		if($exe['success'] == true){
			return redirect('invoice_dp/'.$post['code']);
		}else{

		}
		
	}

	public function upload_progress(){
		$this->load->helper('string');
		$post = $this->input->post();
		
		$file1['file_name'] = '';

		$config['upload_path']          = './uploads/invoice/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = '999999999';
        $config['overwrite']			= TRUE;
		$this->load->library('upload', $config);

		if($this->upload->do_upload('attachment')){
			$file1 = $this->upload->data();
		}

		$now = date('Y-m-d H:i:s');

		$data = array(
			'payment' => $post['payment'],
			'bank_account' => $post['bank_account'],
			'attachment' =>  $file1['file_name'],
			'status' =>  '1',
			'update_at' =>  $now,
		);

		$exe = $this->modinvoice->update_progress($post['invoice_id'],$data);
		$xx = array(
			'code' => $post['code'],
			'src' => 'invoice progress'
		);
		$sendmail = $this->modproject->sendmail_admin($xx);

		if($exe['success'] == true){
			return redirect('invoice_progress/'.$post['invoice_id']);
		}else{

		}
		
	}
}
