<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function index()
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['banner'] = $this->modbanner->get_data();
		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/user/my-account',$data);
		$this->load->view('frontend/nofooter');
	}

	public function identity()
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['banner'] = $this->modbanner->get_data();
		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/user/complete_identity',$data);
		$this->load->view('frontend/nofooter');
	}

	public function myorder()
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['design'] = $this->modproject->get_design_by_user($data['login']['id']);
    	$data['build'] = $this->modproject->get_build_by_user($data['login']['id']);
    	$data['dnb'] = $this->modproject->get_dnb_by_user($data['login']['id']);
    	$data['improve'] = $this->modproject->get_improve_by_user($data['login']['id']);

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/user/my-order',$data);
		$this->load->view('frontend/nofooter');
	}

	public function order($var)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        if($var == 'design'){
        	$data['var'] = $var;
        	$data['menu'] = 'Design';
	        $data['list'] = $this->modproject->get_design_by_user($data['login']['id']);
        }else if($var == 'build'){
        	$data['var'] = $var;
        	$data['menu'] = 'Build';
        	$data['list'] = $this->modproject->get_build_by_user($data['login']['id']);
        }else if($var == 'dnb'){
        	$data['var'] = $var;
        	$data['menu'] = 'Design & build';
        	$data['list'] = $this->modproject->get_dnb_by_user($data['login']['id']);
        }else if($var == 'improve'){
        	$data['var'] = $var;
        	$data['menu'] = 'Improve';
        	$data['list'] = $this->modproject->get_improve_by_user($data['login']['id']);
        }

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}

		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/user/order',$data);
		$this->load->view('frontend/nofooter');
	}

	public function progress($id)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['progress'] = $this->modproject->get_progress_id($id)['progress'];
        $data['image'] = $this->modproject->get_progress_id($id)['image'];

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/user/progress',$data);
		$this->load->view('frontend/footer');
	}

	public function show_design($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['order'] = $this->modproject->get_p_by_code($code);
        // var_dump($data['order']);exit;

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/user/view-project-design',$data);
		$this->load->view('frontend/footer');
	}

	public function show_dnb($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['order'] = $this->modproject->get_p_by_code($code);
        $data['rab'] = $this->modproject->get_file_rab($code);
        $data['progress'] = $this->modproject->get_progress($code);
        $inv = $this->modinvoice->get_count_invoice_by_code($code);
        $data['invoice'] = $this->modinvoice->get_all_invoice_by_code($code);
        $data['total_invoice'] = sizeof($inv);

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/user/view-project-dnb',$data);
		$this->load->view('frontend/footer');
	}

	public function show_build($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['order'] = $this->modproject->get_project_build_by_code($code);
        $data['rab'] = $this->modproject->get_file_rab($code);
        $data['progress'] = $this->modproject->get_progress($code);
        $inv = $this->modinvoice->get_count_invoice_by_code($code);
        $data['invoice'] = $this->modinvoice->get_all_invoice_by_code($code);
        $data['total_invoice'] = sizeof($inv);

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/user/view-project-build',$data);
		$this->load->view('frontend/footer');
	}

	public function show_improve($code)
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['order'] = $this->modproject->get_project_improve_by_code($code);
        $data['rab'] = $this->modproject->get_file_rab($code);
        $data['progress'] = $this->modproject->get_progress($code);
        $inv = $this->modinvoice->get_count_invoice_by_code($code);
        $data['invoice'] = $this->modinvoice->get_all_invoice_by_code($code);
        $data['total_invoice'] = sizeof($inv);

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/user/view-project-improve',$data);
		$this->load->view('frontend/footer');
	}

	public function myaccount()
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['banner'] = $this->modbanner->get_data();
		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/user/my-account',$data);
		$this->load->view('frontend/nofooter');
	}
}
