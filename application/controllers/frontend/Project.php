<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	public function index() {
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }
        $data['fitur'] = array('Smart','Interior','Kitchen Set','Canopy','Civil','Landscape','MEP','Etc');
        $data['link'] = array('smart','interior','kitchen','canopy','civil','landscape','mep','etc');
		if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
			$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/project/project',$data);
		$this->load->view('frontend/footer');
	}

	public function home_owner($page = null) {
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['banner'] = $this->modbanner->get_data_cat($i = null , $page);
        // var_dump($data['banner']);exit;
        $data['luxury'] = $this->modcomponent->get_data_list(1);
        $data['premium'] = $this->modcomponent->get_data_list(2);
        $data['minimalist'] = $this->modcomponent->get_data_list(3);

		if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/master/home_owner',$data);
		$this->load->view('frontend/footer');
	}

	public function design_and_build() {
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['banner'] = $this->modbanner->get_data();
        $data['luxury'] = $this->modcomponent->get_data_list(1);
        $data['premium'] = $this->modcomponent->get_data_list(2);
        $data['minimalist'] = $this->modcomponent->get_data_list(3);
		if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/master/dnb',$data);
		$this->load->view('frontend/nofooter');
	}

	public function design() {
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

        $data['banner'] = $this->modbanner->get_data();
        $data['luxury'] = $this->modcomponent->get_data_list(1);
        $data['premium'] = $this->modcomponent->get_data_list(2);
        $data['minimalist'] = $this->modcomponent->get_data_list(3);
		if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/master/design',$data);
		$this->load->view('frontend/nofooter');
	}

	public function invoice($code) {
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
        if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

		if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/invoice/invoice',$data);
		$this->load->view('frontend/footer');
	}

	public function show($input){
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

		if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		if($input == 'premium' || $input == 'luxury' || $input == 'minimalis' || $input == 'modern' || $input == 'classic' || $input == 'resort') {
	        $data['type'] = $input;
	        $data['location'] = 'DKI Jakarta / Jakarta selatan';
	        $data['title'] = 'WeHouse '.$input.' Project';
	        $data['list'] = $this->modproject->get_list($input);
			$this->load->view('frontend/list',$data);
		}else{
			$this->load->view('frontend/404',$data);
		}
		$this->load->view('frontend/footer');
	}

	public function improve($input){
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
		}
        // $data['improve'] = $this->modproject->get_list_improve($input);
        $data['improve'] = $this->modcomponent->get_detail_improve($input);
        // var_dump($data['improve']);exit;

	$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		if($data['improve'] != false ){
			$this->load->view('frontend/project/improve',$data);
		}else{
			$this->load->view('frontend/404',$data);
		}
		$this->load->view('frontend/footer');
	}

	public function project_request($input){
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	redirect('login');
        }

		if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$data['improve'] = $this->modproject->get_list_improve();
		foreach ($data['improve'] as $value) {
			if($input == $value['name']) {
		        $data['type'] = $value['name'];
				$this->load->view('frontend/project/project_request',$data);
			}
		}
		$this->load->view('frontend/footer');
	}

	public function search(){
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $input = $this->input->get('search');
		$exe = $this->modproject->search($input);
		$data['item'] = $exe['data'];
		$data['search'] = $input;
		// var_dump($data['item']);exit;

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/master/search',$data);
		$this->load->view('frontend/footer');
	}

	public function detail($id) {
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }
        
        $data['banner'] = $this->modbanner->get_data();
		if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/project/detail',$data);
		$this->load->view('frontend/footer');
	}

	public function proceed(){
		$this->load->helper('string');
		$data = $this->input->post();
		$thn = date('y');
		$code = 'WB'.$thn.random_string('numeric',4);
		$data1 = array(
			'code' => $code,
			'customer_id' => $data['customer_id'],
			'province' => $data['province'],
			'city' => $data['city'],
			'luas_tanah' => $data['luas_tanah'],
			'luas_bangunan' => $data['luas_bangunan'],
			'floor' => $data['floor'],
			'type' => $data['type'],
			'tbl' => 'project',
			'cat' => $data['cat'],
			'other' => $data['other'],
		);
		$proceed = $this->modproject->add($data1);
		$data2 = array(
			'code' => $code,
			'main_bedroom' => $data['main_bedroom'],
			'bedroom' => $data['bedroom'],
			'living_bedroom' => $data['living_bedroom'],
			'family_bedroom' => $data['family_bedroom'],
			'main_bathroom' => $data['main_bathroom'],
			'bathroom' => $data['bathroom'],
			'guest_bathroom' => $data['guest_bathroom'],
			'laundry_room' => $data['laundry_room'],
			'study_room' => $data['study_room'],
			'maid_room' => $data['maid_room'],
			'kitchen' => $data['kitchen'],
			'pantry' => $data['pantry'],
			'garage' => $data['garage'],
			'carport' => $data['carport'],
			'lantai' => $data['lantai'],
		);
		$proceed2 = $this->modproject->add_comp($data2);
		$now = date('Y-m-d');
		$date = date('Y-m-d', strtotime($now.' +48 hour'));
		$user = $this->customer->get_data($data['customer_id']);
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($proceed));
	}

	public function proceed_second(){
		$data = $this->input->post();
		$data2 = array(
			'code' => $data['code'],
			'main_bedroom' => $data['main_bedroom'],
			'bedroom' => $data['bedroom'],
			'living_bedroom' => $data['living_bedroom'],
			'family_bedroom' => $data['family_bedroom'],
			'main_bathroom' => $data['main_bathroom'],
			'bathroom' => $data['bathroom'],
			'guest_bathroom' => $data['guest_bathroom'],
			'laundry_room' => $data['laundry_room'],
			'study_room' => $data['study_room'],
			'maid_room' => $data['maid_room'],
			'kitchen' => $data['kitchen'],
			'pantry' => $data['pantry'],
			'lantai' => $data['lantai'],
		);
		$proceed = $this->modproject->add_comp($data2);
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($proceed));
	}

	public function proceed_improve(){
		$this->load->helper('string');
		$data = $this->input->post();
		$thn = date('y');
		$data['code'] = 'WB'.$thn.random_string('numeric',4);
		$proceed = $this->modproject->add_improve($data);
		$now = date('Y-m-d');
		$date = date('Y-m-d', strtotime($now.' +48 hour'));
		$user = $this->customer->get_data($data['customer_id']);
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($proceed));
	}

	public function click(){
		$id = $this->input->post('porto_id');
		$proceed = $this->modportofolio->get_portofolio_by_id($id);
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($proceed));
	}

	public function get_inv_progress(){
		$id = $this->input->post('id');
		$proceed = $this->modproject->get_inv_progress_by_id($id);
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($proceed));
	}

	public function download_file($name,$code){
		$file = $this->modproject->get_file_design($name,$code);
		// var_dump($file);exit;			
		force_download('./uploads/banners/'.$file,NULL);
	}

	public function download_image($id){
		$file = $this->modproject->get_file_image($id);
		// var_dump($file);exit;			
		force_download('./uploads/projects/'.$file,NULL);
	}

	public function download_result($id){
		$file = $this->modproject->get_file_result($id);
		// var_dump($file);exit;			
		force_download('./uploads/design/'.$file,NULL);
	}

	public function download_rab($code){
		$file = $this->modproject->get_file_rab($code);
		// var_dump($file);exit;			
		force_download('./uploads/rab/'.$file,NULL);
	}

	public function upload(){
		$this->load->helper('string');
		$thn = date('y');
		$code = 'WB'.$thn.random_string('numeric',4);
		
		$file1['file_name'] = '';
		$file2['file_name'] = '';
		$file3['file_name'] = '';
		$file4['file_name'] = '';
		$file5['file_name'] = '';

		$config['upload_path']          = './uploads/projects/';
        $config['allowed_types']        = '*';
        $config['max_size']             = '999999999';
        $config['overwrite']			= TRUE;
		$this->load->library('upload', $config);

		// script upload file pertama
		if($this->upload->do_upload('denah1')){
			$file1 = $this->upload->data();
		}
		if($this->upload->do_upload('denah2')){
			$file2 = $this->upload->data();
		}
		if($this->upload->do_upload('denah3')){
			$file3 = $this->upload->data();
		}
		if($this->upload->do_upload('denah4')){
			$file4 = $this->upload->data();
		}
		if($this->upload->do_upload('denah5')){
			$file5 = $this->upload->data();
		}

		$data = array(
			'code' => $code,
			'customer_id' => $this->input->post('user_id'),
			'province' => $this->input->post('province'),
			'city' => $this->input->post('city'),
			'type' => $this->input->post('type'),
			'site_plan' =>  $file1['file_name'],
			'architect_drawing' =>  $file2['file_name'],
			'structure_drawing' =>  $file3['file_name'],
			'mep_drawing' =>  $file4['file_name'],
			'other' =>  $file5['file_name'],
		);

		$exe = $this->modproject->add_image($data);
		$now = date('Y-m-d H:i:s');
		$date = date('Y-m-d H:i:s', strtotime($now.' +48 hour'));
		$user = $this->customer->get_data($data['customer_id']);

		if($exe['success'] == true){
			return redirect('request/'.$code);
		}else{

		}
		
	}

	public function request($code){
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $now = date('Y-m-d');
		$data['date'] = date('Y-m-d', strtotime($now.' +48 hour'));
		$data['project'] = $this->modproject->get_project_req($code);

		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
		}
	$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		if($code != null){
			$this->load->view('frontend/project/request',$data);
	    }else{
	    	$this->load->view('frontend/404',$data);
	    }
	}

	public function request_dnb($code){
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $now = date('Y-m-d');
		$data['date'] = date('Y-m-d', strtotime($now.' +48 hour'));
		$data['project'] = $this->modproject->get_project_req($code);

		if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		if($code != null){
			$this->load->view('frontend/project/request_dnb',$data);
	    }else{
	    	$this->load->view('frontend/404',$data);
	    }
	}

	public function request_design($code){
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $now = date('Y-m-d');
		$data['date'] = date('Y-m-d', strtotime($now.' +48 hour'));
		$data['project'] = $this->modproject->get_project_req($code);

		if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		if($code != null){
			$this->load->view('frontend/project/request_design',$data);
	    }else{
	    	$this->load->view('frontend/404',$data);
	    }
	}

	public function request_improve($code){
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
	        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

	        $now = date('Y-m-d');
			$data['date'] = date('Y-m-d', strtotime($now.' +48 hour'));
			$data['project'] = $this->modproject->get_project_improve_by_code($code);

			if($data['login'] != ''){
			    $data['notif'] = $this->modproject->notif($data['login']['id']);
			    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
				$data['jml_notif'] = sizeof($data['new_notif']);
			}
			$this->load->view('frontend/header',$data);
			$this->load->view('frontend/head',$data);
	        if($code != null){
				$this->load->view('frontend/project/request_improve',$data);
		    }else{
		    	$this->load->view('frontend/404',$data);
		    }

	}
}
