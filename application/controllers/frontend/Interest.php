<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interest extends CI_Controller {

	public function index()
	{
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $data['banner'] = $this->modbanner->get_data();
		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
$data['jml_notif'] = sizeof($data['new_notif']);
		}
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/interest',$data);
		$this->load->view('frontend/footer');
	}
}
