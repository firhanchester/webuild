<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        require_once APPPATH.'third_party/src/Google_Client.php';
        require_once APPPATH.'third_party/src/contrib/Google_Oauth2Service.php';
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language','email','string'));
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('auth');
    }

    public function index()
    { 
        // $data['item'] = $this->customer->show_item()['data'];
        if ($this->ion_auth->logged_in()){
            $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
            redirect('');
        }else if(!is_null($this->session->userdata('user_sosmed'))){
            $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
            redirect('');
        }else if(!is_null($this->session->userdata('customer'))){
            $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
            redirect('');
        }else{
            $data['login'] = '';
        }
        if($data['login'] != ''){
            $data['notif'] = $this->modproject->notif($data['login']['id']);
            $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
            $data['jml_notif'] = sizeof($data['new_notif']);
        }
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/head',$data);
        $this->load->view('frontend/login',$data);
        $this->load->view('frontend/footer');
    }

    public function forgot($id = null)
    { 
        // $data['item'] = $this->customer->show_item()['data'];
        if ($this->ion_auth->logged_in()){
            $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
            redirect('');
        }else if(!is_null($this->session->userdata('user_sosmed'))){
            $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
            redirect('');
        }else if(!is_null($this->session->userdata('customer'))){
            $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
            redirect('');
        }else{
            $data['login'] = '';
        }
        if($data['login'] != ''){
            $data['notif'] = $this->modproject->notif($data['login']['id']);
            $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
            $data['jml_notif'] = sizeof($data['new_notif']);
        }
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/head',$data);
        // var_dump($id);exit;
        if($id == NULL){
            $this->load->view('frontend/forgot',$data);
        }else{
            $data['user'] = $this->customer->get_user_by_token($id);
            // var_dump($data['user']);exit;
            $this->load->view('frontend/password',$data);
        }
        $this->load->view('frontend/footer');
    }

    public function proceed_forgot(){
        $email = $this->input->post('email');
        $exe = $this->customer->cek_email($email);
        $token = random_string('alnum',15);
        $link = base_url().'forgot/'.$token;
        if($exe['success'] == false){
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($exe));
        }else{
            $dats = array(
                'token' => $token,
                'user_id' => $exe['data']['id'],
            ); 
            $this->customer->add_token($dats);
            $data = array(
                'link' => $link,
                'email' => $email,
            );
            $this->customer->sendmail_forgot($data);
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($exe));
        }
    }

    public function proceed_password(){
        $post = $this->input->post();
        $data = array(
            'id' => $post['id'],
            'password' => SHA1($post['password']),
        );
        $this->customer->update_password($data);
        redirect(site_url('login'));
    }

    public function register()
    { 
        // $data['item'] = $this->customer->show_item()['data'];
        if ($this->ion_auth->logged_in()){
            $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
            redirect('');
        }else if(!is_null($this->session->userdata('user_sosmed'))){
            $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
            redirect('');
        }else if(!is_null($this->session->userdata('customer'))){
            $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
            redirect('');
        }else{
            $data['login'] = '';
        }
        if($data['login'] != ''){
            $data['notif'] = $this->modproject->notif($data['login']['id']);
            $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
            $data['jml_notif'] = sizeof($data['new_notif']);
        }
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/head',$data);
        $this->load->view('frontend/register',$data);
        $this->load->view('frontend/footer');
    }

    public function act_login(){
        $post = $this->input->post();
        $data = array(
            'email' => $post['email'],
            'password' => SHA1($post['password']),
        );
        $act = $this->customer->login($data);
        if($act['success'] == true){
            $this->session->set_userdata('customer' , $data['email']);
            redirect(site_url(''));
        }else{
            $this->session->set_flashdata('warning', $act['msg']);
            redirect(site_url('login'));
        }
    }

    public function act_regis(){
        $this->load->helper('string');
        $uniq_id = strtoupper(random_string('alnum', 8));
        $post = $this->input->post();
        $data = array(
            'uniq_id' => 'wb_'.$uniq_id,
            'username' => $post['username'],
            'first_name' => $post['first_name'],
            'last_name' => $post['last_name'],
            'email' => $post['email'],
            'phone' => $post['phone'],
            'password' => SHA1($post['password']),
            'active' => '0',
        );
        $act = $this->customer->regis($data);
        if($act['success'] == true){
            $token = random_string('alnum',15);
            $link = base_url().'forgot/'.$token;
            $dats = array(
                'token' => $token,
                'user_id' => $exe['data']['id'],
            ); 
            $this->customer->add_token_regis($dats);
            $datd = array(
                'link' => $link,
                'email' => $post['email'],
            );
            $this->customer->sendmail_regis($datd);
            $this->session->set_flashdata('success', 'Anda berhasil Mendaftar, silahkan login');
            redirect(site_url('login'));
        }else{
            $this->session->set_flashdata('warning', $act['msg']);
            redirect(site_url('login'));
        }
    }

    public function verified($token){
        $this->customer->get_user_by_token_regis($token);
    }

    public function logout()
    {
        $this->session->unset_userdata('user_sosmed');
        $this->session->unset_userdata('customer');
        $this->session->sess_destroy();
            redirect(site_url(''));
    }

    function fblogin(){

        require_once 'vendor/facebook/graph-sdk/src/Facebook/autoload.php';

        $fb = new \Facebook\Facebook([
            'app_id' => '167218901289095',
            'app_secret' => '91df1b3287188a3f06d67ef9a073ba30',
            'default_graph_version' => 'v2.10',
            ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; 

        $loginUrl = $helper->getLoginUrl(base_url().'fbcallback', $permissions);

        header("location: ".$loginUrl);
    }

    function fbcallback(){
        require_once 'vendor/facebook/graph-sdk/src/Facebook/autoload.php';

        $fb = new Facebook\Facebook([
        'app_id' => '167218901289095',
        'app_secret' => '91df1b3287188a3f06d67ef9a073ba30',
        'default_graph_version' => 'v2.10',
        ]);
        
        $helper = $fb->getRedirectLoginHelper();  
  
        try {  
            
            $accessToken = $helper->getAccessToken();  
            
        }catch(Facebook\Exceptions\FacebookResponseException $e) {  
          // When Graph returns an error  
          echo 'Graph returned an error: ' . $e->getMessage();  
          exit;  
        } catch(Facebook\Exceptions\FacebookSDKException $e) {  
          // When validation fails or other local issues  
          echo 'Facebook SDK returned an error: ' . $e->getMessage();  
          exit;  
        }  
 
 
        try {
          // Get the Facebook\GraphNodes\GraphUser object for the current user.
          // If you provided a 'default_access_token', the '{access-token}' is optional.
          $response = $fb->get('/me?fields=id,name,email,first_name,last_name,birthday,location,gender', $accessToken);
         // print_r($response);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
          echo 'ERROR: Graph ' . $e->getMessage();
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          echo 'ERROR: validation fails ' . $e->getMessage();
          exit;
        }
    
        // User Information Retrival begins................................................
        $input = $response->getGraphUser();
        $id = $input->getProperty('id');
        $name = $input->getProperty('name');
        $firstname = $input->getProperty('first_name');
        $lastname = $input->getProperty('last_name');
        $email = $input->getProperty('email');

        $data = array(
            'id' => $id,
            'name' => $name,
            'email' => $email,
            'picture' => $id,
            'first_name' => $firstname,
            'last_name' => $lastname,
            'uniq_id' => 'fb_'.$id,
        );
        $add = $this->customer->sosmed_user($data);
        $cek = $this->customer->get_identity($data);
        $this->session->set_userdata('user_sosmed' , $data['uniq_id']);
        if ($cek['email'] == null || $cek['email'] == '') {
            redirect('profile/identity');
        }else if($cek['phone'] == null || $cek['phone'] == ''){
            redirect('profile/identity');
        }else{
            redirect('');
        }
    }

    public function google_login()
    {
        $client_id = '317622239748-e8cnnju6tp4fecknbei33lb46g2d0325.apps.googleusercontent.com';
        $client_secret = 'eVN_vbGcPUX6FF1H63qsdnFB';
        $redirectURL = base_url('gcallback');
        
        //Call Google API
        $gClient = new Google_Client();
        $gClient->setApplicationName('Webuild');
        $gClient->setClientId($client_id);
        $gClient->setClientSecret($client_secret);
        $gClient->setRedirectUri($redirectURL);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        $url = $gClient->createAuthUrl();
        header("Location:".$url);
    }

    function gcallback(){
        $client_id = '317622239748-e8cnnju6tp4fecknbei33lb46g2d0325.apps.googleusercontent.com';
        $client_secret = 'eVN_vbGcPUX6FF1H63qsdnFB';
        $redirectURL = base_url('gcallback');
        
        //Call Google API
        $gClient = new Google_Client();
        $gClient->setApplicationName('webuild');
        $gClient->setClientId($client_id);
        $gClient->setClientSecret($client_secret);
        $gClient->setRedirectUri($redirectURL);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        
        if(isset($_GET['code']))
        {
            $gClient->authenticate($_GET['code']);
            $_SESSION['token'] = $gClient->getAccessToken();
            header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
        }

        if (isset($_SESSION['token'])) 
        {
            $gClient->setAccessToken($_SESSION['token']);
        }
        
        if ($gClient->getAccessToken()) {
            $userProfile = $google_oauthV2->userinfo->get();
        } 
        else 
        {
            $url = $gClient->createAuthUrl();
        }

        $id = $userProfile['id']; 
        $name = $userProfile['name']; 
        $pict = $userProfile['picture'];
        $email = $userProfile['email'];
        $firstname = $userProfile['given_name'];
        $lastname = $userProfile['family_name'];

        $data = array(
                'id' => $id,
                'name' => $name,
                'email' => $email,
                'picture' => $pict,
                'first_name' => $firstname,
                'last_name' => $lastname,
                'uniq_id' => 'g_'.$id,
        );
        $add = $this->customer->sosmed_user($data);
        $cek = $this->customer->get_identity($data);
        $this->session->set_userdata('user_sosmed' , $data['email']);
        if ($cek['email'] == null || $cek['email'] == '') {
            redirect('profile/identity');
        }else if($cek['phone'] == null || $cek['phone'] == ''){
            redirect('profile/identity');
        }else{
            redirect('');
        }
    }
}