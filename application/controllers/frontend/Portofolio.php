<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portofolio extends CI_Controller {

	function __construct(){
        parent::__construct();
        //load libary pagination
        $this->load->library('pagination');
    }

	public function index() {
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $data['banner'] = $this->modbanner->get_data();
		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
			$data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}

		$config['base_url'] = site_url('portofolio'); //site url
        $config['total_rows'] = $this->db->count_all('portofolio'); //total row
        $config['per_page'] = 12;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
 

		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
 
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
 
        $data['data'] = $this->modportofolio->get_all_portofolio_pagination($config["per_page"], $data['page'])['data'];
        // var_dump($data['data']);     
 
        $data['pagination'] = $this->pagination->create_links();

		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/portofolio',$data);
		// $this->load->view('frontend/detail_porto',$data);
		$this->load->view('frontend/footer');
	}

	public function detail($slug) {
		$this->session->set_userdata('last_position',explode("/", $this->input->server('REQUEST_URI'))[1]);
		if ($this->ion_auth->logged_in()){
        $data['login'] = $this->customer->get_data($this->ion_auth->get_user_id());
        }else if(!is_null($this->session->userdata('user_sosmed'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('user_sosmed'));
        }else if(!is_null($this->session->userdata('customer'))){
	        $data['login'] = $this->customer->get_user($this->session->userdata('customer'));
        }else{
        	$data['login'] = '';
        }

        $data['banner'] = $this->modbanner->get_data();
		if($data['login'] != ''){
		    $data['notif'] = $this->modproject->notif($data['login']['id']);
		    $data['new_notif'] = $this->modproject->new_notif($data['login']['id']);
			$data['jml_notif'] = sizeof($data['new_notif']);
		}

        $data['porto'] = $this->modportofolio->get_portofolio($slug);
        // var_dump($data['porto']);exit;
		$this->load->view('frontend/header',$data);
		$this->load->view('frontend/head',$data);
		$this->load->view('frontend/project/detail_porto',$data);
		$this->load->view('frontend/footer');
	}
}
